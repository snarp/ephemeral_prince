require 'kramdown'
require 'gepub'
require 'erb'
require 'nokogiri'
require 'yaml'
require 'fileutils'

@in_dir    = 'data/md'
@out_fname = 'Ephemeral Prince.epub'

@cover_img = 'data/images/cover_permanence.png'

@lang   = 'en'
@title  = 'Ephemeral Prince'
@author = "Ronove"

def build_epub(md_dir=@in_dir, fname=@out_fname)
  puts "Preparing to write EPUB: #{fname}"

  book = GEPUB::Book.new
  book.language = @lang

  puts "Adding title..."
  book.add_title(@title, title_type: GEPUB::TITLE_TYPE::MAIN, lang: @lang, file_as: @title, display_seq: 1)

  puts "Adding author..."
  book.add_creator(@author, display_seq: 1)

  puts "Adding cover image..."
  File.open(@cover_img) do |io|
    inner_fname = File.join('images', File.basename(@cover_img))
    book.add_item(inner_fname, content: io)
  end

  puts "Adding chapters..."
  fnames = Dir.glob(File.join(@in_dir, '*.md')).sort
  fnames.each_with_index do |fname, i|
    puts "Adding file #{i} / #{fnames.count}: #{fname}"

    md          = File.read(fname)
    html        = Kramdown::Document.new(md).to_html

    doc = Nokogiri::HTML(html)

    if title = doc.at_css('h1')
      title = title.text.strip
      doc.css('img').each { |img| img.remove }
    else # if it's the cover
      title = 'Cover'
    end
    html = doc.to_html

    inner_fname = File.basename(fname.sub('.md','.html'))

    book.add_ordered_item(inner_fname).add_content(StringIO.new(html)).landmark(type: 'bodymatter', title: title).toc_text(title)
  end

  puts "Generating EPUB file..."
  FileUtils.mkdir_p(File.dirname(fname))
  book.generate_epub(fname)

  puts "Wrote EPUB: #{fname}"
  return book
end

build_epub()