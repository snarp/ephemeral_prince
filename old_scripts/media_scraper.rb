require_relative 'media_scraper/filepath_sanitizer'
require 'fileutils'
require 'faraday'
require 'faraday_middleware'
require 'active_support/core_ext/object/blank'

class MediaScraper
  attr_accessor :dir, :nested, :overwrite, :interval, 
                :output

  def initialize(dir:        './', 
                 overwrite:  false, 
                 nested:     true, 
                 interval:   0.0, 
                 connection: nil)
    @dir,@interval,@overwrite,@nested = dir,interval,overwrite,nested
    @connection=connection
  end


  # @return [Hash]  { 'url'=>'local filename', 'url'='local filename', ... }
  def save_files(urls,      
                 dir=       @dir, 
                 overwrite: @overwrite, 
                 nested:    @nested, 
                 interval:  @interval)
    @output = Hash.new
    urls = urls.flatten.select { |url| valid_url?(url) }.uniq
    if !overwrite
      urls = winnow_out_downloaded(urls, dir)
    end
    urls.each_with_index do |url,i|
      print "\rDOWNLOADING FILE: %05.2f%% (#{i+1}/#{urls.count}) => #{File.basename(url)}" % (100.0*(i+1)/urls.count)
      sleep(interval)
      @output[url] = save_file(url, dir, overwrite: overwrite, nested: nested)
    end
    return @output
  end
  
  def self.save_files(*args)
    self.new.save_files(*args)
  end


  # @return [String]   local filename
  def save_file(url, dir=@dir, fname: nil, overwrite: @overwrite, nested: true)
    fname ||= gen_fname(url, dir: dir, collide_ok: overwrite, nested: nested)
    begin
      download_file(url, fname, overwrite: overwrite)
    rescue Errno::EINVAL => e
      warn "Filename appears to be invalid: #{{e.msg => fname}}"
      return e
    rescue Faraday::ClientError => e
      warn "File could not be fetched: #{{e.message => url}}"
      return e
    end
  end

  def download_file(url, filename, overwrite: @overwrite)
    FileUtils::mkdir_p(File.dirname(filename))
    response = connection.get(url)
    File.open(filename, 'wb') { |file| file.write(response.body) }
    return filename
  end




  # HELPERS

  def connection
    @connection ||= Faraday.new(request: {timeout: 3})
  end

  def connection=(val)
    @connection=val
  end

  def gen_fname(url, dir: nil, collide_ok: true, nested: true)
    return nil unless uri=to_uri(url)

    fname = if nested
      [dir, uri.host.gsub('.','_'), uri.path]
    else
      [dir, File.basename(url)]
    end.select {|v| v}
    fname = sanitize(File.join(*fname))

    return fname if collide_ok || !File.exist?(fname)
    return try_rename(fname)
  end

  def winnow_out_downloaded(urls=[], dir=@dir)
    urls = urls.flatten.select { |url| valid_url?(url) }.uniq
    to_download = urls.select do |url|
      !File.exist?(gen_fname(url, dir: dir))
    end
    if to_download.count < urls.count
      puts "Removed #{urls.count-to_download.count} already-downloaded files; #{to_download.count} remaining to download."
    end
    return to_download
  end


  private
    def try_rename(old_fname)
      new_fname = "#{old_fname}"
      extname   = File.extname(old_fname)
      i=1
      while File.exist?(new_fname)
        new_fname = old_fname.sub(extname, "_#{i+=1}#{extname}")
      end
      return new_fname
    end

    def sanitize(fname)
      FilepathSanitizer::sanitize(fname)
    end

    def to_uri(url)
      begin
        uri = URI(url)
        return nil if uri.host.blank?
      rescue URI::InvalidURIError => e
        warn "#{{ e.msg => url }}"
        return nil
      end
      return uri
    end

    def valid_url?(url)
      !!to_uri(url)
    end

end