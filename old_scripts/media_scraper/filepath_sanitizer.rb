# encoding: utf-8
class MediaScraper
  class FilepathSanitizer

    PATH_CHAR_FILTER       = /[\x00-\x1F:\*\?\"<>\|]+/u
    UNICODE_WHITESPACE     = /[[:space:]]+/u
    WINDOWS_RESERVED_NAMES = ["CON","PRN","AUX","NUL","COM1","COM2","COM3","COM4","COM5","COM6","COM7","COM8","COM9","LPT1","LPT2","LPT3","LPT4","LPT5","LPT6","LPT7","LPT8","LPT9"]

    class << self

      # strip whitespace on beginning and end
      # collapse intra-string whitespace into single spaces
      def normalize(filepath)
        filepath.strip.gsub(UNICODE_WHITESPACE,'_')
      end

      # remove bad things!
      # - remove characters that aren't allowed cross-OS
      # - don't allow certain special filenames (issue on Windows)
      # - don't allow filenames to start with a dot
      # - don't allow empty filenames
      # 
      # this renormalizes after filtering in order to collapse whitespace
      def sanitize(path)
        filter(normalize(path.clone).strip.gsub(PATH_CHAR_FILTER,''))
      end

      private

        def filter(filepath)
          filepath = trim_leading_dots(filepath)
          # Filter blank basename
          if    filepath.empty?
            return fallback_filename
          # Filter blank filename
          elsif File.basename(filepath).empty?
            return File.join(File.dirname(filepath), fallback_filename)
          # Filter windows reserved names
          elsif WINDOWS_RESERVED_NAMES.include?(File.basename(filepath.upcase))
            return File.join(File.dirname(filepath), fallback_filename)
          else
            return filepath
          end
        end

        def trim_leading_dots(filepath)
          if (name=File.basename(filepath)).start_with?('.')
            while true
              name.slice!(0,1)
              break if !name.start_with?('.')
            end
            filepath = File.join(File.dirname(filepath), name)
          end
          return filepath
        end

        def fallback_filename
          "renamed"+Time.now.to_f.to_s.sub('.','')
        end

    end # class << self
  end
end