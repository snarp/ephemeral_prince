require 'json'
require 'nokogiri'
require 'reverse_markdown'
require_relative 'simple_string_cleanup'

@json_dir = 'scraped/_json'
@html_dir = 'scraped/html'
@md_dir   = 'scraped/md'

def extract_chapters
  fnames = gather_fnames
  @output = {}
  FileUtils.mkdir_p(@html_dir)
  FileUtils.mkdir_p(@md_dir)

  i = 0
  fnames.each do |in_fname|
    post = extract_html(in_fname)

    if !post[:title] || !/(Chapter|Prologue|Epilogue|Intermission)/.match(post[:title])
      next
    end

    post[:title] = simple_str_cleanup(post[:title])
    post[:body]  = clean_html(post[:body])

    stub = "%03d" % (i+=1)

    out_fname = File.join(@html_dir, "#{stub}.html")
    File.write(out_fname, post[:body])

    md_fname = File.join(@md_dir, "#{stub}.md")
    File.write(md_fname, ReverseMarkdown.convert(post[:body]).strip)

    @output[out_fname] = post[:title]
  end

  return @output
end

def clean_html(html)
  doc = Nokogiri::HTML(html)

  doc.css('a', 'figure', 'img').each do |node|
    node.remove
  end

  doc.css('i', 'em', 'b').each do |emp|
    puts "#{emp.inner_html}"
  end

  doc.css('p.center').each do |node|
    if node.inner_html.strip == ''
      node.remove
    elsif node.inner_html.include?('★')
      node.remove
    end
  end

  html = doc.at_css('body').inner_html.strip

  html = simple_str_cleanup(html)

  {
    '<!-- more -->' => "", 
    /<br\/?>/       => "</p><p>", 
    "<p"            => "\n\n<p", 
    / +<\/p>/       => '</p>', 
    '<p></p>'       => '', 
    /\n{3,}/        => "\n\n", 
    '<p class="center"><strong>...</strong></p>' => "\n<hr/>\n", 
    '<p class="center">...</p>'                  => "\n<hr/>\n", 
  }.each { |k,v| html.gsub!(k,v) }
  return html
end

def extract_html(fname)
  json = JSON::parse(File.read(fname), symbolize_names: true)

  {
    title: json[:title], 
    body:  "<h1>#{json[:title]}</h1>\n\n<div class='body'>\n\n#{json[:body]}\n\n</div>", 
  }
end


def gather_fnames(dir=@json_dir, glob='*.json')
  Dir.glob(File.join(dir, glob)).sort
end