require 'json'
require 'nokogiri'
require_relative 'media_scraper'
require_relative 'simple_string_cleanup'

@json_dir = 'scraped/_json'
@img_dir  = 'scraped/images'
@md_dir   = 'scraped/md'


def insert_img_tags
  md_fnames = Dir.glob(File.join(@md_dir, '*.md')).sort
  md_fnames.select! do |fname|
    /\d\d\d\.md/.match(File.basename(fname))
  end

  img_map = load_image_urls()

  (1..69).map do |i|
    md_fname = md_fnames[i]

    ind = "%03d" % i
    img_fname = nil
    img_map.each do |k,v|
      if k.start_with?(ind)
        img_fname = "images/#{k}.png"
        insert_img_tag(md_fname, img_fname)
      end
    end

    [md_fname, img_fname]
  end
end

def insert_img_tag(md_fname, img_fname)
  md = File.read(md_fname)
  lines = md.split("\n")

  title_line = lines.shift

  lines.unshift("#{title_line}\n\n![](#{img_fname})\n")

  md = lines.join("\n").strip

  File.write(md_fname, md)
end

def scrape_images(out_dir=@img_dir)
  load_image_urls().map do |slug,url|
    next unless url

    ext = File.extname(url) 
    fname = File.join(out_dir, "#{slug}#{ext}")

    puts fname
    media_scraper().save_file(url, fname: fname)
    fname
  end
end


def load_image_urls(fname='scraped/image_map.yml')
  YAML::load(File.read(fname))
end

def gather_image_urls(fname='scraped/image_map.yml')
  img_map = gather_fnames.map do |fname|
    extract_post_img_urls(fname)
  end.select { |v| !!v }

  output = Hash.new
  i = 0
  img_map.each do |v|
    next unless url = v[:urls].first
    title = simple_str_cleanup(v[:title])
    title = title.split('-').first.strip.sub('Chapter ','')
    title = title.downcase.gsub(/[\-\.'";?]/,'').strip.gsub(/ +/,'_')

    ind = "%03d" % (i+=1)

    output["#{ind}_fleeting_#{title}"] = url
  end

  output = output.to_a.sort_by { |v| v[0] }.to_h
  File.write(fname, output.to_yaml)
  return output
end


def extract_post_img_urls(fname)
  post = JSON::parse(File.read(fname), symbolize_names: true)

  if !post[:title] || !/(Chapter|Prologue|Epilogue|Intermission)/.match(post[:title])
    return nil
  end

  doc = Nokogiri::HTML(post[:body])

  urls = doc.css('img').map do |img|
    img['src']
  end

  {
    title: post[:title], 
    urls:  urls, 
  }
end



def media_scraper
  MediaScraper.new(dir: @img_dir)
end

def gather_fnames(dir=@json_dir, glob='*.json')
  Dir.glob(File.join(dir, glob)).sort
end