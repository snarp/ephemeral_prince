require 'kramdown'
require 'gepub'
require 'erb'
require 'nokogiri'
require 'yaml'
require 'fileutils'

@md_dir           = 'data/md'
@img_dir          = 'data/images'
@css_dir          = 'data/stylesheets'
@chapter_template = 'data/templates/chapter.html.erb'

@out_fname        = 'Ephemeral Prince (w Images).epub'

@metadata = YAML::load(File.read('data/metadata.yml'))
@title    = @metadata[:title]
@author   = @metadata[:author]
@url      = @metadata[:url]
@lang     = @metadata[:lang]

def build_epub(md_dir=@md_dir, out_fname=@out_fname, img_dir: @img_dir)
  puts "Preparing to write EPUB: #{out_fname}"

  book = GEPUB::Book.new
  book.language = @lang

  book.primary_identifier(@url, 'BookID', 'URL')

  puts "Adding title..."
  book.add_title(@title, title_type: GEPUB::TITLE_TYPE::MAIN, lang: @lang, file_as: @title, display_seq: 1)

  puts "Adding author..."
  book.add_creator(@author, display_seq: 1)

  puts "Adding stylesheets..."
  css_fnames = Dir.glob(File.join(@css_dir, '*.css')).sort
  css_fnames.each_with_index do |fname, i|
    puts "Adding stylesheet #{i+1} of #{css_fnames.count}: #{fname}"
    inner_fname = File.basename(fname)
    book.add_item(inner_fname).add_content(StringIO.new(File.read(fname)))
  end

  puts "Adding images..."
  img_fnames = Dir.glob(File.join(img_dir, '*.*')).sort
  img_fnames.each_with_index do |fname, i|
    puts "Adding image #{i+1} of #{img_fnames.count}: #{fname}"
    File.open(fname) do |io|
      inner_fname = File.join('images', File.basename(fname))
      book.add_item(inner_fname, content: io)
    end
  end

  # puts "Adding cover..."
  # add_cover(book)

  puts "Adding chapters..."
  fnames = Dir.glob(File.join(md_dir, '*.md')).sort
  fnames.each_with_index do |fname, i|
    puts "Adding file #{i+1} of #{fnames.count}: #{fname}"
    add_chapter(fname, book)
  end

  puts "Generating EPUB file..."
  FileUtils.mkdir_p(File.dirname(out_fname))
  book.generate_epub(out_fname)

  puts "Wrote EPUB: #{out_fname}"
  return book
end

def add_chapter(fname, book, title: nil)
  md   = File.read(fname)
  html = Kramdown::Document.new(md).to_html

  doc = Nokogiri::HTML(html)
  if title
    # do nothing
  elsif title = doc.at_css('h1')
    title = title.text.strip
  else # assume it's the cover
    title = 'Cover'
  end

  template = ERB.new(File.read(@chapter_template))
  @chapter_title,@chapter_html = title,html
  html = template.result(binding)
  @chapter_title,@chapter_html=nil,nil

  inner_fname = File.basename(fname.sub('.md','.html'))
  book.add_ordered_item(inner_fname).add_content(StringIO.new(html)).landmark(type: 'bodymatter', title: title).toc_text(title)
end

build_epub()