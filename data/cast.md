
# Fleeting

## Snowe

He was found on a boat just off Aldcoast shores, burns almost all over his body and a gash along the right side of his face. He doesn’t remember anything about his time on the boat or anything before then; all he remembers clearly is his name. As a result of the gash along his eye, he is practically blind in his right eye. Nevertheless, he is determined to live his life and help those that helped him back to health.

## Canan

One of the priestesses from Barina that helped heal Snowe back to health. Since she was born and raised in the temple, she always feels she has a duty to help those who can’t help themselves. Even if her life as a priestess keeps her near Barina, she wishes that one day she can explore the world. She has been friends with Rhea since they were little and they help each other through their lives at the temple. She has an older brother named Iain.

## Rhea

One of the other priestesses from Barina to help heal Snowe back to health. She was raised in a kingdom to the north and moved to Barina with her mother and father when her mother fell ill. When her mother died at the temple, Rhea remained at the temple as her father didn’t want to go through the trouble of getting her back up north. Unlike Canan, she can be brash and hotheaded, but she means well.

## High Priestess Esmeralda

She is one of the esteemed High Priestesses in the White Cloud Temple. She is in charge of Canan’s and Rhea’s teachings and is one of the closest to the temple’s lord, Sephi. She is a kind woman and takes her job very seriously and is perhaps one of the most skilled priestesses from the temple.

## Beliaz

He is a lord that many believe is too friendly for his own good (he seems to flirt with just about anyone he knows for more than a few minutes). In recent years, he’s been accepted into the Crown, an elite sect of seven lords who oversee everything. Before, however, he was Zuan’s disciple and enjoyed in exploring the world as Zuan taught him most of what he knows. While he has his admirers around Norin, he has only made close friends with those from the White Cloud Temple.

## Zuan

He is a lord that has made a name for himself with his sense of duty. Unlike Beliaz, he has no interest in humans and if at all possible would like never to be around them again. Nevertheless, he seems to take joy in the executions he oversaw in Norin and it has earned him the nickname: the Cruel Lord. Over the past two years, however, since Beliaz has entered the Crown, Zuan has only seemed to become crueler and many rumor he’s jealous of his former disciple.

## Iain

He is Canan’s older brother and he once was a priest-in-training in Barina, but he now trains in the Norin army with his father, Ciran. Like Canan, he was born in the temple, but he only spent his childhood there until two years after his mother disappeared. He enjoys life outside the temple, but he wouldn’t mind going back to live in the desert. He can easily get along with just about anyone and loves a good time.

## Hiante

A skeleton who snuck aboard a commercial ship and feigned dead until someone took him into Aldcoast. For months he was trapped in the ocean, his rage consuming him. He has but one goal: get revenge for the deaths of Astra and Erio. Even if it means forcing Snowe to confront that which may destroy him, Hiante will make sure something is done. Despite his rage, however, Hiante has an almost overwhelming feeling of sadness.

## Ciran

He is Canan and Iain’s father and is the head of Princess Lorin’s guard in the capital of Norin. He takes his job seriously and many guards and soldiers hold him in high respect. Originally from Barina, he left for Norin at a young age to make something of his life. Only when he returned to visit did he meet Canan and Iain’s mother and quickly fell in love with her. He deeply misses her, but he hides it and puts all his love into his children.

## Xiri

He is the new king of Norin after he beat the the Lion without breaking a sweat. He has no interest in ruling, however, and happily allows Lorin, the princess, do what she wishes while he takes control of Norin’s army. People are quick to point out he’s not human, but he laughs at thought that it would even matter. While in Norin, he has one goal: drag down the Crown of Lords and kill every last lord so he may return home. He’s already ridden the capital of the lords and now he bides his time until he is ready.

------

# Permanence

