# Chapter 16 - "The Castle of Alsan"

![](images/017_fleeting_16.png)


Every fiber of Snowe's being felt sick as they walked through the underground passage that led toward the Alsan castle. It wasn't the cobwebs that hung low from the passage's ceiling or the incredible cold held within; it was the idea the demon that had tormented him had to be back inside of him when all he had wanted before was to be free of the demon. The thought alone made him tremble, made him want to forget everything else existed. No amount of lies could make Snowe feel any better. No amount of convictions knowing this would help fix everything he had broken made it better.

Even at the dead of night, Alsan bustled above them. Guards were prevalent all around the city but despite their vigilance, Iain had been able to find the secret passage without anyone getting caught. It was hidden beneath a large flower patch right inside the city and thankfully no one had seen them slipping inside. Iain had explained when they came to the end of the passage, they would be close to the castle. From there he would have to find the secret entrance and with any luck, Iain could get it opened and they'd be inside. Though Iain and Canan decided that it would be best if they found their father before they ventured too far into the castle as he could ensure they were kept safe and let them know what was going on inside before Snowe could start his plan.

After minutes of pulling cobwebs from their clothes and jumping at mice that scurried in front of them, they found the exit to the passage. Iain went up first, Hiante giving him a boost up, and once he had the trap door opened, he pulled himself out. A quick pause and Iain knelt back down and reached his hand inside.

"It's safe," he said.

Rhea went first and Hiante gave her a boost as well. "How did you even know this was here?" Rhea asked as Iain took her hand and helped her through the hole.

"This is how my dad snuck me out," Iain explained. When Rhea stood safely beside him, he reached down again.

"It was probably made in case they had to sneak the royal family out," Canan suggested as Hiante helped her up so she could grab her brother's hand. He pulled her up with ease and Iain reached down once again.

Hiante boosted Snowe up next and once he was outside and beside the two priestesses, Iain helped pull Hiante through. Given his height, he didn't need any help reaching Iain's hand. When everyone was out, Iain closed the trap door and pulled the bush branches forward so they covered the trap door once again. Everything as it was before they had come through, Iain looked toward the castle and a smile stretched across his lips. Against the night sky, the castle looked lit up as each light blazed in the dark.

"We're almost there," he said. After he glanced around to get his bearings straight, he led the way forward and beckoned everyone to follow. They reached the castle walls within moments and Iain led them along it, keeping his hand on the wall as though he searched for something.

"Is Dad even going to be awake?" Canan asked as she stuck close to her brother.

"He should be for a few more hours unless Xiri changed his schedule," Iain explained.

"The guards won't hurt Hiante, will they?" Snowe asked.

"Dad will make sure they won't," Iain said as he glanced back. "Even if he's not officially the captain of the entire guard yet, everyone acts like he is."

Canan smiled. "Some things never change."

After they had moved along the wall for far enough, Iain stopped (Snowe and Canan ran into his back) and looked upward. Without a word, he retraced his steps and knocked on the wall as he went. When he found a brick that resounded with a quiet sound, as though a tiny bell had rung, he grinned.

"Found it," he said. He leaned against it and when it didn't budge, he frowned. "Damn it. They actually locked it for once."

"Probably safer that way," Rhea pointed out.

"How do we get in then?" Snowe asked.

Iain looked at the sky and folded his arms. A lookout post stood out from the castle with a few guards moving about high above. They hadn't noticed anyone, thankfully, but Iain sighed.

"Gotta whistle and hope it's the right person up there," he said. "Just be quiet and hug the wall." He stepped away and as everyone flattened their backs against the wall, he stared up at the lookout post and gave it a whistle.

A whistle promptly answered his and Iain crossed his arms and replied with his own once again. One more whistle, this time at a higher pitch, and Iain grinned.

"Oh good, she's getting my dad to open it up," he said and looked back at everyone else. "Should be just a second."

That second felt like minutes but the wall Iain had pushed before clicked and moved backwards before it slid aside. A wooden door lay beyond and as it opened, the courtyard flooded with light from within. A man, a little out of breath, stood in the doorway and he stared at Canan and Iain, his eyes wide. He looked like them both; their facial features were almost a match, their skin the same tone, and his hair dark like theirs (though it was noticeably going gray). His eyes remained distinct however, and were a pale violet. After a moment of frozen silence, the man moved forward and enveloped Canan and Iain into a tight hug.

"I was worried about you two," he said and withdrew. He gave Rhea the same hug quickly before he stepped back and looked at everyone. "I told you not to come back, Iain." His voice had changed, more stern than before, and Iain laughed weakly. He glanced over his children's heads and sighted Snowe and Hiante.

"Oh, Dad, these are Snowe and Hiante," Canan introduced as she pulled Snowe closer. "Snowe's the one they found in Aldcoast."

"My name's Ciran," Canan's father said as he took Snowe's hand and shook it. He gave Hiante one more glance before he looked at his children again. "You have a skeleton standing with you. What exactly are you doing here?"

Iain laughed weakly again as he ran a hand through his hair. "Can we come in already? This is going to take a lot of explaining."

Ciran gave everyone one last look before he nodded and moved aside so everyone could slip past him. Right inside the door was a cozy room filled with supplies; a book case had been moved aside to gain access to the door and most of the books--spell books, manuals, et cetera--had fallen to the floor before it, and other shelves filled the room with emergency supplies strewn about. Warm lanterns strung up along the ceiling lit the room and was a treat given the chill outside. After everyone had moved inside, Ciran pulled the door closed and touched a token imbedded into the door. The stone wall replaced itself quickly and Ciran turned toward everyone. He folded his arms, no doubt waiting for the explanation.

Snowe was glad Canan, Iain, and Rhea helmed the explanation. He didn't even know where to begin and they explained it far better than he knew he could have given his state. When they finished, Ciran considered his children a moment before he sighed and ran a hand through his graying hair.

"That's quite a story," he said quietly. "It's nice knowing _where_ Xiri came from at least, though it's a frightening concept." He watched Snowe for a moment before he leaned against the wall. "What he's doing now, however, is sure to cause Norin's destruction no matter how much he's helped us in the past months he's been here."

"What has he been doing here?" Hiante asked.

"What everyone thinks he's doing; building an army and preparing it so it can storm the land of lords as soon as he's able to force it open," Ciran explained.

The priestesses paled. "W-Would he hurt the temple?" Rhea asked. "I know the lords are there, but not all the priestesses want to fight him."

Ciran shook his head. "Any malice he's thrown the temple's way is a ruse," he explained. "The more he targets it, the more the lords empty out of the Crown and their land to defend the temple. It's a ploy to get the land of lords empty of any lords that could lay waste to his army. Once he has it empty, especially the Crown, Xiri can set foot into it without harm. He plans to take control of the Crown and destroy the lords using the power that's welled up there."

"Beliaz is still in the Crown though," Canan pointed out. "I don't think he'll be leaving it."

"And therein lies Xiri's issue as of late," Ciran replied. "As ill thought as most lords are, they aren't naïve enough to leave the Crown unoccupied." He sighed again. "As much as I like seeing the lords fear something they may not win against, I know for everyone else, this isn't going to end well."

The room went quiet as everyone watched Ciran. Snowe fidgeted in his spot. "Y-You won't mind then, if I take him back?" he asked.

"I'll have to discuss it with Lorin," Ciran said as he looked at Snowe, "but I figure she won't mind having her kingdom out of harm's way. She'll be happy with anything that keeps us from one day being at the mercy of the lords. They'll destroy the kingdom if it came to that. They've done it before."

"They have?" Snowe asked.

Ciran shrugged. "Some small kingdom to the north. One day the capital burned to the ground and their king and queen went missing, presumed dead," he explained. "I don't want that to happen to Norin."

"Do you think Xiri would be able to win against the lords?" Iain asked.

"I'm really not sure," Ciran said. "On the off chance he fails, which is a great possibility given he's one demon against an army of lords, and the lords are the ones to take him down, you can bet we'll all be held accountable we let him get that far and our lives here won't be peaceful any longer. If we can bring Xiri down ourselves, it'll save us from scrutiny and we can group back up and live to fight another day."

"It's a shame this is all going south," Rhea said as she crossed her arms.

Ciran paused as though he had just remembered something and he looked at Rhea and Canan, taking their hands. He knelt down a bit to be on their level. "I'm sorry about Esmeralda."

While Canan gave her father a tight hug, Rhea stayed in her spot and shook her head. "It wasn't you! Beliaz is to blame," she said.

Ciran turned to Rhea once his daughter had withdrew and he knelt down. "I was the one who kept asking her for information," he said. "If I hadn't, if I had been happy with what she had initially gave me, Esmeralda wouldn't have been caught. It is my fault alone, Rhea. Please do not blame Beliaz for something he couldn't stop."

Rhea looked away as tears welled up in her eyes and Canan wrapped an arm around her, giving her a squeeze. She didn't reply or look at Ciran again and he stood up. After a moment of silence, Ciran looked toward Snowe.

"Did you want to see Xiri tonight?" he asked.

"If-If you think it'd be okay," Snowe said.

"I'm sure he's still awake," Ciran said and when Snowe returned his nod, Ciran looked at Iain and Canan. "Iain, keep an eye on your sister and Rhea. Canan, you do the same with him. Don't let your brother do anything senseless," he said. "Do not leave this room until I come back."

Iain snorted. "You say that like I'm out to make trouble."

"You being back here means that you are," Ciran said with a smirk. When his children nodded, he looked to Hiante. "You stay here as well. I don't need a guard accidentally attacking you before I've had a chance to delicately tell them of the situation."

Hiante nodded and stayed put as Ciran led Snowe out of the small storage room. As Ciran stood in the hall, he looked back into the room and gave his children one last stern look before he finally closed the door. He looked at Snowe.

"Just stick close, all right?"

When Snowe gave him a nod, Ciran led the way.

The hall was empty and devoid of any life aside from themselves. Like the secret passage from before, a few cobwebs hung from the ceiling, but they hadn't had the chance to hang low enough to cause a problem. Most of the rooms in the hall had their doors open and the rooms empty within, and Ciran mentioned the hall was used as a place to meet without suspicion and used as a place to relax without anyone finding them. As they exited the hall, guards nearby stood at attention in Ciran's presence and he gave them each a nod in turn before he continued toward his destination.

The inside of the castle was as extravagant as Snowe had assumed it would be; it looked big enough for an entire city on its own and each spot outside the hall they had just been in was immaculate. Each banister polished so it shined, each window so clean it hardly looked like there was glass at all. Lanterns on the walls kept everything lit with a soft glow and the castle was as warm as the lights made it appear. Banners hung on almost every wall, each with an expertly stitched lion in the middle or some variation of. The carpeting beneath their feet was plush as they moved across it and it softened the sound of their footsteps. Guards stood nearby at every inch of the castle and just like the others, stood at attention as Ciran neared, giving truth to what Iain had said before.

As they entered the main hall, Ciran led Snowe toward a grand staircase. It looked to go up through every floor of the castle and from the main hall, Snowe could see balconies from each floor up and they each had vibrant plants hanging over with flowers that looked to glow underneath the soft light from the lanterns. Ciran led Snowe upwards after giving him a moment to take in the main hall.

"If I had to guess," Ciran conversed as he slowed his pace to match Snowe's, "Xiri should be in his drawing room about now. He likes to look through all of the Lion's books for any way to help him against the lords."

"Did anyone ever like the lords?" Snowe asked.

Ciran shrugged. "Not in my time," he said. "Which is why no one minded Xiri as soon as he told us of his plan to get rid of them. It was only later we realized it wasn't the right way."

Snowe watched Ciran for a moment. "Do you like him?"

Ciran raised his eyebrows as he watched Snowe. "How do you mean?"

"I mean, even given what you said about needing him gone, would you and the others miss him?" Snowe asked.

Ciran paused on the steps and sighed. "He likes me and Lorin well enough," he admitted. "And as much as I've enjoyed his company, especially compared to Lorin's father, I know if he remains, we're in trouble. Even if we'll miss him, we're not blind to the fact this is unavoidable."

Without another word said, Ciran continued up the steps with Snowe close behind. Once they reached the fourth floor, Ciran led Snowe onto the balcony and took him down the first hallway they came upon. This hall had no guards and the lights already dimmed down. "He hates it when other guards are around; he really likes to be left alone especially this time a night," Ciran explained. He paused after a moment and chuckled. "Well, aside from me I guess. He never seems to mind it when I pay him a visit."

The doors at the end of the hall were open a crack and as they approached it, Ciran leaned down toward Snowe. "Let me give you a warning before you go in," he whispered. "Sometimes he's a bit too friendly and doesn't think much of personal space."

Snowe nodded. "I know that."

"I'll be right with you if he does react badly to you, don't worry," Ciran said as they stopped before the door. "You're Canan's friend and I won't let anyone harm you."

Another nod and Snowe took a deep breath. In the back of his mind, he hoped Beliaz was wrong; that Xiri wasn't the demon that had been inside of him. The thought alone made Snowe tremble and he took another deep breath as Ciran learned toward the door.

"Xiri, this is Ciran," he called out. "May I enter?"

"Where did you even disappear to?" a voice replied from beyond the door and Snowe froze. It was familiar. "Come on in--I wanted you to look over a few things I found to tell me if you can do it or not."

Ciran glanced at Snowe and patted his shoulder. Snowe flinched at his touch, almost forgetting he had been there, and Ciran pulled the door open.

The drawing room inside was as extravagant as the rest of the castle; old and decrepit books broke the extravagant image as they were littered across the floor and stacked on the plush arm chairs set askew from the rest of the room. In the middle of it all stood a man and he looked to the doors, a grin on his lips. His hair was an unnatural red, his skin pale as though he hadn't seen the sun in a long time, and his eyes were a blazing red, far brighter than his hair. The whites of his eyes were a pitch black, just like other demons. Though the demon once trapped inside of Snowe had always been shrouded in shadow from what Snowe could recall, he knew that grin, he knew that voice, he knew this presence. Xiri was his demon after all.