# Prologue - "He's alive."

![](images/001_fleeting_prologue.png)


A ship had drifted close to port one day; the sails burnt to a crisp, the wood so destroyed that the guards at port watching it wondered how it hadn't sunk yet. It drifted alone in the bay for a few days, Aldcoast afraid to send anyone to check on it. At first they had been determined to check, but the only body they saw did not move and soon disappeared; Aldcoast became afraid of the boat.

Guards would watch it from the docks, but no one went out to check on it, not even the sailors who had to circumvent the boat as they came to dock. What if the boat was a den of phantoms? They were few and far between, especially on the mainland, but with how destroyed the ship is, one only wonders what disaster struck it and what if it was traumatic enough to give rise to phantoms? Aldcoast hadn't the manpower to take on many phantoms so as the ship kept itself adrift, they sent word to the capital for soldiers to help. Nevertheless, the guards kept watch on the ship from afar, checking it with their spyglasses for any sign that someone, or something perhaps, was alive.

The sign came a few days after Aldcoast first saw the ship; a guard had come off duty and he gave the ship one last look through his spyglass. He wanted to be sure no phantoms would come off it before he went to share a drink with other guards coming off their night shifts. It would be a good few minutes before the next showed up to keep watch. Unlike the many other times, as he looked across the bow, he saw someone. It was quick and he couldn't discern features, but someone moved along the ship's deck. It came as such a great shock, the guard almost dropped his spyglass when he spotted it. When he looked again, the figure was gone. It moved too well to be a phantom, but it was gone now.

Must be my imagination, the guard thought to himself after another glance over the ship. He compressed his spyglass and slipped it into his pocket. He was tired, he knew it. He turned to walk to the small tavern just off the docks when a voice whispered to him.

"There's someone on the ship."

The voice spoke so near, the guard jumped and looked around him. Nothing was there. He was alone on the dock. I'm drunk, the guard told himself. But drunk on what? He hadn't touched anything yet. Perhaps on thought alone he had intoxicated himself?

"Look again."

Still no one around, but the guard turned back to the ship. He gave in to the voice and pulled out his spyglass once more. Another look wouldn't hurt. He scanned the ship slowly; it still floated in its spot, all the impossibilities be damned. And the guard saw him. A fair haired young man, leaning over the railing. He hadn't been there a moment ago, he hadn't been there in the days the ship remained in the bay. He didn't even match the descriptions of the first person Aldcoast had seen. He didn't move, however, surely he was dead.

"He's alive."

The voice hadn't lied yet; surely he was alive.

The sleepy port of Aldcoast awoke as the guard gathered everyone he could. Someone was alive in the ship, he just knew it, and they had to help him. The thought of phantoms be damned. Sailors jumped at the chance to help and soon they had amassed enough supplies to head out to the boat adrift in the bay. In small groups, they rowed out to the boat, each sailor and guard only glad the skies were clear and the water was calm. Within minutes, they arrived, and from up close, they all wondered once again how it still floated. There were many holes in the hull and it was impossible that it still did not sink.

The fair haired young man still lay on the railing where the first guard had seen him and he still did not move. A few sailors and guards remained underneath him should he fall while the other groups climbed into the ship to get to him. Burns covered the young man's arms and his clothes were in tatters, blood stains seemingly everywhere on him. When a few guards drew close and pulled him from the railing, they noticed a long gash that went down his face. It cut through his right eye, the blood stuck to his skin, staining the side of his face. Clutched in his bloody and burned hands was a crystal amulet and in his cloak was a bag of something guards could not fathom what it could be, only that they glowed a dull light. As the guards checked him, very sure he must have been dead, they realized he still breathed. Through whatever calamity had struck the boat, he was alive.

The guards shouted to the sailors still in the water to return to Aldcoast and find a doctor or two. They would know how best to move him without causing more damage. As the sailors in the water went back to port to do just that, a few guards remained with the young man while the others checked on the rest of the boat.

Dead bodies were all over the ship; what appeared to be adults and children alike. They were too burned through to discern any details about who they once were and it made the guards and sailors sick as they happened upon them. Blood spread throughout what hadn't been burned through and a cold air hung over the entire ship; some guards wondered if they'd ever feel warm ever again. Afraid of every step they took, the ones who explored returned to the young man and the guards watching over him.

The two doctors arrived shortly and they did a quick check of the young man. Burns covered most of his body, underneath his clothes, and down his chest and back. They were just as shocked to find he was even alive. With blankets they had brought, the doctors wrapped the young man up and as they lifted him, a terrifying screech rang out across the boat. A phantom broke through the boards underneath them and shot into one of the doctors, knocking him clean off the ship. The other doctor almost lost hold of the young man, but a guard grabbed her and the young man, pulling them away as his comrades attacked the phantom.

The ship started to fall apart underneath the doctor's feet as she ran with the guard. Every step she and the guard took created yet another hole in the ship. Another phantom came through the boards once more, but as it tried to steady itself, the floor gave way and it crashed further into the ship. The remaining guards on the ship escaped the best they could, helping each other as the phantoms tried to attack when they weren't falling through the weakened boards.

The doctor slipped back down to her rowboat and once she was steady, she helped the guard get the young man down safely. When he was safe in her arms, the guard came down into the boat and they heard another unworldly screech; something sparked and soon every single board caught fire and the fire reached high into the sky and appeared to reach to the very starts up ahead.

With quick magic, the sailor propelled their rowboat away from the boat. The magically constructed waves moved them away fast and the doctor sighed in relief once closer to shore. Guards and other sailors dove from the burning ship and rescued anyone who had fallen when the phantoms attacked. Thankfully, no lives had been lost. Wounded yes, but those could be treated with ease compared to what awaited the burned young man they had all saved.

As the doctor took the young man to the clinic just beyond the docks, the guards and sailors watched the boat in the distance, almost ready in case any phantoms decided to come ashore. None did and the ship burned slowly in the bay and started to sink. Their curiosity abated, the people of Aldcoast could only hope the boy they rescued would wake soon and have answers. Why had the boat drifted ashore, something impossible given its state of disrepair, and why did it suddenly burst into flames once he was off the boat? Unfortunately, the answers would have to wait.