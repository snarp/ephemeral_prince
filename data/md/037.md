# Intermission - "The cold kingdom."

![](images/037_permanence_intermission.png)


**1.**

My father died even though I was once told as a child he never would; I only existed in the off-chance he became bored with ruling. He shouldn't have died, but one morning, he would not wake. No matter the clerics or priestesses I brought in to try something, he was dead. The whole kingdom was in mourning; he had been around since before most of them were born and he never let any of them come to harm. He was their king and now he lay dead. That was the way it was though, people are born and eventually they die. Even our kingdom would not be spared and again and again we would have to go through this.

There has to be a way to cheat death. We have lords and demons, why can't we find a way? Their oldest live forever it seems and grow to such great powers but we cannot? It makes no sense that we cannot be allowed to be free from that suffering. My father may already be dead, but I decided that I do not wish this suffering any longer on my kingdom. None of them should have to go through this and I do not wish to go through it again. After I was crowned king, I wanted time to mourn and so my father's advisor allowed me the time. I left for the western tower where my father kept all his magic tomes in, ones he had collected (or even stolen, he never told me where he found them), ones he himself had created, and ones I had been told my mother had penned and kept before my father took from her. Perhaps there was a way hidden within those tomes, those scriptures.

Months of searching, of scouring, and I found nothing. Spells to kill, spells to fry the very land of this island so nothing would grow, spells to make people forget, spells to control phantoms, but nothing that helped me. My father's advisor visited me for a time, but I don't recall what he said or if he helped with anything; he sent those clones to help me, but they never understood what I wanted. Death was something they simply didn't understand and I sent them back to the castle.

I don't want to believe that we all are doomed to a finite life while the lords on high and the demons down below can live as long as they want to. I can only stand by as my people die eventually and then soon I will follow the same route. The thought to burn the entire tower itself for misleading me rose as I was at the end of my patience, but then he came to me.

Like all lords, he had an unearthliness about him; hair, while dark, that seemed to shine, bright gold eyes that never seemed to blink, skin so perfect it was a wonder it was real at all, and dressed in clothes that seemed to give off its own light. I've only met one when I was a child before I came to the island, but she did not strike fear into me when she looked at me; from her I felt warmth I had never felt again. From him, something close to death I would guess.

"Do you need help?" the lord asked, his voice sending a shiver down my spine.

"My father banished lords from here a long time ago," I reminded as I tried to quell my fear. "What business do you have on this kingdom?"

"I do not desire men to fill our armies, don't worry, your Majesty," the lord said, a smirk tugging at his thin lips. He crossed his arms and drew closer and I found my feet could not move, from my own fear or something he did, I do not recall. "I saw you from above. You looked so distraught down here."

"And why would you care what I felt?"

The lord laughed and it echoed throughout the tower. "Do you dream that your kingdom shall never die?"

I took in a sharp breath; had he really surmised that from just a moment of seeing me? They couldn't read minds as far as I could recall, but the lord's eyes bore into me, waiting for any kind of an answer. I gave him a nod; he was a lord after all. You don't lie to them.

The lord unfolded his arms and looked at a few of the books I still had scattered everywhere. He almost looked amused at my mother's writing and sighed at what my father had wrote down. "Did you know there is a demon that can make dreams reality?" the lord asked without turning to look at me.

"There are demons that can move whole cities."

Another laugh and the lord looked back at me. "He is called the King of Dreams and he can make your dream into a reality if he so wishes," he explained.

"Why are you telling me this?"

"You could summon him to help you."

"And what would that get you?"

The lord shrugged as he returned his gaze to the texts and scriptures on the floor. "I would finally get a throne up on high," he said quietly. "And you would get what you so wish: a kingdom that never feels the pain of dying like your father or anyone before them." He turned back to me and held out his hand. "I could help you, I know the demon's scripture of life, and I can draw it for you if you like."

It fell right into my lap, I would be stupid not to try. What do I care of what the lord gains? If he doesn't hurt my kingdom and I don't have to give it up to its finite life, why would I not agree? I took the lord's hand and a sick smile spread across his lips. The barest touch and he pulled his hand out of my grip and turned to the room. He lifted his hand and the tomes and scriptures flew into the air and shot to opposite directions. Light came from the lord's fingertips and the light started to dance across the room and as it came in contact with the walls and floors, symbols began to emerge. Eventually I had to move closer to the doors or the symbol would have been drawn right on top of me.

"Is there anything I should know about this demon?" I asked.

"He's tricky," the lord replied and he glanced at me. "Please do not distract me, this will take time to draw. Come back in a week and I shall be finished."

A week? I stared at the symbols already on the floor in astonishment; there was already a great deal created and it would take a week to finish it all? Was this really worth it, I wondered. I was playing into a game I had no need to bother with, but the thought of my kingdom never dying was too loud for reason. They never had to feel death again. I never had to see them disappear one by one and replace with faces I could scarcely recognize.

I left the lord in the tower and after a week of sleepless nights, days I could hardly remember, I returned. The room had changed drastically; not a space had been left uncovered. Symbols and magic circles took over everything and glowed as soon as I stepped into the room. The lord stood still in the middle of it all and he turned to me and smiled.

"Are you finished?" I asked.

"Like I said," the lord replied and walked up to me. He placed his hands on my face and once again, a shiver went down my spine. His eyes glowed momentarily and I felt something electric rush through my body. As he stepped away, I staggered in my spot. "Now if you ever need to recall this symbol, you can perfectly," he explained.

"W-why would I need to?"

The lord shrugged. "I needed it out of my head or I could get into trouble," he said. "How you use it is up to you." He turned back to the center of the symbol and beckoned me over. "To summon him, place your hands on the symbol and speak his name."

"What's his name?"

"Xiri."

And the lord was gone like he had never been there. Any trace of his presence lifted from the room and I almost thought it had been an elaborate dream. The symbols still etched into the floor told me otherwise and I took a deep breath. Again I'm sure the thoughts that this was too risky came to mind, but I ignored them and placed my hands on the symbol. It glowed brightly from my touch and I closed my eyes and whispered the demon's name.

"Xiri."

Power raced from my fingertips and the scripture changed color, symbol by symbol, line by line, until the whole room was covered in a bright light. The air in the room swirled and soon the symbols became so bright I had to shut my eyes tight. I began to feel faint as though my strength leaked out from my fingertips and fueled the symbols before me. The thought to pull my hands from the symbol crossed my mind, but I stopped when a laugh filled the room.

The wind died and the bright lights washed away; a throne had appeared, made of ice, and a young man who looked scarcely older than me, sat in it. His hair an unnatural red, his skin paler than even those who were sick, his grin one I'm sure delighted in seeing the fear in my eyes. Like the lord, his presence was unearthly, but he was clearly no lord; his eyes told as much. They were a blazing red, brighter than his hair, but the sclera were a pitch black. A demon. The King of Dreams. As soon as the room had settled, I stood back up and stared at the demon before me.

The demon only considered me a moment before he hopped up from his throne and strode over to me. Though he was a good few inches shorter than me, he didn't look intimidated in the least.

"Do you know how cold it is here?" he said. "Look at my throne, it turned to ice! I need a cloak or something or I think I'll freeze myself back to the land of demons. I bet you don't even feel how cold it is here."

With nothing else on my person aside from my own cloak, I took it off and offered it to the demon who took it without question and wrapped it around his shoulders. He patted the dark fur around the hood and whistled.

"How gracious of you!" he said, a mocking tone rising in his voice. After he had secured everything to make sure the cloak wouldn't slide from his shoulders, he looked back to me and gave me a once over.

"Are you Xiri?" I asked.

The demon laughed. "Who else would I be I wonder?" he asked. He walked around me, nodding as he went. "I never knew a human could summon me, it was all so surprising. You were so neat with the scripture even," he said.

"What do you mean?"

Xiri stood before me and readjusted the cloak. "You don't even know what the conditions are for summoning demons?" He laughed when I gave him a blank stare. "You have to match me in spirit and power--lucky for you, you have both or I could have fried you for wasting my time."

My father had been the best mage in the land, and my mother had been the best priestess Barina had to offer my father. Even being born from them, to think it could be on par with a demon like this was almost hard to believe.

"Too bad most of your power is blocked away because of human's finite power," Xiri lamented as he walked back to his throne. He sat back down and looked at me with a grin. "But it's there and that's truly fascinating." He leaned back in his throne and stretched his arms. "What do you need? I'm here to serve."

I still stood dumbfounded--what do you say to a demon who is powerful enough to grant dreams?--and the demon laughed at me and jumped back up. He slapped me lightly on the cheek as he stood on his toes to be level with me.

"Don't give me that look," he said. "Tell me so I can help you out already. I have time before everyone needs me back home, but I don't want to play for too long."

"Need you for what?" I asked.

"Nothing you need to worry about," Xiri said. "Out with it already."

I never thought a demon could be excited at what a human would say, but he listened to every word I spoke without comment. When I finished, he crossed his arms and nodded.

"I don't think I want to die either," he said quietly. He smiled. "But is that all? You don't want a warmer place to live? Away from all this cold weather? Won't that make everyone happier?"

"What do you mean?" I asked.

"That's what you really want isn't it?" Xiri said. "Wouldn't you like a warm place, like paradise, instead of here? You can stay here if you really want, I wouldn't, but wouldn't it be better if you had your own place far away from anything here? So you're never bothered by anyone?"

"Do you think it would be wise? To cut off everything else?"

Xiri nodded. "Of course it would be. Who needs everything else?" He held out his hand and his smile widened. "Do you want it bad enough to make a deal with me? I can give you anything you want."

No need for thought; a paradise for my kingdom where they never die. Yes, that was my dream, and I took Xiri's hand in mine. An electric feeling flew into my hand from the demon's and I could hardly stand steady while Xiri stared at me with his grin. The cold air washed away from me and the fatigue I had felt from the week of sleepless nights washed away and I felt as though I could do anything. When it stopped, Xiri took his hand away, leaving me to stumble to my knees. It all felt unreal and I could hardly believe it, but Xiri knelt in front of me and smiled at me once again.

"A little bit of me to you," he said. "Now, would you like me to get started?"

**2.**

From out of nothing, he made a floating island. Within days it started to float around my kingdom and one could only see it on clear days. At first, he would not allow me to go up, saying he had a few pests to clear out. Even still, after it had appeared, he had to rest and slept a whole week away in my chambers, only waking for brief moments, away from prying eyes and safe and sound.

As he slept, my people sighted the island and were astonished to find it floating along just above the clouds. Many came to me with questions and were elated when I told them that one day they would all live above the clouds, never to be bothered by the world, to always be happy and warm. My advisor wasn't as elated as my people, but he had always been a cautious one; a demon living in the castle put him on edge, but I promised him Xiri would do him no harm.

When Xiri awoke, he was gone for merely a day and when he came back, he eagerly wanted to show me everything the floating island had to offer. He had managed to create a portal in the empty Shrine of the Holy. It was just beyond the statue of maidens who once carried words from my mother to me (though they had fallen silent a year before my father passed). As the demon began to step into the portal, he stopped as though a thought had occurred to him and turned back to me.

"You should close your eyes," he said with a grin.

I obliged the demon, and he took my hand and led me into the portal. Warmth surrounded me and I felt weightless for a moment. When my feet touched the ground once more, I began to open my eyes but the demon covered them instantly.

"I didn't say you could look yet, it's not that impressive in here," Xiri said quickly. When I sighed and nodded, he took his hands away from my eyes and grabbed my hand once again.

As we descended staircase after staircase, whatever we were in filled with warmth and already I enjoyed it more than my kingdom. A set of double doors creaked open and Xiri pulled me through and let my hand go free. I opened my eyes and what a sight it was. Lush gardens, grass so green, the climate so warm, I could not believe it really existed.

Like a child just waiting to show off something he had made, Xiri showed me everything on the island, from the almost too immaculate tower, to the small town with a house for everyone imaginable. It sat right in front of a lavish hedge maze where flowers grew wild and statues had been erected to help point the way. Through the winding maze (Xiri laughed at the thought that I would surely have lost my way in the maze on my own), Xiri led me into the castle fit just for myself and my subjects.

"You can even rearrange it at will, if you want," Xiri explained as we stood in the entrance chamber. He held his hand out in front of himself and the room began to change; a staircase appeared, and new curtains hang down from the ceiling.

"How can I do that?" I asked.

"I gave you what I could do, just give it a try," Xiri replied. "Just imagine what you want and it will be."

Easier said than done; Xiri laughed at my feeble attempts to try--I only managed to change one curtain--but he promised he'd show me how to change it later. When I had finished with it, Xiri led me back outside, but stopped abruptly.

"Wait, I almost forgot!" he turned to me quickly, still a grin spread across his pale face. "I made something else to, just for you and whatever family you ever wish to have."

"More than this castle?" I said and laughed. "You might be spoiling me a bit."

That didn't deter the demon. He closed the doors of the castle and placed both palms on it. He muttered something I didn't catch and the doors flew open once more and the entryway changed. Pools with lily pads sat on both sides of a long, pristine staircase which reached high into the ceiling. Xiri pulled me inside and we climbed the stairs together.

A momentary darkness filled my vision as we were but halfway up, but then light flashed before us and I couldn't see for a moment. The sight that waited for me, however, left me stunned in my spot. I do not know how, but we were even higher in the sky in an elaborate airway, all pristine and white. Even more like paradise than before.

"Where are we?" I asked as I looked back at the stairs we had emerged from; they descended into a void and I could not see down into the castle from before.

"I'm not sure," Xiri said as he crossed his arms. He whistled and a small winged creature came into view and flew around him. "You dreamt it so I just made it happen." He poked the winged creature and it flew away from him and went in circles around me, whistling happily.

"I did?"

Xiri nodded. "You don't remember?" he asked. When I shook my head, he shrugged and spread his arms. "This is just for you. A place to relax, a place to learn more magic, whatever you want to do with it. Just like the castle, it has a place for you and a family if you wish to have one."

It was amazing and Xiri only grinned as I explored everything I could. This wondrous place was just for my kingdom and here we would never have to worry about anything else. With haste, we returned to the kingdom and I had to show my advisor everything that Xiri had created. He was hesitant at first, but when I explained to him how everything looked, how everything felt, he smiled and followed me and Xiri to the shrine.

As we stepped into the teleport and emerged in the tower on the floating island, my advisor turned to dust next to me. His clothes remained, but anything he had once been became a pile of dust beside me. It must have been a joke, I thought to myself as I stared at what remained of my advisor. Xiri's expression told me differently; he stared at it, his eyes wide and his grin gone.

"What happened to him?" I asked.

Xiri didn't respond, he just stared at the pile of dust.

"Can I even bring other people up here?"

"Everything was made to your vision," Xiri snapped. "Perhaps you have no idea how to get him up here and thus he turned to dust."

"You knew this would happen, didn't you?"

"Do you really think me that cruel?" Xiri asked as he crossed his arms.

"Then what should I do?"

The demon had no answer. This paradise in the skies and I could do nothing to bring my people into it. We returned to the kingdom and Xiri did quick work to make those who were waiting for their paradise simply forget about it as we knew it would not happen soon. I did not want to keep them waiting and have that desire for paradise turn to malice toward me. I had the advisor buried and life went on in my snowy kingdom, but I did not give up. Every day I trekked back to the western tower with Xiri at my heels as we tried to search for a way to get my people up there. I only had to understand how and Xiri could take care of the rest. Xiri would bring me books from afar, books that pained him to touch (as lords had written them), books in languages I couldn't understand until Xiri taught me how. There had to be a way.

Months went by and we still had no answer. Xiri remained my companion through it all (out of a friendship with me or the fact that it was something he could not do I could never figure out which) and I believe that had he not been there to help me, I surely would have gone mad. Even on days we couldn't find anything, I think part of me enjoyed his company. But as time went on, no matter how he tried to make me happier, my remorse grew. Part of me wished he had never made that floating island, but at the same time, I doubt it would have mattered. I'd still be here, searching for a way to allow my kingdom to escape death. There was no answer to my problem and I had begun to think all of this was a waste. They would not live forever and I should steel myself to remember this fact and do what I can to make what I had now work for the best.

It proved too hard to do that.

One day, Xiri left me alone in the western tower and once again, I poured over what books Xiri had collected for me. An answer was probably hidden somewhere, I just had to make the right connections. Perhaps I had gone mad by then; I must have read those texts thousands of times, surely I could have recited them from memory, and I still expected the answer to be right there, plainly spelled out for me. But it had to be somewhere, I knew it.

That's when he returned to me, just like before. The lord had not changed one bit.

"How are you faring?" the lord asked, his words startling me out of the passage that I had been reading. He looked over my shoulder and chuckled at it. "Has Xiri been a help?"

"He made a paradise," I said as I sat back in my chair. "We just can't get anyone inside of it or they turn to dust."

The lord laughed. "And you believe that Xiri isn't capable?"

"He told me that if I don't know how, he can't do it."

"Isn't that curious? If you knew how to do it, what need would you have of him?" the lord asked. "He's tricking you; he knows very well how to do it."

I stared at the lord in shock and once again, he laughed at me. "Do you honestly think that your time together means anything to that demon?" the lord asked. "You're just his plaything until he becomes bored, he has no interest in helping you achieve your real goal."

"But we made a deal," I said.

"So? What benefit does Xiri have for going through with the deal? He won't give you what you want unless there's something in it for him."

"Then what do I do?"

The smile on the lord's lips should have made me run away, but at the time, I hadn't even noticed it. I had been too blind with the idea Xiri had tricked me and I had allowed it to happen.

"You have to eat Xiri and all his powers shall be yours."

"Eat?"

"Do you not understand?" The lord crossed his arms and watched me.

"How would that even be possible?"

"Lords and demons have no bodies of their own in the same sense as you do," the lord explained. "Your bodies give you limits so you can never realize your true potential. My body is a mere clever disguise to hide my core which holds everything about me and allows me that infinite power and demons work the same exact way."

"How would I eat the core?"

"Just grab it and eat it," the lord said and laughed. "If you get Xiri to rest within your body, all his powers will be yours to use as you wish."

I looked back at the books I had been searching through. Was my answer simply to devour Xiri and use his powers as my own? Wasn't what he did now simply that? I looked back at the lord and found that he still stared at me, waiting for me to agree. There was something not right about this. First he had me summon Xiri and he left me alone for months. Why show up now?

"Why are you adamant to get rid of Xiri?" I asked.

The lord's face twitched for a moment, but he covered it up with another laugh. "I was tasked to be rid of Xiri. Simply having you as his distraction didn't count. He can kill me with a snap of his fingers so going toe-to-toe with him isn't a good idea," he said. "I'm not stupid enough to put myself in harm's way."

"But you'd put me."

He smiled at me and I remembered: why would a lord care about me or anyone else for that matter? They ruled the world as they sought fit; my father had been able to escape their influence and they had left me alone, likely afraid I could do what he did. He didn't care what I could do for my kingdom. He only returned for one reason: Xiri was still around and at any moment he could return home and mess up whatever the lord's plans were. I must have been quiet for too long for the lord reached forward and patted my shoulder.

"It's your choice," he said. "Let your kingdom live and eventually die, or take matters into your own hands and force Xiri to do what he has kept from you."

With that, the lord was gone. I still felt the weight of his hand on my shoulder, but he had left. Everything I had in the tower held no answers for me and I left the tower, lost in thought. My goal was to save my kingdom from its eventual death and all I had to do was devour Xiri and make it so. I didn't summon him to have a friend, I summoned him to help me and he had not. He had tricked me. Perhaps I was desperate and anything sounded better than letting my kingdom live out its life and leave me, but the lord's voice stayed in my head. I just had to devour Xiri and I could do it. I believed the lord.

When I returned to the town, it was after dark, and I sighed. The kingdom looked the same as it had been, and there were faces I still recognized. I wondered how long it would last before those faces disappeared and were replaced by people I could hardly remember. Everyone was turning in for the night and the guards greeted me happily; nothing had happened while I was away. All at peace and I walked through the town and went toward the castle.

As I approached the castle doors, Xiri appeared beside me as he was prone to do. On his face was the same grin, but he was more excited than usual.

"I just witnessed the birth of a little girl," he told me as he followed me into the castle. He laughed when I looked at him. "The whole process was so ugly! I'd never seen the birth of a human before and I didn't realize it was so messy."

"Did they even know you were watching?"

Xiri paused and thought for a moment before he laughed again. "I don't think they did!"

His continued laugh put a smile on my face and he told me all about the little girl as we went through the castle. A few attendants watched him worriedly, but like before, they soon became complacent at his presence like usual.

"Were you able to think of how to bring your people into paradise today?" Xiri asked as we entered my chambers.

I sighed. "I have not."

Xiri frowned as he leaned against my desk. "Maybe it's the connection you have with your body," he suggested. "Bodies don't live forever and your souls and minds are attached to your body."

"What about mine? I can go on the floating island."

"That's because I'm here and I keep you from decaying or growing old," Xiri said. He paused and crossed his arms, suddenly deep in thought. "I wonder if it's partly the want to live forever and you obviously can't because your bodies decay..."

"What's your body made of then?"

Xiri shrugged. "I don't know, I never thought about it," he said and grinned once more.

"But you can be hurt?" I asked.

"Unfortunately."

"Would you die?"

"It depends really," Xiri said and he shrugged his shoulder. "If you do such insurmountable damage to my body, eventually it will kill me, but it's very hard to do that to a demon." He smiled at me. "You really just have to make demons vulnerable with the pain and that's all there really is to it."

"Really?"

"All demons and lords are made of energy spinning inside of us," Xiri explained slowly. "It's made up of our scriptures of life and as long as it spins, we stay alive and we keep going. If you were to strike the core, our bodies would break down and we would die."

"Too bad humans aren't made like that," I said with a sigh. "Then maybe we'd be able to get up there with everyone else."

Xiri nodded slowly and for a moment, he was lost in thought. When he looked back at me, he stepped away from my desk, and flashed another grin. "Do you want to see my core?" he asked.

If only it hadn't been that easy, maybe I wouldn't have followed the lord's advice. I stared at Xiri in shock and the demon chuckled.

"I'm sure you've never seen one before," he said. "It's actually very pretty."

I didn't even have to tell him I'd like to see it. He wanted to show off. He lifted up his shirt and closed his eyes. Bit by bit, his stomach started to dissolve and beyond it, a bright swirling bit of light appeared. Symbols lit up within the core and I stared at it, wide eyed.

"Isn't it bright?" Xiri said, his eyes still closed. "My friend told me once it was the brightest she'd ever seen."

This was all I needed to allow my kingdom to live forever. To never die like my father did, like everyone else eventually. It was there. The choice was mine and I took it.

Xiri's eyes snapped open when I grabbed his core and ripped it from his stomach. Only a strand of symbols kept his core connected to him but his legs buckled underneath him, his face contorting in pain. I was too focused on his core in my hands to even notice. It was bright, warm, and pulsed as it sat in my hands.

"LET IT GO!" Xiri screamed, his voice shrill as he tried to lunge at me. I stepped back quickly and Xiri fell to the ground, barely able to hold his own weight. All his power was in my hands and he screamed at me, pleaded for me to let it go, to give it back. But I had made up my mind.

The core slid into my mouth easy enough, but it burned my lips as it passed and it felt as though fire slid down my throat. I almost thought my whole body had been engulfed in flames as the heat raced through me, but there was no fire. It was Xiri's power. Xiri screamed, but his body had disappeared as though it had never been there. I almost wondered where his scream came from, but I had not the time to think on it; my knees gave way underneath me and I fell to the floor. My arms flailed out and I tried to stop my fall, but I only ended up knocking everything off my desk. Pain raced through my body and something was coming up my throat and it burned. I tried to hold it in but I could only for a moment; onto the floor I vomited something dark, but as it slid onto the floor, it began to burn. Guards rushed into my chambers and rushed up to me.

"Your Majesty--what's wrong?" one of the guards asked as he tried to help me stand.

The dark sludge kept coming out of me and I had no idea what I did, but in moments, the guards shot out of the room and everything in my chambers suddenly shot up in the air. The doors slammed shut and I was left alone. Alone as my body felt aflame, burning everything around me. The flames weren't real, however; they raged on, but nothing changed.

"Please, Xiri!" I screamed as lightning started to shoot off from my fingers. My magic worked on its own and it destroyed everything around me. Small fires lit up the curtains and I gripped my hair. Anything to make it stop. "Just quiet down! PLEASE!"

"You tricked me!" Xiri screeched, his voice filling the room. A man shrouded in shadow appeared before me, where Xiri's body had been, and he reached out and grabbed my collar and lifted me up. "The lords put you up to this, didn't they?"

How did he appear before me? I tried to get out of Xiri's grasp, but he held on tight, his grip began to burn my clothes.

"I knew I smelled one on you, but I trusted you!"

The room began to grow dark. More dark sludge came from my throat and I could hardly breathe. It went down my chin and burned my skin. Everything was on fire and soon my vision blacked out and I fell into the darkness.

It must have been days later, but I eventually awoke. My chambers looked just as before. I wondered if it had been all a dream, but I knew it wasn't. I felt different. As I stood from my bed, I realized that I no longer felt pain. Magic came with ease. Xiri had settled within me and I could use his powers as I wished. I had won. The day would come soon I would be in paradise with my kingdom and we would have no fear of death.

Or so I had thought. He was inside of me and I could call upon his powers as I wished, but people still turned to dust as I brought them to the floating island. Xiri laughed inside of my head, mocking me. I still didn't understand and so I could not do anything. I had been tricked and it hadn't been by him.

Days went by, months went by, and then perhaps years went by. I began to forget the faces of my people and again, the demon mocked me. The lord never came back, but I'm sure he laughed at me as well. I had fallen into his trap and I had lost. My people could not go into my paradise.

One day, I don't remember how long after I devoured Xiri, he started whispering to me. Long gone were the days he mocked me; I'm sure he grew bored of it.

"What's in the way, my dear king?" Xiri whispered one night. "But your bodies? They're trapped in their finite world because of their bodies. Release them, and everything will be infinite."

And I believed him.

No one is left on my island but me now. Their bodies long since buried and everyone is inside of me, to live forever like I wished. With no bodies to impede them, they have broken free of the path laid out for us. Xiri tells me how they're doing from time to time. All of them sleep soundly trapped within me and Xiri tells me of their dreams. One dreamt of his two little children but I did not recognize their faces. Nevertheless, at least they're safe and will never die. Soon I will figure out how to get them out of myself so they can live on their paradise. Soon I know they will.

How long has it been, I wonder? I stopped keeping track of the days as I found myself forgetting the days more and more. I am able to bring animals up to the paradise now, though, that I remember quite well. A spell my mother had penned that I had not thought to use as I had been too preoccupied with the thought to have them live forever. On my paradise, the animals did not live forever, but they did not turn to dust. Perhaps I shouldn't have listened to that lord? While the animals still died eventually, they lived their small lives happy in the warmth, but they always pass away eventually. Not like it mattered, it was no use to me now. Inside my head, my people lived forever, but asleep with no bodies; they could not enjoy the paradise that had been created just for them. The town had decayed. Somehow it had disappeared and I tried to figure out why, but I gave up eventually. It wasn't like it mattered.

I'm not alone on the island even though it feels that way; the statues in the shrine speak to me again. It is not my mother's voice, but someone else. Perhaps they always spoke to me, but I had ignored them. Some tell me what I did was wrong, but what did it matter now?

I sealed my island beyond the stars and destroyed the port one day after I found someone on my island. The face hardly recognizable, I think I buried him somewhere (I wish I could recall where). No one is allowed to come onto the island; the buildings aren't theirs to occupy, the land isn't theirs to fill up. It belongs to my kingdom and awaits the day they return. I know it will happen soon. I have dreamt it. Everything is kept just like they left it and I make sure of it. Just for that day I can bring them back.

The stars were a nice addition to the sky. Often I'd lay outside (I didn't feel the cold any longer) as I tried to think and I would count the stars, one by one. Each one for those that had been in my kingdom. They all had names but I don't recall them now like I don't recall their faces. Xiri laughs at that.

Time had no meaning for me any longer. Day in and day out, I tried everything I could. I'd count the stars, I'd go over what I had written, but I could not get them out of my head. I'm sure years and years went by. I even thought to free Xiri (perhaps he'd help me bring everyone out?), but I did not know how (and I wouldn't dare try calling a lord to get him out of my head), and if I did, the thought grew in my mind that if he were gone, the people in my head would die. He would surely kill me if I allowed him out. As much as I deserved that fate, I still do not wish to die.

No, he has to stay there and keep watch over them until I can free them all.

One day, after I had dusted the cobwebs off my body (had I fallen asleep again?), I realized a ship had come through the stars.