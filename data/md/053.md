# Chapter 23 - "Uneasy thoughts."

![](images/053_permanence_23.png)


The forest had overgrown with weeds and roots standing high and Snowe and Erio struggled as they moved through it. It was all too apparent no one had been in this deep in a long time; though there appeared to be a road, it had disappeared underneath the brush. The late morning sun shone overhead and its rays filtered through the autumn leaves above, but even with the sun, the cold breeze kept any warmth from Snowe as he followed Erio. Ever since they started, they hadn't seen nor heard anyone else around them and the trees remained quiet aside from the leaves that rustled in the wind. Even the animals had abandoned the woods.

As Snowe kept up with Erio and tried to hide how each limb trembled with the cold, he found every part of his body racked with pain and all he wanted to do was rest. He could hardly move his hands; Erio had healed them up as far as he could and had even torn up his sash to use as makeshift bandages. Nothing stopped their sting as Snowe moved his hands, nothing stopped the pain that throbbed as if to remind Snowe what he had done. Snowe had wiped the blood from his face but it still felt as though it remained. He'd even wipe his face with his arm just to make sure it was still gone.

"Y-You c-can go back to Astra if you want," Snowe said, breaking the silence. Erio stopped ahead of Snowe and when he looked back at him, Snowe gave him a small smile. "I-I mean to t-tell her what happened to us."

"I'm not going to leave you in the middle of nowhere," Erio replied. "We'll get someplace safe and then I'll get back for a few to fill her in."

"Probably is for the better I stay out of Norin, huh?" Snowe asked.

Erio paused before he took a few steps back to Snowe and he nodded. "With what Xiri pulled, it's best if we lay as low as possible," he said. He turned back to the area and folded his arms as he glanced around. "I don't even think we're in Norin right now--our trees haven't changed like this yet--so that's a plus. It would help if we knew where we were though."

A breeze blew through the trees and both Snowe and Erio shuddered before they started their trudge forward again. Erio helped Snowe over a fallen tree that had weeds and mushrooms growing up and around it and as they moved past the large branches that still stood tall, they came across a clearing where a lone scarecrow stood proudly with autumn flowers blooming at its base. Snowe gave a pause as they neared it but Erio kept moving; the scarecrow had no expression on its burlap sack head and its hay had already fallen out of its sun-bleached clothes. A scent Snowe couldn't place wafted off the scarecrow and he watched it.

"Don't start talking to it," Erio warned as he glanced back at Snowe.

"I-I don't think it's that kind," Snowe said. He leaned toward the scarecrow and sniffed it before he looked at Erio. "It smells like those candles you had in the apothecary shop in the summer."

Erio came back and after he sniffed as well, he nodded. "I wonder if the smell alone keeps phantoms away," he murmured to himself.

They took another breather near the scarecrow and as Snowe took a deep breath, he winced as pain reminded him it still remained. All he wanted to do was collapse on the ground and nap--in the middle of nowhere or no. He flinched as he felt Erio took his hand and pulled it up.

"Do they still hurt?" Erio asked as he peeked underneath the makeshift bandages.

Snowe shrugged. "T-They're just h-hard to move," he said. "Y-You really can't heal them anymore?"

"I'm worried if I get the skin to heal, it'll have an infection in it or something," Erio admitted. "Even still, if I heal it, it might not let you have use of your hands like before. We need to find a healer who specializes in this stuff. I don't want to accidentally screw up your hands."

Though Snowe just wanted it fixed, his hands healthy in the end or not, Snowe nodded and took his hand away from Erio. He pulled at the bandages before he sighed. "I'm sorry you tore up your sash."

"It's not a problem," Erio said. "Come on--we should get moving again."

Their trek fell into silence as they continued on; the brush and grass became shorter as they moved away from the scarecrow and after Erio helped Snowe around thick bushes with bright berries ready to be picked, they found the road. Though they shared a celebratory smile, neither had time to figure out which direction to go first; a pair of horses came into view from their right and Snowe and Erio jumped in fright as the horses came to a sudden halt.

"What the hell are you two doing?!"

Erio shoved Snowe behind himself and placed his hand at his dagger against his hip, but all that stood before them were the horses pulling a covered wagon that had seen better days. A middle-aged man in a thick cloak sat at the driver's seat with the reigns in hand and sitting beside him was another man who had a chest plate much too large for him strapped around his torso and a similar cloak around his shoulders. He had a spear in hand and looked ready to use it. Though as he got a better look at Snowe and Erio, the man in armor let out a breath and sat back.

"Charging out on the road like that from the bushes." The driver shook his head and the man in armor patted his back. "Should be glad my horses are old; weren't going fast."

"What are you two doing coming out of there anyway?" the man in armor asked. "Nothing out there but trees and weeds."

"Our village was attacked by phantoms," Erio lied quickly as he took his hand away from his dagger. "We've been fumbling through the woods all night to find a road."

The driver and the man in armor exchanged a suspicious glance but if they had seen the lie, they chose to look past it. The driver looked over Erio's head and frowned as he caught sight of Snowe. "You look pretty worse for wear."

"Is there anything nearby where we can rest?" Erio asked.

"We're actually headed for an inn right now," the man in armor pointed out. He nodded toward the wagon. "We rounded up survivors earlier this morning from another place that was attacked. The inn provides food and shelter for anyone who needs it, free of charge for a little while."

"Survivors?"

"Other villages attacked by phantoms," the driver said and he leaned forward. "The capital doesn't have enough soldiers to help; they're all keeping an eye on the farms. In this area alone, most of the villages that used to be in the woods have been wiped out."

"And that's not even counting what happens to those who find bandits taking advantage of the chaos," the man in armor added quickly. "Best to just find a nice big city to stay in and stay there until this phantom mess is sorted."

"Do you know where the phantoms are coming from?" Erio asked.

Both the driver and man in armor shrugged. "Your guess is as good as the top researchers in the capital," the driver said. "About a year or so ago they just started to appear from out of nowhere and the number just keeps growing."

Though Erio glanced back at Snowe a moment, Snowe kept his eyes on the driver. He knew why. He trembled at the thought and tried to will it away as the man in armor handed the driver his spear and slipped off the seat.

"Come on you two, we can take you to the inn as well," he said. "I can help you get on."

Erio led the way forward with Snowe close behind him and he followed the man in armor as he took them around the wagon.

"When you're in there, make sure to grab Tellah--we picked her up from a small farming village near the river that got wiped out last night," he conversed. "She was a doctor there and she's good at making the pain go away. I think she's the only reason anyone's still alive from there."

After the man in armor had helped both Erio and Snowe into the wagon, he returned to the front and Erio and Snowe looked at everyone else already in the wagon. Men and women were huddled inside in groups and looked as exhausted as Snowe felt himself. They avoided any eye-contact with others and only spoke in quiet conversations with those they must have known before. As Erio and Snowe settled with their backs against the side of the wagon, the horses at the front started forward again.

"You were attacked too, huh?" a young man sitting across from them asked as he looked up from his group. He had one of his eyes covered in a bandage and his arm in a sling. "Anyone else escape?"

Erio shook his head. "Not that we know of," he whispered. He shifted in his spot uncomfortably and the young man looked back to his group as a little boy pulled at his shirt.

A woman with a loose scarf around her head moved out from another group and as she made eye-contact with Snowe, she gave him a smile. "You look like you could use some help," she said.

"Are you Tellah?" Erio asked.

"That I am." She sat in front of Snowe and she gently took up Snowe's hands and peeked underneath the make-shift bandages. "Unlucky for you, I'm not a water mage; can't help with this. I've heard there is a healer at the inn who definitely can." She let his hand go and she reached forward and touched Snowe's cheek. "At least I can help the pain for a little bit."

Warmth trickled its way through Snowe as it came from Tellah's hand and it felt like fire magic Snowe used to do. The warmth only lasted for a moment and once it had gone, the pain all across his body dulled to something more manageable. It must have shown on his expression for Tellah gave him a grin and she turned to Erio.

"Your turn!"

Though Erio looked uncomfortable as Tellah placed her hand on his face, he let out a sigh of relief and relaxed his shoulders. By the time Tellah took her hand away and gave him the same smile, a baby from one of the huddled groups wailed and Tellah quickly excused herself and moved back to her group. The baby soon hushed and the wagon fell back into quiet conversations.

"You can sleep if you want," Erio whispered after a few moments as he leaned toward Snowe. "I'll wake you up when we get there."

Sleep sounded like a treat and even though Snowe had wanted to fall asleep even if for a short nap, it never came. All that went through his mind once he had his eyes closed kept him fully awake and afraid. The images wouldn't stop and Snowe kept his eyes open if only to keep the images at bay.

The wagon stopped at the inn along the edge of the road just after noon and it looked as though it was the only building for miles. It stood tall nestled against the trees that grew close around it and as Snowe slipped off the wagon, he could smell lunch from the road. After everyone had come off the wagon, Erio and Snowe followed the group of survivors into the inn with the driver and the man in armor. The workers at the front desk moved quickly to grab clean clothes for everyone who needed them and helped those too wounded to move far on their own. When Erio and Snowe got to the desk, the middle-aged woman smiled at them.

"You two look healthier than most," she said as the younger woman beside her handed Erio and Snowe some clean clothes. "Did you two want a room together?"

"That's fine," Erio said. "Is there a bath in the room?" he asked once the woman handed him a key.

"Sure is--would you two like some hot water too? We have to turn it on down here," the woman explained. When Erio and Snowe nodded quickly, she chuckled. "All right then, I'll tell my workers in back to turn it on for your room. Lunch'll be ready soon so if you're hungry, feel free to come back down and grab some food."

After a quick thanks, Erio and Snowe went to find their room; it was on the second floor and at the end of the hall and once they reached it and entered, Erio breathed a sigh of relief.

"You wanted a bath?" Snowe asked as Erio closed the door behind them.

"Well, at the very least you should have one," Erio pointed out as he put his fresh clothes on the bed in the middle of the room. "I'm sure it'll feel nice after everything you've gone through." He looked at Snowe. "While you do that, I'll find out exactly where we are and then I'll fill Astra in. Are you going to be all right here alone?"

Snowe nodded quickly as he moved to the bed and put his clean clothes down. "I will."

Erio moved to leave the room, but he stopped as a knock came from the door. Hand back at his dagger, Erio pushed Snowe away from the door and opened it so he could peek out.

"Is this the room with the young man with burned hands?" a young woman asked.

The voice sounded familiar and as Snowe tried to place it, Erio opened the door fully. As Snowe looked at the young woman in the hall, he felt his voice caught in his throat; she was familiar beyond a doubt. She had red hair tied into a high pony-tail that she kept back with a pale violet bandana with barrettes keeping stray strands of hair in place. She wore clothes similar to what Canan had worn at the temple and she had a large bag across her shoulders. Once she caught Snowe's gaze, she froze as well.

"Oh wow. You looked really familiar for a second," she said and laughed. "My name is Rhea."

"My name's Erio and this is Snowe," Erio introduced. He stepped back from the door and as Rhea stepped inside, he crossed his arms. "He's the one with the burned hands."

"I can see that!" Rhea said. "Well, I can't promise I can heal the burns in one session, but my mentor is out collecting herbs right now so we can make a salve and over the next few days if we keep applying it, your burns should be better in no time." She adjusted the bag on her shoulder. "I can at least clear it out to prevent infection. Can I do that?"

Snowe had no reason to decline and he nodded. With a relieved expression, Rhea stepped toward the dresser beside the door and she placed her bag on it. While she opened it to grab what was inside, Snowe couldn't help but think he was seeing a friend after a long time from being apart. Everything about her felt familiar as he had felt with Canan. If she felt the same thing, she kept it hidden and she pulled out a large jar of water and showed it to Snowe.

"This water has been enchanted to help clean out burns and wounds," she explained. She gave it a pat before she placed it on the dresser and reached into her bag again. A bracelet with a blue gem in the middle came out next and Rhea slipped that onto her wrist and once the gem glowed, she looked at Snowe again. "It's going to sting, but I promise it'll feel loads better soon."

"No pain no gain, right?" Snowe joked.

Rhea grinned. "Ah, I love hearing that!" she said as she opened the jar. "You would not believe how many people moan about the sting. It's like, I promise, it will feel better in the end!"

"Does the bracelet help you manipulate water?" Snowe asked as Rhea reached forward and carefully took his hands in hers.

"Is it obvious?" Rhea laughed as she slowly removed the makeshift bandages from Snowe's hands. As she looked at each finger and knuckle, Snowe had to look away; he couldn't deal with seeing them in their state. Rhea looked back at Snowe. "I've seen worse," she said. She let his hands hang in the air as she held out her palms above Snowe's hands.

The water from the jar moved as the bracelet's gem glowed and soon it came out of the jar. It pressed gently against Snowe's hands and submerged them as Rhea began to move her hands. The water flowered around Snowe's hands and with each movement, the water stung against the burns, but it also left a tingling sensation behind as though it cleaned the wounds. After a few cycles, the pain dulled and Snowe breathed a sigh of relief.

Rhea manipulated the water back into the jar and once it settled within, she closed the top. "I'll dispose of that later," she whispered to herself before she gave Snowe a wide grin. "See, feels better, right?"

"A lot," Snowe said and nodded. "Thank you."

"When my mentor comes back, we'll make a salve for you and bring it in the evening," Rhea said as she slipped the jar back into her bag. "I wish we had some already on hand, but I guess that's just how it goes."

"Thank you very much," Snowe said again.

Rhea rolled her eyes. "Don't say that too much," she joked. "I'm just doing my job." She opened a pocket on the outside of the bag and pulled out clean bandages. "And have these. They'll be better than a ripped sash I bet."

Once Erio took the bandages, she gave them both a smile again and pulled her bag up. "Take care and I'll see you two later." She reached her arms forward as though to give Snowe a hug, but she stopped short and laughed weakly. "Whoops. That was weird," she said. "I-I just felt like I should have given you a hug you know, and tell you everything would be all right." She dropped her arms at her sides. "I'll save it for when we know each other more, all right?"

She gave them both a quick wave and she left the room, closing the door behind her. Her shoes clacked against the wooden floor as she went down the hall and once the sound was too far away to hear, Erio placed the bandages on the dresser.

"I'll help you wrap your hands after your bath, all right?" Erio asked.

"Good idea," Snowe said.

"Just stay put here and I'll be back soon."

Snowe gave Erio a nod and though he looked like he had wanted to say more, he returned Snowe's nod instead and left the room.

Door closed again and alone, Snowe listened to Erio's footsteps down the hall. Far quieter than Rhea's and when he couldn't hear them any longer, he locked the door and turned back to the room. Small and cozy, Snowe knew it should feel warm, but it didn't. His body remained as cold as it had been and Snowe sighed to himself.

He took his clean clothes into the bathroom and found a large tub inside, big enough to fully submerge in if he wanted to, and a small sink and mirror with a cabinet of toiletries beside it. Though Snowe had wanted to be alone, the actuality of being alone made him afraid. More thoughts to will away, Snowe folded his clothes on the small cabinet near the sink and as he stood at the mirror, he caught sight of himself.

It made him sick.

His skin paler than usual, his eyes tired with dark circles forming again, and dried blood he hadn't noticed before still stained his skin around his right eye. The scar had healed over again thankfully, but it looked worse than before. His shirt fared the worst; more blood than he could clean out, he knew he couldn't salvage it. Snowe took his eyes away from himself in the mirror and he leaned over the bath to turn on the faucet. His fingers stung as he moved them, but once the water was on, he took a deep breath.

It didn't help.

Snowe sat on the floor and took another deep breath. He felt sick. He could hardly recall what had happened in Morzin. Or perhaps he just wanted to tell himself that. How many people did I kill because I wanted to live? Snowe asked himself as he leaned his back against the tub. The question alone made him feel worse and he curled up. He let Xiri have control and the demon had gone overboard.

But, what had he expected?

The thoughts wouldn't go away; he knew Xiri hadn't gone overboard. The demon had only a few goals in mind: kill Zuan (and by extension the executioner) and kill Astra. Xiri had set the scaffold on fire for sure, but that had been it. He hadn't set the other fires. The scaffold had been contained; it was merely to instigate fear, not to set the rest of Morzin on fire.

It had been Snowe and Snowe alone and he knew it. He had wanted control again after Xiri went after Zuan, but Xiri wouldn't allow it. All those fires had been Snowe's attempt to set himself aflame to stop Xiri, but it hadn't worked; Xiri simply sent the fire off in other directions and Morzin burned. All those thoughts swirled in his mind, reminding him, keeping him from feeling safe.

"Shut up," Snowe told himself. He pulled himself back up as he felt water drip from the sides of the tub and once he found the water had filled the tub to the brim, Snowe reached over and turned the faucet off. Another deep breath, but it didn't help calm him.

"I just need a nice hot bath," he whispered as he slowly peeled off his clothes. His shirt stuck to his skin and he hardly wanted to touch it before he discarded it on the floor. He didn't look at his body as he pulled the rest of his clothes off; he knew what it felt like, it would do his mind no good to see it. The charms Erio had made were long gone; Snowe didn't know where they had gone but he suspected Zuan had gotten rid of them.

Thoughts pushed to the side as best Snowe could manage, Snowe stepped into the bath. Hot to the point Snowe could hardly stand it, the water washed over him as he sunk deep into the tub. He took another deep breath. Breathe in--breathe out, just like before. In and out. In and out.