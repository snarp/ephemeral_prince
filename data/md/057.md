# Chapter 27 - "Restful days and a phantom so large."

![](images/057_permanence_27.png)


The sunrays from the window had moved across the room as the morning continued but Snowe stayed curled up underneath the blankets. Only when the sun finally shined into his eyes did he wince and pull the blankets over his face. It woke him up all the same and he realized he didn't feel as warm as he had before; he opened his eyes and found Erio had already woken up and he had left the room.

It's about time, Snowe thought to himself as he took a deep breath. For the first four days, Erio hadn't moved from the bed until Snowe was awake. The first night Snowe had slept through most of the day and though Erio had woken up hours before him, he hadn't moved and stayed right beside Snowe only because he had promised he would be there when Snowe woke up. He hadn't even complained.

As Snowe remained underneath the blankets, he ran his fingers along the pendant Canan had sent back with Erio. Like Erio's charms from before, it was very warm to the touch and it made him feel at home. Was it the one Beliaz had given Canan? Snowe thought to himself as a memory tugged at him. He could hardly remember what it had looked like and as he let the pendant fall against his chest, he supposed it didn't matter. Another deep breath; Snowe didn't feel like getting up yet.

The door opened just as he decided and Snowe peeked out from underneath his covers; Erio came inside, a tray in hand, and as he shut the door with his foot, he sighted Snowe.

"I can't believe it," Erio said. "You're up before noon."

Snowe laughed as he sat up and sniffed; whatever Erio had brought in smelled delicious and his stomach gave out a loud growl.

"You should probably eat while it's still warm," Erio said as he set the tray on the small table beside the door. He pulled out one of the chairs and sat down before he yawned.

Snowe slipped out of bed and he moved to the table, his stomach growling again. As he sat beside Erio, he had to hide a laugh; the demon had grabbed a lot. Oatmeal, eggs, pancakes, sausage--all in abundance.

"One of the workers was insistent," Erio said as he grabbed the plates set to the side of the tray. "She said we were too small and we should bulk up before it gets too cold."

Snowe laughed at the thought and before he could reach out to load up his own plate, Erio waved away his hand and did it himself. He bit his lip as he watched Erio; he could have done that himself. He pulled at the bandages still around his hands out of habit and while he knew they were harder to move because of the bandages, he knew he could at least load up his own plate.

"Are your hands feeling better?" Erio asked.

Snowe separated his hands and nodded. "A lot actually," he said. "I-I could have done that, you know."

Erio rolled his eyes and handed Snowe his plate. They divvied up the food and by the time they were finished, Snowe couldn't help but laugh at what he had piled on his plate; he had no idea how he would eat so much. He started into his food with a smile, but as he ate, he watched his hands. They hardly stung at all now that he thought about it. Rhea's salve had worked wonders though at the back of his mind, he knew it would.

The morning she had returned, Snowe had slept through her visit and though Erio mentioned he did say a few things, Snowe could hardly recall what. Nevertheless, the next time she had come with the salve, Snowe was shocked to find Esmeralda had come in after her; he hadn't even realized she had left Barina and she gave Snowe a quick hug when she came in. When she worked the salve over his hands, it felt familiar to him. A momentary dread filled up within Snowe as he thought on it, but he quickly pushed it aside and focused on eating. Now was completely different than then.

Everything Snowe ate was warm and delicious. Each bite settled in Snowe's stomach just fine and he felt relieved; he had been worried that with how little he had eaten lately, it wouldn't sit well, but bite after bite went down with no issue and he felt better. By the time he had made his way through half the food on his plate, a knock came from the door.

"It's unlocked," Erio said as he glanced at the door.

The door opened a crack and Rhea poked her head inside before she grinned. "You're eating!" she said as she walked inside. She had a jar of salve in her hands and she adjusted it in her grip as she closed the door behind her. "If you're eating, that must mean you're feeling better!"

"I am," Snowe said as he returned her smile. "Thank you for your help."

Rhea's smile warmed as she set the jar of salve on the table and sat in the vacant chair beside Snowe. "That's what I'm here for," she said. "Did you want me to check over your hands really quick? It's about time to give them more salve."

Snowe placed his fork down and turned to Rhea. "Since you're here, we might as well," he said and he held out his hands.

Rhea's hands were quick as she took the bandages from Snowe's hands and once she set the bandages aside, Snowe had to look away; he still felt hesitant to look at them. As the salve moved across his skin, Snowe gave out an involuntary shudder and glanced down; scarred for sure, but his hands hadn't looked as bad as before. Rhea had both hands covered in salve within seconds and she smiled at Snowe.

"You can still move them pretty well, right?"

Snowe moved his fingers and each responded accordingly with only a little pain remaining. Without another word, Rhea finished up and wrapped his hands in a clean roll of bandages. As she finished up, Snowe paused and sniffed; he smelled something odd when Rhea had moved closer. The salve had no scent but something smelled distinctly of orchids and wax.

"What's that smell?" Snowe asked.

"It really is noticeable?" Rhea straightened up, her cheeks red. "They told me it wasn't!"

"What happened?" Snowe asked as he covered a laugh.

"I was dipping these scarecrow pieces into this mixture Esmeralda made up," Rhea explained as she leaned back. "I sort of tripped into the basin when I went to put the pieces into my bag." She laughed. "At least the phantoms won't be bothering me if even you can smell it, huh?"

"Were you the one who put up the scarecrow near the road?" Snowe asked.

"I don't know where I got the idea, but it works," Rhea explained with a nod. "Phantoms stay away from the scarecrows whenever we put them up. Until the smell goes away anyway."

"How long until it goes away?" Erio asked as he set his fork down.

"About a month before I have to replace the clothes," Rhea replied. "I have to go out and do a replacement pretty soon actually. Was going to do it when I was done here."

"Can I help at all?" Snowe suggested as he leaned forward.

"You don't have to do that," Rhea said as she smiled again at Snowe. "I'm very capable, you know--tripping into the stuff or not."

Snowe frowned. "I-I didn't mean it like that," he said. "I-I just want to pay you back for all your help."

Rhea laughed as she folded her arms and considered Snowe for a moment. "For some reason, I knew you'd say that," she said. "To be honest, I wouldn't mind the company; Esmeralda usually stays here while I go out." She leaned forward. "Have you two dealt with phantoms before?"

Erio snorted from behind Snowe and Snowe had to stifle a laugh. "You don't even know the half of it," he said.

"If you're sure then!" Rhea hopped up from her chair and grinned at them both. "I'll meet you two in the lobby when you're finished eating, all right?"

She left without another word and once the door closed behind her, Snowe looked at Erio; the demon frowned and crossed his arms.

"Are you sure you'll be fine?" he asked.

"I should get out of this room at any rate," Snowe replied and he smiled. "As much as I've enjoyed sleeping the days away, I think it's time for me to actually do something." He looked back at his plate and took up his fork again. "Thank you for putting up with me."

Though Erio didn't reply, out of the corner of his eye, Snowe noticed he had smiled. They finished eating in silence and when they were done, they quickly changed into clean clothes before they left their room. Before Snowe got a step down the hall, however, Erio slipped back into their room for a moment and when he emerged, he had his daggers slid into his belt.

"Where did you even hide those?" Snowe asked as he laughed.

Erio gave him a hard look. "I'm not going to tell you that," he said.

"O-oh. I-I didn't mean it l-like that."

"I know."

Even still, Snowe wished he hadn't said anything. He didn't have the chance to apologize before Erio pulled him down the hall and Snowe tried to get the thought back out of his mind. By the time they arrived at the small lobby, Rhea was already waiting, a large bag at her feet, a thick cloak around her shoulders, and two swords in hand. When she sighted Snowe and Erio, she gave them a wave.

"Are you two ready?" she asked.

"As we'll ever be," Erio replied.

"Oh, good you have a weapon," Rhea said as she looked at the daggers in Erio's belt. She handed one of her swords out for Snowe. "Take this, Snowe. Just don't chop your hand off or anything."

Snowe laughed harder than he had intended to and only stopped when Rhea gave his shoulder a light slap. She had even started to laugh.

"It wasn't that funny!" she said. When Snowe calmed down and attached the sword to his belt, Rhea rolled her eyes and bent down to her bag. She pulled out two cloaks and both smelled just like she had. Snowe had to keep his nose from scrunching up as he took one.

"It's chilly today so these two should keep you nice and warm," she said. "Don't mind the smell. It was a good idea at the time, I swear!"

When Snowe and Erio had pulled on their cloaks and Rhea pulled up her bag, she led the way outside. Chilly was an understatement; as soon as Snowe stepped outside, he shuddered as the cold air hit his face. Still not a match for Sabine, he had forgotten how cold the mainland could get and he pulled the cloak tighter around him.

The area hadn't changed in the few days Snowe had slept away; the road remained empty aside from them, and the sounds from the woods around the inn had gone quiet. Rhea led the way forward with ease and only took a moment to figure out which direction to head.

"It won't take us too long to get there," Rhea explained as she led them through the bushes along the side of the road. She brushed off a branch that kept a hold of her cloak before she went forward again. "The scarecrow we're replacing is around this area I collect herbs in. Hopefully by the time they regrow, the phantoms will be too scared to get near it if we can't keep the scarecrows up."

Snowe quickened his pace and walked alongside Rhea. "Why are the phantoms so bad here?" he asked. "I hardly noticed them in Norin."

Rhea slowed to a stop and thought a moment. "Well, Lumisia is less densely populated like Norin is; we have so many places people forget to check and by the time they do, it's too late." She sighed and started forward again. "And Norin has a lot more manpower than Lumisia does. There's a lord named Rizec in the capital so he keeps the phantoms away from there, but he doesn't really send out soldiers to help us out here. Says we should just move on to one of the larger city until the phantoms are dealt with but that's asking a lot."

"When did the phantoms first start appearing?" Erio asked.

"Before I came here," Rhea said as she stepped over a fallen tree. She looked at Snowe and Erio. "I think it was maybe a year ago we started hearing about them."

She started moving again and Erio followed her, but Snowe's pace slowed. A year ago was when he would have changed everything. He came to a stop. Is that really why? he asked himself. Sephi had mentioned the idea back in Barina. The timespan fit. He couldn't dwell on the idea for long before he felt something warm against his shoulder. He flinched and found Erio had come back.

"What's wrong?" he asked.

"S-Sorry just thinking too hard," Snowe said. He gave the demon a smile.

Though Erio opened his mouth to say more, he stopped as Rhea's voice overtook his. It carried from beyond the trees and both he and Snowe looked toward where it came from.

"Here we are!" she called out. She paused. "W-Wait, where did you two go?"

"We're coming!" Snowe called out and before Erio could return to their conversation, he took Erio's cloak in hand and pulled him toward Rhea's voice.

She stood in the middle of a clearing in front of a scarecrow standing proud in the sunlight. Its arms were spread out and the clothing hung off it but all of its clothes had gone dry and they had no scent to it. Its burlap head had gone lopsided as the hay had leaked out and Rhea sighed as she began to collect the hay at its base.

"I swear birds keep picking at it," she said. She set her bag down and looked at Snowe and Erio. "Time to get to work."

As Rhea collected all the hay that had escaped and stuffed it back into the scarecrow's head, Snowe and Erio worked to get the dried clothes off the sticks. The land around them remained quiet as they worked and only the wind moving through the dry leaves broke the silence. Even still, something made Snowe pause as Rhea pulled out the new clothes she had soaked earlier. Goosebumps went up his arms and he stared at the trees around them.

"_Something's watching us_," Xiri whispered and Snowe felt hands at his chin and they moved his chin to look more to his left.

Snowe could hear something and he knew it wasn't the wind; the branches swayed in place and a few began to snap. Nothing looked out of the ordinary as he stared, however, but he couldn't stop trembling. The shadows on the trees looked too dark and as Snowe focused on it, the snapping branches ceased.

"What is it?" Erio asked as he and Rhea paused and looked at Snowe.

He had no time to answer; the land shuddered around them and a few trees gave way as a phantom fell into them. Its form massive, Snowe could see dozens of eyes and rows of sharp teeth all over its form. It had no cloak to speak of like normal phantoms and its entire mass was like a cloud of shadow. It had two large arms, its fists practically dragging on the ground, and the rest of its body faded before it touched the ground. Even still, as it moved forward, the land shuddered with each movement. It stopped as it moved from beyond the trees and all its mouths snarled at once.

Erio made the first move; he dropped the cloth he had held onto and he held out his hands toward the phantom. Shadows leapt out from the trees behind it and sunk deep into the phantom; they only garnered a howl from the phantom and it charged forward, the shadows snapping out of it as it moved. Rhea moved next: wind shot out from her hands and knocked Snowe out of the way before she threw up the cloth made for the scarecrow and shot wind into it. The cloth blew into the phantom and as it made contact, some of the mouth screeched, but those voices faded quickly before the phantom took hold of the cloth and tossed it aside. Smaller for sure, Rhea paled as the phantom turned to her.

Before the phantom launched its attack, Snowe had pulled himself up and concentrated on it; fire flew up along its back and the phantom stopped. It screeched loud and a few more faces on its form disappeared, but the attack alone didn't stop the phantom for long; all the eyes glowed at once as it turned to Snowe and he jumped as a large sword fell out of the phantom's body and thumped on the ground below. The sword glistened as the phantom pulled it up and fire flew across the blade.

"_Hide in the trees!_" Xiri shouted.

Snowe dodged the first attack the phantom made and charged for the trees. The blade slammed into the ground he just vacated and he flattened himself behind a large tree and held his breath. The phantom growled as it moved forward but it stopped at the edge of the trees; they were too close together to give the phantom's shoulders space. It didn't stop for long, however, it smashed one of its large arms into the trees and even the one Snowe had hidden behind toppled over and Snowe jumped up. A shield made of sparks flew up in front of him and Snowe flinched as the phantom brought its sword down; the attack rebounded as it hit the shield, but the shield fizzled out just the same.

Distracted once again, the phantom turned to howl at Rhea and Erio but before it could move forward, Snowe pulled out his sword and charged to the phantom's side; he plunged the sword into the phantom's stomach and at the same time his blade sunk in deep, a lightning bolt smashed into the phantom. Snowe fell backwards as the phantom screeched in pain, its voice so loud the trees shook in response. As Snowe pulled himself up again, he ducked low to the ground as the phantom's arm swung out, just close enough to knock Snowe sideways. Fearing the worst, Snowe rolled over and tensed up, prepared for another attack, but the phantom hadn't turned to him again; it focused on Rhea as Erio landed on the phantom's back, pushed by Rhea's wind before she dodged the attack aimed her direction. The sword smashed into the ground again, missing Rhea by a few feet, and Erio shoved both his daggers into the phantom's back and slid down, dragging his daggers along. Once his feet touch the ground, shadows from the trees leapt out once again, each carrying daggers made of shadow, and they charged into the phantom.

It wasn't enough, however; the phantom took in all the shadows and their daggers with ease, each of its mouths gritting in pain, and it rounded on Erio. It bashed through the shield Erio hastily put up around himself and it shoved him to the side, sending him down in one blow. Snowe flinched as another bolt of lightning shot into the phantom, and as it turned back to Rhea, Snowe ran forward and helped a dazed Erio back to his feet. Erio held out his hand toward Rhea and as she covered her head for an attack, a shield made of sparks flew up around her. The phantom stopped midswing before it hit the shield and it turned back to Erio, each pair of eyes glowing bright on its form.

Another lightning bolt crashed into its back and the phantom whipped around to attack Rhea, but she had already moved; with wind at her back, she launched herself beside Erio and Snowe and just as she took hold of them to get them elsewhere, Snowe pulled her down to the ground, just barely missing the phantom as it swung its arm to hit them both. Its arm flew over their heads and Erio pulled up another spark shield just before the phantom brought its fists down. The shield held and sent the phantom off kilter, its arms alight with sparks dancing across them. Rhea grabbed both Erio and Snowe and wind pushed at their backs, sending them further into the trees. They only passed a few trees before a sword crashed down beside them, causing Rhea's concentration to fall and as her magic dwindled, and the three of them fell over themselves.

As Snowe scrambled back to his feet to help Rhea and Erio up, his foot slipped out from underneath him and with incredible force, he shot back the way he came; the phantom had a grip around his ankle.

All the mouths on the phantom had disappeared and one large one had formed against its chest; its grin so wide, it took over all of the phantom's torso, it showed off rows and rows of sharp teeth. Panicking, Snowe held out his hands and fire sparked on the phantom's arm. Though it raced up the arm and went to the phantom's back, the fire froze in place and the phantom's skin over took it. The phantom's grin only widened and it raised its sword again.

Everything went quiet in the span of a second and everything grew much colder; ice froze the phantom's arm and sword in place and the ice snaked itself around the phantom's body until everything had been covered. The ice stopped before it covered Snowe's foot but it had covered parts of the ground and wormed its way up the trees closest to the phantom.

Snowe looked back to where Erio and Rhea stood and both were looking at the trees behind them. A tall woman, her hair a dark blue, came forward, a spear balanced against her shoulder.

"Relenia!" Rhea squealed and she suddenly dropped to her knees. "Oh my god, I thought we were done for."

"You know her?" Erio asked, out of breath, as Relenia walked past him and Rhea and went for Snowe.

"Of course I do," Rhea replied. "She helps me out all the time here."

"Nice to see you in trouble Snowe," Relenia joked as she stuck her spear into what still had a hold of Snowe's ankle. She pried him free and only paused when the ice covering the phantom cracked. She dragged Snowe to his feet and after she pushed him back toward Erio, she looked at Rhea. "Rhea, hit it with lightning before it breaks out!" she ordered.

Rhea jumped back to her feet with renewed energy and she stood beside Relenia before she took a deep breath. Lightning came from her fingertips and slid through the ice and into the phantom. The ice cracking continued with each bolt of lightning and Relenia concentrated, making more ice appear. With each partnered strike, the phantom howled and pieces of it fell from its form. Its sword had already disappeared and soon all of its pieces followed suit. By the time it had dissipated, Rhea looked exhausted and fell back to her knees to finish catching her breath and Relenia looked back to Snowe and Erio.

"Nice to see the two of you," she said and smiled. She looked beyond them. "It's safe, Anastasia, you can come out now!"

Relenia's daughter came out from behind a large tree, decked out from head to toe in a thick white cloak. She beamed as she rushed up to Snowe and gave him a tight a hug; a few inches taller than Snowe had seen her last, he couldn't keep from laughing.

"I missed you!" Anastasia said. She turned to Erio and quickly gave him a tight hug before he could move out of her reach. "You two haven't changed one bit!"

"And you got taller!" Snowe said as she withdrew from Erio.

Anastasia grinned as the words sunk in and she stood straighter. "I'll get taller than my mom! Just wait!" she said.

Relenia laughed as she came closer. "I don't know about that," she said. "Your dad was a hair shorter than me if I remember right." She winked at her daughter before she looked at Snowe and Erio again. "Erio your eyes look pretty dark again."

Erio groaned. "You think I don't know that?" he snapped. He folded his arms. "You should have gotten here sooner--everyone at the inn is going to give me weird looks now."

"Hey! I was running as soon as that thing made the road shake!" Relenia said. "Not my fault I had a hard time finding all of you!" She laughed as Erio rolled his eyes. "Let me at least help you all finish up that scarecrow as an apology and I'll take you all back to the inn. You both look like you could use a bit of warmth after that fight."

No complaints there and Relenia had Rhea show them back to the clearing where the phantom had attacked them. She assembled the broken scarecrow all on her own and thwarted off Snowe's and Rhea's attempts to help her out. By the time she had it standing proudly once again, draped in the cloth still smelled of orchids and wax, and led the way back to the inn, all Snowe wanted to do was crawl into something warm and nap. Perhaps he had overexerted himself on his first day feeling better.