# Chapter 5 - "A farewell to Aldcoast."

![](images/028_intermission_05.png)


The snowfall in Aldcoast had let up and the station right outside town finally agreed to let Relenia rent a carriage; she only hoped the snowfall didn't return tenfold after she had everything ready. Her plan to already be in Virin by now to catch a train to Lumisia had been disrupted by the snowfall and cold that had set in across Norin. Though it wasn't quite the same as the cold and snow on Sabine, people in Norin reacted differently to the snowfall. In Sabine it had been commonplace and everyone just became used to it but Norin enjoyed all seasons and Relenia realized soon enough she was one of the few who thought despite the snowfall, it was traveling weather.

Despite never wanting to see snow ever again, when winter arrived, everyone had found it pleasant: Vera had given birth to her daughter earlier in the month and both were as healthy as could be; when Vera hadn't needed any more help becoming accustomed to her daughter, Relenia had taken the time to make plenty of snowmen with Anastasia and Theodore when Snowe was too exhausted or cold to do so himself. She even taught them all how to make hot cocoa (Sabine never had what she needed and when Aldcoast had its first snowfall, she ordered a bunch of supplies and just realized how much she had missed the beverage) and though Snowe and Erio had burned their lips from drinking the hot cocoa too fast, Relenia had been happy to learn her daughter and Astra had a bit more common sense.

Though Relenia had planned to be in Virin by now, she wondered briefly to herself if she would have gone already had everything gone to plan; Richard and Vera's daughter had been born before the first large snowfall and Relenia was sure she used that as an excuse to stay an extra week or two, just to make sure Vera would hold up well. While she was a year older than Relenia, she had been nervous about finally having a child and Relenia made sure she was nearby in case Vera needed help. After the first few days, however, Vera had immersed herself into the routine of taking care of her daughter. Even when Richard tried to take some responsibilities off her shoulders, Vera didn't want to be away from her daughter for too long. Relenia wondered if she had been the same way about Anastasia, but she couldn't recall.

Relenia heaved another trunk into the back of the carriage and after she had it inside, she frowned to herself; this shouldn't be as hard as it was. All she wanted was to be in Lumisia because she wanted Anastasia to know at least one set of her grandparents. Dominic and Mariland had equally found a few family members in Lumisia as well and both wanted to go with Relenia to introduce Theodore to them so he had more family to look after him. But it was finally starting to hit Relenia what it meant. She would be leaving Aldcoast.

"Did you want any help?"

Relenia jumped at the sudden voice and as she turned, she found Hiante standing right behind her. His cloak had been pulled carefully over every inch of his body so not a single bone could be seen; Relenia wasn't sure where Astra found it, but she purchased a cloak in the beginning of autumn that was long enough to cover Hiante and then some. He could easily keep every piece of himself hidden and if anyone happened to glance at him in the night from their windows, they would assume he had a body and all.

"How do you do that?" Relenia joked as she beckoned Hiante closer. "I never hear you when you're walking. I think I would have killed for that ability when Anastasia was younger."

Hiante chuckled as he approached and once he stood beside Relenia, he bent down and grabbed one end of the trunk still on the ground at her feet. "I guess I'm lucky I'm a faster learner. Erio and Astra were such light sleepers they could hear me from the stairway in the tower so I just had to get really good at it," he admitted.

Relenia smiled at him and she bent over to grab the other end of her trunk. Together, they pulled it up and easily slipped it into the carriage beside the others. They secured it to the other parcels in the carriage and once everything was secure, Relenia let out a deep breath.

"Were you really going to leave first thing in the morning?" Hiante asked.

Relenia sighed as she turned to lean her back against the carriage. She had been hoping he wouldn't have brought it up, but she should have known better. She pulled herself up into the carriage and sat on the edge with her legs hanging over.

"I'm no good at this," she said.

"I thought as much." Hiante pulled himself up beside Relenia. He readjusted his cloak against his legs before he stared at the sleepy town before them. "Do you think Anastasia will like your husband's family?"

"I'm sure she will," Relenia said. "I just want her to know more about her father. I'm really bad with words concerning all that. I think she'll be glad to get to know them after this long."

Hiante patted Relenia on the shoulder. "I think Astra and Snowe would be sad if you left before they got up in the morning," he said.

"I know," Relenia whispered. "When I left Sabine to go to the shrine, no one cared." She paused and laughed weakly. "W-Well Richard did, but I mean he did everything he could think of to convince them not to send me, but he lost and I don't think he could face me afterwards." She leaned forward and crossed her arms. "But having no one care helped because I was already bad with goodbyes. I don't think I could keep a level head if I had to see their faces right before I left."

"Did you tell Richard and Vera?"

Relenia nodded. "I only told the three of you," she said. "They have their hands full with their daughter anyway. I don't want them to try to send me off when she's just going to be fussy out in the cold." Relenia laughed and rubbed the back of her neck. "Vera even made us all this large meal for the ride to Virin; I have no idea where she or even Richard found the time to make it."

"Your guess is as good as mine," Hiante said. "Couldn't have been Snowe since he's been staying with us since he got sick. He didn't want Richard and Vera's daughter to catch anything."

"I bet Vera doesn't even sleep then," Relenia replied and laughed. "I wish I could remember how I was after Anastasia was born. I could at least share stories."

"You don't?"

"A lot of my memories are jumbled," Relenia admitted quietly. "I didn't have half the spells on me everyone else did, but it all just feels hazy when I try to remember."

"I suppose it's good you can recall some things," Hiante said. "Sometimes Snowe is telling Astra or Erio something and then he'll stop and wonder if it really happened." He leaned forward and sighed. "I thought my memory was bad but all his are just mixed up."

"At least we don't have to worry about it here." Relenia stretched her arms in front of her.

They went quiet and once Relenia put her arms back down, she sighted a few snowflakes drifting down from above. They swirled as they went and took their time before they settled on the snow already accumulated on the ground; Relenia felt relieved they didn't fall like the nights before and she knew they wouldn't grow to be anything substantial. As she looked up to the sky, she found herself smiling; though she hated the snow on Sabine, seeing it blanket Aldcoast made everything pleasant and peaceful. Perhaps it was the knowledge the snow would eventually melt that kept the worry at bay. Relenia glanced back to Hiante and found that he stared at the snow as well.

"How did you even manage to get out here without Astra?" Relenia asked.

Hiante laughed and scratched the spot just below his right eye. "She doesn't know I'm out here," he said.

"Asleep?"

"No, she and Erio are looking over something they won't let me see," Hiante admitted. "Snowe was helping them but last I peeked, he had laid out on the couch and he was out like a light." He paused as Relenia let out a laugh. "They were all too distracted to notice me sneaking out."

"Have they been researching a lot?" Relenia asked.

"Recently," Hiante replied. "Neither will let me help though, but Astra's not much for researching; she gets really antsy." He looked at Relenia. "Do you know what they're looking for?

Relenia nodded and leaned back. "I have a pretty good idea," she said. She covered a laugh as Hiante continued to stare at her. "I know you're giving me a look but they just want to find a way to give you a real body, that's all."

A sigh came from Hiante and though Relenia had expected it, the sigh sounded much sadder than she had anticipated. Hiante looked back to Aldcoast and scratched the back of his skull.

"I wish she wouldn't worry about that," he said, his voice sad. "I'm happy where I'm at and she doesn't need to worry. I bet that's why she and Erio haven't been sleeping much."

"You may not have a face, Hiante, but you can't hide it," Relenia said as she placed her hand on his shoulder. "They just want you to enjoy life in Aldcoast as much as they do."

Hiante didn't take his eyes from the road in front of them and Relenia dropped her hand and did the same; once again they watched the snowflakes fall in silence and she took a deep breath. Part of her wished she could think of something else to say; she hadn't wanted to leave until Hiante was able to get a body, but she knew she wasn't very much help to either Astra and Erio. Ever since she had contacted her husband's family in Lumisia, the urge to take her daughter up north wouldn't stop pulling at her. She wanted Anastasia to know her father's family and from the letters she had received from them, they equally wanted to see her and her daughter. Eighteen years and no contact? It was a wonder they hadn't given up hope some of them were still alive.

As more thoughts came to mind, Relenia sighed and ran a hand under her eyes. Reality must have finally hit her and she felt her eyes fill up.

"Thank you for helping me, Hiante," Relenia whispered.

"Ah, you won't be thanking me in a second."

Before Relenia could reply, she sighted a lantern's light down the street. She hid her laughter and she refrained from knocking Hiante off the carriage as following the lantern's light came Astra, Snowe, and Erio. She had forgotten he could communicate with Erio. All three of them were decked out in cloaks (and Relenia swore Snowe had thrown two on; the cloaks on the mainland were in nowhere near as thick as the one he had worn on Sabine) and Snowe held onto a small mug with steam coming off the top while Astra held onto the lantern.

"You should have told us!" Astra said once they were close. "We could have made you a nice going away dinner!"

"We just had to make do with hot cocoa on such short notice," Erio said as he nodded toward Snowe.

"It-It might be hot," Snowe said as he held out the mug.

Relenia gave Hiante a light punch before she stood and took the mug of hot cocoa in hand. It warmed her fingers as soon as she took it and she looked it over; it looked the right color and smelled about right. She smiled at the three of them before she put the mug to her lips and sipped. Hot was an understatement and as soon as the cocoa hit her lips, it burned and she took the mug away and coughed.

"What do you mean hot?" she teased. "That's scalding!"

Snowe's face turned a bright shade of red. "T-They were rushing me!" he said. "I just woke up and they didn't want to let the milk boil regularly!"

"Did you just plop a flame right inside?" Relenia teased and she laughed as Snowe's face became redder. "And it's so bitter! I know I told you just the right amount to put in."

"I always thought you made it too sweet," Erio replied.

Relenia couldn't help but laugh again and she glanced back to Hiante. He had his palm against his forehead and Relenia looked at Astra next, her eyebrows raised.

"Don't look at me!" Astra said, flustered. "I-I was trying to find the lantern! I thought they had it covered!"

"This is great you three," Relenia said as she kept down another laugh. "Couldn't have asked for a better send off."

Astra smiled and adjusted the lantern in her hands. "You'll come back and visit a lot, right?"

Relenia nodded. "Of course I will," she said. She turned back to Hiante and handed him the mug for a moment before she reached forward and gave Astra a tight hug. After Astra gave her a squeeze in response and let her withdraw, Relenia turned and looked at Snowe.

"Make sure you're a good big brother, all right?" she asked. "Vera's daughter is going to be fussier than Anastasia."

Snowe laughed. "I-I know that," he said.

Relenia wrapped her arms around Snowe and gave him the same tight hug. As she pulled away, she put the back of her hand against his cheek and laughed. "And you have a fever even!" she said. "You didn't have to come out to see me."

"Then I would have missed you completely," Snowe said quickly as he brushed off Relenia's hand. "J-Just write a lot, all right? I think Richard and Vera are going to miss you a lot."

Relenia gave Snowe another hug. "That's my plan," she said. "Just stay healthy."

A small smile from Snowe and Relenia turned to Erio and stopped herself; even when Astra gave him a hug, the demon would look uncomfortable at best, especially if he knew people were around to notice. Erio quickly gave Relenia small smile and held up a hand to no doubt stop her if she had decided to try to give him a hug.

"I should get Snowe back before he faints out here," he said quickly.

"I-I won't faint," Snowe said as he looked at Erio.

Though the look Erio gave Snowe made Relenia laughed, she found herself caught off guard as Astra gave her another tight hug. She withdrew quickly and put her free arm around Snowe's back to keep him from wavering in his spot and she looked back at Relenia.

"I'll come see you off in the morning," Astra said and smiled. She looked at Hiante. "And Hiante, don't stay out too much longer! You know the guards like to do their rounds early when it snows."

Even though Hiante's expression couldn't change, Astra smiled widely as though she and he had just shared one. Relenia gave her a wave as Astra and Erio helped Snowe back through the streets. After a moment, Relenia sat beside Hiante again and sighed to herself. She ran her hand underneath her eyes again, surprised she hadn't cried, and looked at Hiante. He held out the mug of hot cocoa again and she laughed.

"You should try to finish it," Hiante said. "Erio might get offended."

"Does he have any taste buds?" Relenia joked as she took the mug.

Hiante chuckled. "He cooked dinner for Astra once and she won't let him do it again until he lets Snowe teach him how," he said. "For a kid raised in a castle, Snowe's surprisingly good at cooking once given the directions."

"And with a body you could know that first hand," Relenia said and though she knew Hiante gave her a look, she feigned ignorance and took another sip of the hot cocoa. Still too bitter, she cringed and once Hiante chuckled, she couldn't keep from laughing again.

They went quiet once again and Relenia watched the road as she leaned forward. Everything so quiet and asleep, she felt as though she wanted to leave right now just to avoid the spectacle in the morning, but she knew she had no choice. At least with Astra, Snowe, and Erio seeing her tonight, she avoided some of the spectacle, but even still, she just wanted to leave without having to say many goodbyes. They were too hard. She knew Anastasia and Theodore would rush around in the early morning telling goodbye to the ones they would miss (and no doubt have another bracelet or two made for Snowe), but she doubted she could do the same. All the people she felt important to her had been given their goodbyes at least. Relenia sighed to herself and took another sip of the hot cocoa. She set the mug beside her and wiped her eyes.

"Thank you, Hiante," she said. "No matter how badly coordinated that was, it made me smile."

Hiante patted Relenia on the back. "I'll be sad to see you go," he said. "I know we sound like a broken record, but make sure you keep in touch."

Relenia smiled at Hiante. "After what we went through, I wouldn't have it any other way."