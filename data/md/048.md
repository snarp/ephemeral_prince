# Chapter 19 - "Ruins revisited and a library scoured for a new lead."

![](images/048_permanence_19.png)


Spending time with her father after months of not seeing him had put Canan in a good mood and she had slept better than she had in months. Early in the morning, she awoke and when she went to wake Snowe and Erio, she found they had already gone to the library. Instead, Canan made her way out of the castle to find Astra; she had hoped Astra would have stayed in the castle like Snowe and Erio had, but they hadn't felt safe. Not that she blamed her any.

She went into the small bed-and-breakfast off the main street of Alsan where she recalled Snowe said Astra was staying at and just inside, Canan found Astra coming out of the dining hall. Two plates full of pancakes and eggs, a cup of coffee and a cup of orange juice on each plate, balanced on her arms as she carefully made her way toward the stairs.

"Good morning!" Canan said as she stepped beside Astra.

Her voice made Astra jump and Canan couldn't help but laugh as she helped keep Astra balanced as both plates almost tipped off her arms. Astra gave Canan an exasperated look and laughed.

"Early for you!" she said. "Did you have fun catching up with your dad?"

"I did," Canan said and nodded. She took the cup of coffee and the cup of orange juice from the plates and giggled as she sighted the relieved look on Astra's face. "Did you sleep well here? I'm sad the castle scared you away."

"After what Snowe told me, I didn't feel comfortable," Astra said as she led Canan up the stairs toward where the inn rooms were located. "I know it was lords they killed, but I don't think I would have been able to sleep in the castle."

"Did you and Hiante find anything in the city?" Canan asked.

Astra paused as she entered the hall. "We did." She turned to Canan as she stepped off the steps and lowered her voice. "Have you ever seen these ruins deep in the woods just outside the city?"

Something about the description made Canan pause; however, even though she searched her memory, nothing concrete came to mind. Just an uncomfortable feeling of familiarity and she shook her head. "I don't think so," she said.

"You should help me check it out again then," Astra said as she moved forward again. "I had a few things I wanted to go back and see."

"You didn't want to help with the researching?" Canan asked as she followed Astra.

"Erio better researches alone." Astra smiled at Canan once she stopped at the door at the end of the hall. "I bet even now he's either gagged Snowe to keep him from talking or he's threatening to throw Snowe out a window to get some peace and quiet."

As Canan laughed at the thought, Astra adjusted the plates in her hands and once she had a free hand, she opened the door. Inside, Hiante sat at the table pushed up against the wall on the right with a book opened in front of him and as Canan and Astra moved inside, he smiled at the two of them.

"And here be breakfast!" Astra announced as she placed the plates on the table. Hiante pushed away the book as Canan offered him both the coffee and orange juice.

"Ah, pancakes," Hiante said as he took the coffee from Canan and placed it next to his plate. "Have you made these yet, Astra?" He took the fork from the plate and cut off a piece.

"Not yet!" Astra pulled over another chair from the other side of the room and placed it at the table so Canan could sit. "Vera tried to give me a recipe but it looked questionable. When we get back to Aldcoast, I'm going to try one the cook here gave me."

Hiante smiled and as he and Astra turned their attention to their breakfast (Astra offered to share some with Canan, but she had already eaten before she left the castle). While they ate, Canan peeked at the book Hiante had pushed aside. It was one she hadn't ever seen before but as she looked through the pages, she caught sight of symbols she recognized.

"Where'd you get this one from?" she asked.

"Erio found it," Astra replied. "We're not entirely sure what it's for yet."

Canan pointed to the symbols along the top of the page. "It's a lord symbol I think," she said. "This should be the name... though if I'm remembering right, this lord has been gone for a long time."

"Just as well," Hiante replied after he took a drink of his coffee. "Shouldn't be summoning one anyway." He looked at Astra who had already eaten through most of her food. He stifled a laugh. "Are you heading out today? I think your stomach is going to hurt later if you eat like that."

Astra swallowed what she had shoveled into her mouth and laughed weakly. "If you don't mind me leaving again," she said. "I don't want to leave you here alone."

"Better than being cooped up here with me," Hiante replied. "Don't need to slow you down, so don't worry. I'll be fine here."

"What happened?" Canan asked as she frowned.

"I did far more walking than I should have yesterday." Hiante patted his leg and winced. "They just ache now."

"When we get back, make sure I look up this recipe I have for a salve Esmeralda taught me," Canan said as she leaned forward. "It helps keep your muscles in tip-top shape and soothes away the aches."

"Would you have all the stuff for it here?" Astra asked.

"The castle should." Canan smiled. "I can get my brother sneak me into one of the storerooms."

Hiante laughed and nodded. "If you think it'll help me, thank you." He looked at Astra and kept down another laugh as she finished off her orange juice. "Go on, be off already." He pulled the book closer and gave it a pat. "I'll be here reading this one over for Erio."

Once Astra placed her glass back down, she stood and reached over the table to give Hiante a quick hug. She grabbed her bag from her bed and (only after Hiante reminded her) grabbed her sword from the bed post. As she latched it to her belt, she bid Hiante good luck before she pulled Canan back into the hall.

The city still buzzed early in the morning as Canan and Astra left the inn and Astra took the lead and led Canan through the crowds toward the southern gate of Alsan. The gates were full of people just coming in for the morning and merchants moved through slowly and stopped just inside to allow guards to search through their belongings. Other travelers filed in past them, occasionally being stopped if they looked suspicious enough.

"It wasn't this crowded yesterday," Astra pointed out as they dodged a few groups rushing past them.

"I think they just want to be cautious of what happened yesterday," Canan replied.

Canan pulled Astra closer to the gates but she had to stop again and move aside as a group of guards filed in around the carts which had been stopped. They were from one of the scouting groups and as they passed, Canan spotted her brother amongst them and she had to stifle a laugh. It surprised her that he was already up and his tired expression showed just how out of the norm an early expedition was for him. When Iain caught Canan's gaze, he slowed to a stop. He gave a glance at the guards he had come through with before he stepped toward Canan and Astra.

"You're up early!" he said once he came close.

"I should say the same to you," Canan teased. "You don't usually have your patrol until after lunch, right? What, did you sneak on with another group this morning? I can't believe you dragged yourself out of bed for that."

Iain hushed his sister and quickly glanced over his shoulder at the guards he had moved away from. They were engrossed in speaking with a few travelers and Iain pulled Canan and Astra away from the streets. "Don't tell anyone--I'm surprised they didn't even notice me," he said. "Dad'll kill me if he finds out I snuck out this morning."

"Why did you sneak out?" Canan asked.

"I wanted to check something." Iain glanced back to the gates and watched a cart move through for a moment before he looked back at Canan. "There's these ruins in the woods and I think the phantoms congregate around it--Dad won't show me it though." He ran a hand through his hair. "I just want to check it out, but my group never gets close to it. This one really didn't even follow the path they were supposed to take."

"A ruin in the woods?" Astra leaned forward. "I found it yesterday. We were going to check it out again right now."

"Alone?" Iain asked.

Canan took the hilt of Astra's sword and jiggled it in place. "Look at this thing!" she said. "No phantom will be able to touch us."

Iain glanced at the sword and while he whistled, he still looked worried. He glanced at the guards he had come in with and watched as they started to move back to the castle. "Let me come with you then." He stopped abruptly and looked at Astra quickly. "I-I-I mean, not-not to say you aren't capable, but... uh..."

"I don't mind if you come with us," Astra said and laughed. "The more the merrier, right?"

"Just don't look suspicious. Getting in might be trickier since we're all on edge about lords." He turned to the gates and took a deep breath before he started forward. "Follow my lead."

* * *

First thing in the morning, Snowe and Erio had gained access to the vast library and grabbed anything that looked helpful before anyone else aside from the librarian showed up. Though as they found a corner nestled between two large bookcases with books that hadn't been touched for a long time and sat down to look through the books, a headache pounded at the back of Snowe's head; the large window behind the table let in the bright sun and Snowe had to pull the curtains closed. As he settled back in his chair to look through the books, he shut his eyes for a moment to will away the headache.

The pain made Snowe think back to what he had done after he drank with Soan; he recalled the old king had helped him back to his room, but anything after that was too foggy. Snowe hardly recalled getting into Erio's room much less crawling into his bed and when he awoke against Erio, Snowe felt thoroughly embarrassed with himself. Even still, Erio hadn't mentioned much at all from the previous night; he sat back in his chair, books open all around and he had already jotted down a few notes in the margins. Quite a number of books had already been placed to the side as they weren't helpful.

As Snowe tried to quell his headache, he watched as Erio worked. He couldn't understand why the demon hadn't said something. Something in the back of Snowe's mind told him he had done something, but what Snowe recalled would surely be cause for comment. Was it a dream? Snowe wondered to himself, but he knew that couldn't be. Xiri controlled his dreams and surely Xiri wouldn't have made that dream.

"E-Erio," Snowe said and once the demon glanced at him, Snowe leaned forward. "D-Did I say anything embarrassing last night? O-Or d-do anything?" he asked.

"Nope." Erio looked back at the book and turned the page. "How does your head feel?"

Snowe groaned as he lay his head against the opened book in front of him. "Like someone hit it with a brick," Snowe replied.

Erio snorted. "What did you expect?" he asked. "How much did you even drink? You never drank before, right? Why did you think you'd be perfectly fine afterwards?"

"But Soan kept knocking them back!" Snowe said. "I didn't think it'd bother me this much."

"He's probably drank like that before!" Erio reached forward and flicked Snowe in the head.

"Don't do that!" Snowe sat back up and waved away Erio's hand.

"Then get back to researching," Erio stressed. "I will throw you out that window if you keep complaining."

Snowe kept down a laugh; that was the reaction he had wanted to hear. Perhaps he was overthinking everything and he ignored any lingering thoughts and looked at the pile of books Erio had stacked to his left. "None of those are helpful then?" he asked.

"Not in the least." Erio closed another book and placed it on the stack. "A lot are about various ways to summon a lord without being fried for simply trying." He grabbed another book from the stack to his right and held it up. "I'm beginning to think someone moved most of the books in this series out of here; we only have three out of the five now." He sat back again before he opened the book. "Though the more I look at this the more it sounds like the spell Edgar and Lina used on you."

Snowe stood up and moved beside Erio to look over his shoulder. The diagram on the page was so detailed, he knew he'd need a magnifying glass to read the fine print. Even trying made Snowe's head spin and he pulled his chair over and sat down beside Erio.

"But even with this, there's nothing about getting a demon out of a person after doing this to them." Erio sighed. "Except for the obvious."

Snowe nodded and leaned on the table again. Another thought came back to him from his conversation with Soan and he glanced at Erio. "Xiri w-wouldn't be so connected to me it would hurt me if we got him out of me, would it?"

Erio paused before he closed the book and sat back. "I don't know." He placed the book on the others and glanced at Snowe. "Do you have any idea at all how Edgar and Lina found this spell? Sabine never looked like the place for lords to leave their spells around. I mean, from everything I've read, this spell was used when demons tried to strike back, but that was so long ago. This spell isn't even used anymore."

"I don't know," Snowe said. "There's a lot we don't know about that island actually." He paused and put his head back down on the table. "Even about Edgar; I doubt Richard knew much about him."

"I can't even see Edgar being smart enough to figure it out." Erio folded his arms. "I mean... I can barely make sense of it."

"To get Xiri out, do you think we'll have to call upon the lords?"

"And therein lies an issue." Erio nodded slowly. "No lord really would let us call them--except maybe Beliaz, but I doubt he'd be in a position to help us anyway."

They went quiet and Snowe sighed to himself; he doubted if Beliaz even had his full power, he could actually help. Something told him that it was impossible. As Snowe remained still, Erio stood from his chair and picked up the books in the useless pile.

"Let me do another run through the shelves and return these," Erio said. "I'll be right back; don't leave."

Once Erio had left, Snowe lay his head on the table and covered it with his arms; his headache only growing worse, he felt like he was on Sabine again before his parents had died. Though Snowe knew it was a different kind of pain, he wished he hadn't gotten out of bed. The hangover only coupled with the frustration and made the pain even worse.

"Snowe?"

Snowe jumped at the new voice and as he sat up and looked over his shoulder, he found the woman who had stared at him throughout the lunch stood behind him. She had her hair drawn back in a loose bun with barrettes at the side keeping her hair in place and she wore a muted dress with a thin shawl around her arms.

"I'm sorry," Snowe said and he stood. "I just dazed out for a second."

"Lina used to do that." The woman smiled at Snowe as she moved to the seat Erio had vacated and sat down.

Snowe stared at her with his eyes wide; she had said his mother's name. No words came to mind and as Snowe stared at her, stunned. The woman laughed quietly.

"I'm sorry," she said. "My name is Matreya. Lorin told me your name."

"You knew my mother?" Snowe sat back in his seat and kept his eyes on the woman.

"She was my cousin," Matreya said as she crossed her legs. She leaned on the table and smiled at Snowe again. "When I saw you, it was like seeing Lina all over again. You have her eyes, her mannerisms, and expressions believe it or not." She took her eyes away from Snowe and took a book from Erio's stack and opened it. "It's actually very sad what happened in the end," she said. "Though it is nice she must have found happiness somewhere if you're here."

"W-What happened?"

"Sabine Capital burned to the ground." Matreya looked at Snowe and frowned. "Lina disappeared and she never came back." She watched Snowe a moment before she leaned forward. "Are you Edgar's son?"

Snowe looked away; though he felt he should have answered, he couldn't raise his voice. Nevertheless, after a moment, Matreya sat back up and closed the book.

"You sure don't look like him," she said. She replaced the book on the stack and considered Snowe again. "You know, if you're looking for lord texts, you should try Morzin."

"Morzin?"

"It used to be the capital of Norin," Matreya explained. "A lot of books once here have gone there as the lords have become protective over their texts. Since Zuan usually resides in Morzin, he can keep them safe."

Snowe shook his head. "I-If that's where Zuan stays, I-I'd rather not get close."

"Word has it, Zuan's been missing in action lately," Matreya whispered. "Hasn't been in Morzin for months." She smiled again and patted Snowe's arm. "Best to check it now before he returns." She stood before Snowe had time to respond and she straightened her dress. "If you're ever in Lumisia, please visit me. I'd love to speak with you more about Lina. Something tells me you didn't know her for very long, hm?"

"You can't talk now?" Snowe asked as he stood.

"I have to return to Lumisia as soon as possible; this little chat has already thrown me off schedule," Matreya said. "The lord there is going to get worried about me soon and I have to come up with a story to tell him. But please, come visit me in Winley in Lumisia. We have much to discuss."

"W-Wait," Snowe said as she started to step away. "W-What did you think about Edgar?"

Matreya paused before she looked away. "He liked playing with fire," she said. She gave Snowe a slight bow. "I hope to see you soon."

As she walked away, Snowe sighed again and looked back at the books. Before he could reach for them, however, Erio appeared beside him and Snowe jumped.

"Did you forget I could do that?" Erio asked, an amused smile on his lips.

"Warning would be nice!" Snowe replied and laughed. "W-Why did you hide?"

"Wasn't sure about her." Erio dropped the book he had in hand on the table and went to the bookcase to peek around it. "Morzin sounds like a plan though." He came back to Snowe and crossed his arms. "Nothing else here really."

As Snowe nodded, he and Erio collected what books they had found useful and placed them in their bags. They replaced books they decided against using (only a few helpful passages in them that Erio already wrote down) and they carefully left the library; thankfully the librarian hadn't noticed their bags were a bit heavier than when they had come in.

The castle remained quieter than it had the day they arrived, but with the headache that still threatened to return, Snowe was glad for the peace. They returned to the castle foyer in due time, but as they went for the doors, Snowe slowed to a stop as he noticed Soan coming inside.

"Are you two already finished in the library?" Soan asked as he stopped. "I thought for sure you'd be in there longer."

"We got what we wanted," Snowe said as he patted his bag. Immediately he wished he hadn't said anything as Erio glared at him; they weren't supposed to admit they took anything out.

Soan laughed all the same. "I won't tell the librarian you took something out," he said. "Just bring them back eventually, all right?" When Snowe and Erio nodded, he smiled. "Where are you headed now?"

"Morzin," Snowe replied. "I was told there could be something there."

"As good a lead as any I suppose. Make sure to be careful," Soan said. He held back a laugh as though a thought just occurred to him. "So how is your headache, Snowe?"

Snowe felt his cheeks heat up and he laughed. "Please tell me you have one too!"

"I am deeply sorry I let you drink that much. I didn't even think about it until after the fact," Soan said. He leaned forward. "I hope you visit again soon; it was nice getting to know you."

"When this is all over, I promise I will," Snowe said. "Thank you for your help."

Though Soan looked as though he wanted to reach forward, he stopped and straightened his back instead. He gave both Snowe and Erio a nod before he continued on his way. Once he had entered the castle again, Erio and Snowe left the foyer and Snowe had to pause for a moment as the sun made his headache worse.

"If only sending you at the lords would work to make them friendly," Erio said as he led the way.

Snowe laughed more than he had intended to and as a few groups nearby gave them curious looks, Erio flicked the side of Snowe's head again.

"It wasn't that funny," he whispered.

"L-Let's just group back up with everyone," Snowe replied as he calmed down.

* * *

The ruins hadn't changed since Astra had been away; some of the grass around it had been burnt where Ciran had set the phantoms aflame, but everything else remained as it was before. As Astra led the way to the ruin entrance, she felt the cold wash over her skin again. Though she hoped she kept the shivering under control, as she stepped into the ruin's antechamber and looked back at the two siblings, she found Iain had paused as he pulled his thin cloak off his shoulders.

"I'm fine," Astra said.

"And I'm warm," Iain replied. He handed it out to Astra. "If you don't want to use it, I'll just leave it out here."

Astra took the cloak as she rolled her eyes and while she pulled it on, Iain and Canan stepped into the ruins beside her. Both paused abruptly once they moved past the broken doors and they shared a glance.

"What's wrong?" Astra asked as she clasped the front of the cloak together.

Canan shuddered. "I don't know," she said. "It's not cold, but I think I've been here before." She took a few steps forward, her eyes on the ceiling. Paint long since faded covered the ceiling and its image had been broken up by cracks along the ceiling. "That looks really familiar."

"Doesn't help the sounds outside stopped," Iain added as he placed his hand on the hilt of his sword. "I don't like this place at all."

Astra nodded and she led the two siblings forward; a few pieces of the antechamber had cracked from the lightning she had flung around before, but nothing else had changed. The phantom she and Hiante had attacked had disappeared as well but Astra still stayed alert. She took Canan and Iain into the circular room and she found it hadn't changed either; the skeleton still lay where it had, its clothes still in tatters and it still looked up through the skylight. As Canan and Iain came close to the skeleton, they both stopped, their blue eyes wide as they stared at the skeleton.

"I do not like this place," Canan whispered as she trembled.

"Why would a skeleton feel familiar?" Iain asked as he slowly stepped over to it, as though expecting it to spring up. He knelt down and moved some tattered cloth from skeleton's wrist. A simple beaded bracelet remained on it. Astra stopped as she sighted it; she hadn't seen it before.

"Hey, Canan, doesn't your friend have a bracelet like this?" Iain asked.

"D-Does he?" Canan knelt down and looked at it closer.

"He does." Astra kept down a shudder as she watched it. She didn't want that skeleton to be Snowe's and she looked up. The sky still had holes in it where stars twinkled through the darkness. "Look up."

Iain and Canan looked up at the skylight and both went pale. Iain straightened up and looked at Astra. "Have you looked out of it yet?" he asked.

"I'm too short to reach it," Astra said. "And when I was here, a phantom attacked us, so I didn't have the time to try winding myself up."

"Wind me up," Iain said.

"What if you hit your head?" Canan asked as she frowned. "We can't drag you all the way back to Alsan."

"That would only happen if Rhea or I did it," Iain replied quickly. He stood up and held his hand out to Astra. "Come on, she looks capable! You just insulted her, you know!"

Canan blushed deeply and she looked at Astra. "I didn't mean to!"

Astra laughed, glad for the lightened mood and she shook her head. "It's all right," she said. "I can wind you up there no problem, just brace yourself."

Iain pushed his sister closer to Astra and after he carefully moved the skeleton aside, he stood below the skylight and took a deep breath. Astra closed her eyes once he was ready and she concentrated on the wind gently moving through the ruins; she felt it push her clothes and as she held out her hands toward Iain, the wind rushed in from the antechamber and swirled around him. When Astra opened her eyes, the wind shot Iain upward and though he yelped in fright, he managed to hang onto the edge of the skylight and he pulled himself up, letting his feet dangle inside. Though as he looked out from the skylight, he remained quiet and after a moment, Astra and Canan shared a worried glance.

"I-Is something up there?" Canan asked.

"There's nothing," Iain whispered. "The-The sky's there all right, but there are holes in it everywhere." He shuddered and readjusted his arms. "And it's freezing. All the trees here are dead, I don't even see Alsan even though it should be there." He pulled himself further up and leaned forward. "Everything looks rusted; it smells like blood."

Astra had enough of waiting, she moved below the skylight herself and as she started to summon the wind again, she stopped; her concentration cut short as Iain yelped and dropped himself back inside, only hanging on with his fingers, and a large phantom shot down. Astra jumped back and pushed Canan against the wall to keep her safe as the phantom crumbled to the ground. It remained still for a moment before its body started twitching and it shot up to its feet, screeching as loud as it could. Its limbs bent at odd angles, bones sticking out of its skin, and its long robes weighed it down as they were soaked at the bottom. As it moved a step forward, its cloak dragged and a streak of blood was left behind. Before it could charge or Astra could attack, Iain dropped down from below and pushed the phantom into the ground. Canan moved out from behind Astra, her hand out in front and ice materialized beneath the phantom and trapped it against the ground while Iain hopped off its back.

Iain and Astra shot the phantom with lightning at the same time and as it howled in pain, Iain moved across the room and took his sister's wrist before he pulled her back into the antechamber with Astra close behind. They stopped halfway down the hall, however, when more screeches filled the ruins and as they looked back, Astra took in a sharp breath; more and more phantoms fell through the skylight, each more disfigured than the last, some even had weapons sticking out of their backs. So many were falling inside, they almost looked like one large entity. All the phantoms went quiet for a split second as their eyes looked forward at once and when they caught Astra's gaze, they charged forward all at once. Astra called upon wind again and sent a huge gust toward the mass of phantoms, sending them backwards, Iain moved in front of Astra and pulled out his sword. He deflected any attacks from phantoms that had peeled away from the mass and as Astra and Canan flung magic around him, more and more phantoms fell through the skylight.

"I wish I hadn't looked out!" Iain shouted as he kicked away a phantom before he let loose a lightning bolt from his blade. It fanned out across the antechamber and shocked any phantom nearby. Even though a wave of phantoms had fallen in pain, the others soon overtook them. Astra shot them back with another gust of wind and she felt Canan pull her backwards with Iain.

"When we get out, you two blow them all back, all right?" Canan ordered. "Don't even hold anything back."

Iain didn't question his sister; he and Astra stood side-by-side and kept up their magic as Canan pulled them back through the hall. As soon as Astra felt the ground beneath her feet and they had left through the doorway, she and Iain shot forth another gust of wind. From sheer force, the phantoms shot back, wailing in pain, and as they began to regroup, Canan squeezed between her brother and Astra, dropping to her knees. She had her palms against the earth and it trembled as she pulled her arms upward. Ground shot up, following her hands, and created a wall in front of the entrance. The wall shook in seconds from the onslaught on the other side and Canan created another wall to strengthen the first. When the shaking ceased, Canan sighed as she stood up, taking deep breaths.

"I thought you used water magic!" Astra said, amazed.

Canan grinned at Astra. "I was born on the Vernal Equinox," she said. "I can use both water and earth."

"Do you think that'll hold the phantoms inside?" Iain asked as he helped his sister stand up straight.

"For now," Canan said as she watched the wall of earth. She looked at her palms suddenly. "It feels like I did this before. Only from the inside," she whispered.

Iain paused and gave out another shudder. "I hate this place!" he said. "Didn't help me understand anything any better." He looked at Astra. "Why did you come here before anyway? Did you find anything?"

"No, we just came across it the other day," Astra replied quickly. "I was hoping we would have figured why the sky's different here..."

"It felt like someplace else entirely when I looked out," Iain admitted and he looked up.

They went quiet and watched the sky alongside Iain; though the leaves were plentiful overhead, Astra could still see the blue sky above, no tear in sight. The sounds from the forest had returned and it sounded exactly as it should.

"I'll tell my dad and Soan to look into it," Iain said as he glanced back at the wall of earth. "Whenever those phantoms go away, I'll see if I can get back in there and give that skeleton a proper burial as well. I just feel like it really deserves one." He sighed and looked at Astra, giving her a smile. "Don't worry about us; we'll have this covered in no time."

The wall of earth shook violently and after a worried glance exchanged, Canan pulled up another wall of earth and the three made haste back to Alsan.

* * *

Just after noon, Snowe and Erio had fought their way through the crowds that had gathered at the castle grounds and finally entered the city. They had to squeeze through more crowds on their way and by the time Snowe had to excuse him and Erio from running into a group, he had lost his patience.

"You could just teleport us, you know," Snowe suggested as Erio pulled him past another group.

"On the off chance a lord does show up, I don't want to feel exhausted from having to teleport you with me," Erio said. "Carrying you with me leaves me pretty tired."

They became quiet as they entered the main street and both breathed a sigh of relief to find it wasn't as crowded. As they neared the bed-and-breakfast, they found Canan and Astra just outside as Iain strode away from them, his cloak folded over his arm. He gave Snowe a nod as he passed before he quickened his pace.

"Done already?" Astra asked as Snowe and Erio stopped in front of her.

"We didn't find that much," Erio admitted. "The rest of the books weren't in there aside from two. We still need two more to complete the set."

Canan frowned. "I'm sorry," she said. "I didn't actually think anyone would move them."

"We were told Morzin might have more," Snowe pointed out. "Zuan is apparently missing so it's better to go now, right?"

Astra nodded and moved toward the inn doors. "Sounds like a plan," she said. "We should fill Hiante in."

She led the way through the inn and once they entered Astra and Hiante's inn room, they found Hiante was up and moving, though very slowly.

"Tired?" Erio asked as they came in.

"Too much walking yesterday," Hiante replied.

"Did you suddenly get old while I wasn't looking?" Erio asked with a smirk.

Hiante laughed and shook his head. "Did you two find anything in the castle?"

Snowe quickly filled him in and once finished, Hiante leaned against the table and crossed his arms. "Did you send a letter back to Richard and Vera telling them where we're going next?" he asked. "It's the least you could do."

"Oh, I should," Snowe said and nodded quickly. He looked at Canan. "I-Is there a good place I can send a letter?"

Canan nodded and smiled. "The castle has a post system in place with a few well-trained hawks. One for each major city and I think Aldcoast was one of those," she said. "My father uses them all the time so I'll show you and then you can help me get some stuff from the castle so I can help Hiante out."

"What are you getting?" Erio asked.

"I need ingredients for a pain-deluding salve," Canan explained. "I can share the directions with you too if you want!"

"Lead the way then!" Snowe said.

Canan grinned at them both and led them back out of the inn room, leaving Astra and Hiante alone. As Astra moved around the room to collect hers and Hiante's things, Hiante moved to help her.

"Don't worry! I have this covered," Astra said quickly.

Hiante paused for a moment as Astra moved to their bags and he frowned. "You went back to those ruins, didn't you?" he asked.

"Is it obvious?" Astra asked as she stuffed her clothes into her bag before she moved to Hiante's bag.

"Did you find anything with Canan in tow?"

Astra shook her head and finished putting all of Hiante's things back in his bag. She clasped it shut and looked at him. "I took Canan's brother too, but all they felt really was that it was familiar," she said. "Iain looked through the skylight but it was like a whole different world." She closed her bag and pulled it up. "We should get moving though. I don't think we can do anything with it anyway."

Hiante nodded. "If you insist," he said. He moved over to his bag and as he pulled it up, he groaned. "I really hope she comes back with that salve soon."

"You don't regret having a body, do you?" Astra asked as she grabbed Hiante's swords from where he had left them.

"Of course not." Hiante smiled at Astra as he took his swords and attached them to his belt. "I just don't like feeling old." He reached over and patted Astra on the shoulder. "Come on, let's find a nice place to sit outside to wait for them to come back. I don't feel like chasing them all over a castle."