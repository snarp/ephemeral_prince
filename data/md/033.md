# Chapter 5 - "Are you sad?"

The morning sun streamed through the window and Snowe felt the rays warm his face as they came through. No nightmare to speak of; Erio's plan had worked. As Snowe opened his eyes, elated, he found himself in a different place than where he had fallen asleep; he could see the top of the bed's headboard. He always slept on his side. Snowe's relief and elation washed away and as he wavered, he realized warmth came from his hands. A pleasant warmth that worked its way up his arms and filled his body the longer he stayed still.

_Why?_ Snowe thought to himself, too afraid of the answer, and he looked down. Underneath him lay Erio and Snowe held his breath as he realized his hands were wrapped around Erio's throat. Erio wasn't awake, however, and he barely struggled underneath Snowe's weight.

"_I don't like him_," a voice sung in Snowe's mind. "_Why do you_?"

Nausea and panic rose up in Snowe as he took his hands away from Erio and, in his attempt to get off Erio gracefully, Snowe stumbled off the side of the bed. He landed with a thump and as he tried to push himself up, he found his limbs shook with each movement, like he had no control of them. They relented nonetheless, however, and all the warmth disappeared as though it had never been there, and he pushed himself off the floor. He moved as far away as he could from Erio and once his back hit the wall, his legs folded underneath him, letting him tumble back to the floor. The demon's voice laughed in his head and it echoed so loud, Snowe curled up and covered his ears, willing the voice away.

He wanted it to be the nightmare; he'd give anything to wake up and realize it hadn't happened. No matter the wishes, no matter how hard Snowe pushed against his ears to stop the laughing, he knew it was real.

Erio's groan from the bed stopped the laughing voice in Snowe's head and as he sat up, Snowe realized his breathing was ragged. Snowe tried to flatten himself against the wall to disappear--anything to pretend what happened had only been a figment of his imagination and Erio didn't notice it--but Snowe remained where he was. Erio reached up to feel his neck and Snowe felt his stomach plummet. Of course he would notice. When Erio looked at Snowe, confused, Snowe shut his eyes tight and curled up tighter.

"What's wrong?" Erio asked.

"I didn't mean to," Snowe whispered.

Erio paused and he felt his neck again before his eyes widened. As he stood from the bed, Snowe shook his head again. "I'm fine," Erio reassured as he moved closer. "You didn't hurt me."

"What if he made me set the whole inn on fire?" Snowe asked. "I could have killed you, killed Astra, killed everyone else here."

"But you didn't." Erio knelt in front of Snowe. "It's got to mean something--you made sure he wouldn't."

Another shake of his head; it wasn't that simple. The demon was toying with him again to draw out the suffering. It had nothing to do with strength or anything of the sort. Snowe flinched as he felt Erio's hands at his arms.

"I should teach you how to meditate," Erio offered as he pulled Snowe's arms down. When Snowe refused to look directly at him, Erio sighed and sat cross-legged beside Snowe. "It'll help you keep a calm head."

_No it wouldn't_, Snowe wanted to say, but he nodded nonetheless and sat as Erio did.

"Breathe evenly and concentrate on that," Erio explained and he took a deep breath. "Don't think of that demon or anything else all right?"

Another deep breath and Erio closed his eyes. Snowe watched him for a moment before he finally followed suit. The demon's voice had gone from his mind and he wondered if the demon had become bored because Erio was awake; his lone audience was distracted after all. Another shiver went through Snowe's body and he took another deep breath.

"You're stronger than the demon you know," Erio pointed out quietly. "You're not awake in your own head and that usually means he can take charge of your body, but you're still here."

"He's j-just toying with me," Snowe replied as he glanced at Erio.

"I'm pretty sure I'm the right one here," Erio said and smirked.

"I'm sorry I let that happen." Snowe took another breath and tried to relax. "After what you even did for me."

"You didn't let it happen," Erio corrected as he flicked Snowe in the shoulder. "Don't blame yourself like that. It'll just make it easier for the demon to convince you that you're not as strong as you are." He wrapped his arm around Snowe's and Snowe sighed as the warmth went through him again. "I have a few ideas in mind for when we get back to Aldcoast that might help you sleep for sure. No vivid dreams at all."

Snowe nodded. "J-Just f-focus on breathing then? That's what you said?"

"Easy, right?" Erio grinned.

Snowe returned the smile and straightened his back as he took in another breath. Once he closed his eyes, he concentrated on his breathing and the warmth that emanated from Erio. Breathe in, breath out--in and out.

* * *

Astra groaned as a loud knock reverberated from the door. From beside her, Canan groaned in response and stole the pillow Astra had underneath her head and she covered her ears with it. Astra gave the priestess an amused glare, but Canan didn't move to get up and Astra gave in, sitting up and letting out a yawn. The sun came through the windows already, warming the room, and as she stretched, the knock came again.

"Give me a minute!" she called out as she slipped out of bed and went to collect her clothes. She changed quickly and once she had everything on, made sure her hair lay flat, she stepped over to the door and peeked out. The merchant that had brought her, Erio, and Snowe to Lura stood outside, already dressed for traveling.

"Good morning," he said. "I think we should head back pretty soon if that's all right with you?"

Astra slipped out of her room and closed the door behind her. "Is something wrong?"

"Probably better to show you." The merchant glanced down the hall and rubbed the back of his neck. "Come on."

After Astra had reached back in her room to grab her boots and slipped them on, she followed the merchant through the inn. He took her to the lobby and once they stood right outside the main entrance, he pointed toward the end of the street.

A dazzling and unearthly man stood with a few others nearby as he spoke with them; his hair was a blonde that reminded Astra of the sun, and he had it brushed straight back. Astra could see light blue markings on the man's face and he wore light colored clothes with a red sash at his waist. Though he smiled with those he spoke with, Astra couldn't help but feel a sadness coming from him. He looked as ordinary as everyone else, his clothes maybe far cleaner than everyone else's, but as Astra stared at him, she felt her skin crawl.

"That's Beliaz," the merchant said and he chuckled. "This your first time seeing a lord? I had the same expression when I was a kid."

"Really?" Astra shuddered. "S-Should we worry?"

The merchant laughed. "If I can tell within a day or two your friend is a demon, Beliaz will pick it up in no time if he's even close to him." He looked back toward the lord and crossed his arms. "Granted, Beliaz is like a puppy; he won't hurt anyone, but if he's here... well, let's just say it's best if we high tail it out of here for now."

"Wait, how long have you known Erio was a demon?" Astra asked as she moved back into the inn.

"Aldcoast is too polite to tell ya what ya already know," the merchant replied as he followed her in. "We don't really care what he is; he does good work. Hate to see him gone."

"When are we leaving?"

"Whenever you're ready," the merchant said. "All my stuff is already packed so collect your boys and I'll meet you at the southern gate."

He gave her a slight bow before he left Astra to head back up to the rooms by herself. She went to Erio's and Snowe's room and after she had pulled her hair back into a pony tail, she took a deep breath and knocked.

"It's unlocked," came Erio's voice.

Astra peeked inside and smiled; the two sat on the floor as though they had been meditating. Although Snowe had fallen asleep against Erio's shoulder, the demon remained still. She gave Erio a wave.

"I'm sorry about before," she said.

"I should have told you," Erio replied. "Did you think of anything?"

"I did." Astra nodded and came closer. "Canan said that the White Cloud Temple might be able to help us out," she explained. "We've got nothing else so I thought it would be worth a visit."

After Erio nodded, they went quiet. With a roll of his eyes, Erio shook Snowe's shoulder and Astra kept down a laugh.

"Huh?" Snowe straightened back up and rubbed his eyes. "Did I fall asleep?"

"Should have taught you meditating a long time ago," Erio joked as he stood up.

Snowe laughed weakly and looked at Astra. "What's wrong?" he asked. "A-Are we leaving already?"

"There's a lord in Lura," Astra said. "We might as well head back to Aldcoast for right now."

As Erio reached for his things, he paled. "Wait, Canan won't tell the lord we're here, will she?" he asked.

"I don't think she will." Astra turned back toward the door. "I'll go ask her while you two get ready to leave."

Once they nodded and went to collect their things, Astra slipped out and headed back down the hall for Canan's room. The priestess had awoken as well and hadn't moved away from the bed. She gave Astra a sleepy wave.

"Morning," she said.

"Don't turn us in."

Canan paused and stared at Astra. "For what?"

"There's a lord in Lura now."

"Which one?"

"Beliaz."

Canan relaxed and smiled. "Beliaz is nice," she said. As she stretched, she paused and cringed. "Oh. If he's here though, Zuan will be close behind. He's the one you should watch out for."

"The temple won't hurt Snowe or Erio, will they?" Astra asked. "None of the other lords will be there aside from the one who sponsors it, right?"

"Sephi doesn't like many other lords in her temple aside from Beliaz," Canan replied as she undid her braid. "Even if he shows up, Sephi won't allow Zuan very far inside and nor will she let him hurt anyone within her walls. So I think they'll be safe." As she began to braid her hair, she gave Astra a smile. "Do you want me to distract Beliaz so you can get going without getting stopped?"

"If you wouldn't mind."

Canan grinned and nodded. "Of course I wouldn't!" she said. "Been meaning to check to see how he's doing anyway."

As the priestess went to grab her clothes from her bag, Astra grabbed her bag from where she had left it before and headed back to Erio and Snowe's room. They had already changed into clean clothes and were ready when she came in. Once they made sure they had everything, they started for the stairs. Barely a few steps down before Canan went past them, already dressed to perfection, and gave them each a smile before she sped off.

True to her word, Canan had quickly approached the lord and as Astra led everyone past the two, she saw Canan had wrapped the lord into a tight hug. Unlike before with the others he had been speaking with, he looked genuinely happy to see her. As they became engrossed in conversation, Astra focused on where she was going and soon they reached the southern exit. The merchant greeted them happily and after they helped him light some candles to thwart off any nearby phantoms on the road, they were headed back to Aldcoast.

* * *

By early evening, they had returned to Aldcoast and dark clouds had rolled in. Erio took Snowe back to Richard and Vera while Astra headed home to the apothecary shop to get Hiante caught up on everything that had happened.

A customer left the shop as Astra neared and after they exchanged a quick greeting, Astra slipped into the apothecary shop. Hiante sat alone behind the counter and he had begun to stand up until he noticed Astra. He gave her an awkward smile, as though still not used to facial expressions, as Astra waved at him.

"You three are back already?" he asked.

Astra nodded as she turned over the sign in the window and locked the front doors. When she turned back to Hiante, his smile had disappeared.

"What's wrong?"

She wished he hadn't asked that; though she had expected he already knew something was up, she had hoped she had hidden any indication of worry from her expression. What he had felt before was a fluke she wanted to say and laugh, but she couldn't lie to him. She looked away to try to come up with a way to tell him lightly that Snowe's demon wanted her dead most of all, but words didn't come right away. She hadn't even noticed Hiante had moved closer and she jumped in surprise when she found Hiante's arms around her, giving her a tight hug.

"I-I don't remember getting hugs from you very often," she admitted as she tried to keep her tears back.

Hiante returned Astra's laugh. "I always worried I had a spider somewhere on me before," he admitted. "I didn't want it to jump out and scare you if I had."

Astra giggled as Hiante gave her a squeeze before he withdrew.

"What's wrong?"

A deep breath and Astra gave Hiante a smile. "We should sit down," she said.

* * *

When Erio and Snowe reached Richard and Vera's place, Snowe stopped Erio. "I'll be fine from here," he said.

"Are you sure?"

Snowe nodded. "Thank you though," he said.

Erio gave him a hard look but when Snowe didn't relent, the demon nodded. "I'll be back later then when I find something you can take."

Without another word, Erio disappeared with a flash of light and alone, Snowe turned back to the door. Many different questions sprung to mind at once but he didn't want to ask them; he wanted to walk in with a smile, pretend he was still doing just fine. By now he knew Richard and Vera would have finished dinner and if River had been too fussy earlier, one of them would be trying to feed her now. A perfectly normal and peaceful life like they had both wanted. No matter how wide the smile, Snowe knew the scars on his face and neck would spoil any lie he would have come up with and once again he would ruin what they had.

Snowe sighed as he pushed open the door and walked into the kitchen; like he had guessed, Richard sat at the table with River, who without missing a beat grinned at Snowe as he entered, while Richard tried to feed her, and Vera stood near the sink as she cleaned out the dishes they had used earlier. Richard looked at Snowe right after his daughter had and though his lips began to smile, they quickly drop into a frown as his eyes widened.

"Snowe?" He jumped up, dropping the spoon with River's dinner on it, and quickly made his way to Snowe. "What happened to your eye?"

"It's fine," Snowe said as Richard held his face. "D-Don't worry."

"You have a scar going through your eye," Richard replied. "I am going to worry--what happened?"

"Edgar wasn't my father, was he?"

Richard stammered into silence as Vera turned away from the sink, her eyes as wide as Richard's. The bowl she had held slipped from her hands but she caught it before it shattered on the ground. Neither managed to get any words out before River squirmed in her chair. Vera took her eyes away from Snowe and as she went to her daughter, Richard glanced back at Vera.

"Vera, could you get River ready for bed? I think she ate enough," Richard requested.

"Do you want me to--"

"I've got this covered." Richard's shook as he returned his gaze to Snowe.

Vera nodded and she had River in her arms within seconds. Though she had made a step toward the living area, she turned on her heel and came closer to Snowe. She gave him a tight hug (River even attempted to give Snowe a hug of her own) and after she kissed Snowe's forehead, she made her way out of the kitchen. When she had gone, Richard exhaled and took Snowe to the table.

"How did this come up?" he asked as they sat down.

"It doesn't matter," Snowe replied. "He wasn't my father, was he? The Original King was, right?" When Richard didn't open his mouth to say anything, Snowe frowned. "Did you always know?"

Not even the barest hint of a nod or shake of the head, Richard frowned and glanced away. Before Snowe could say anything else, Richard gave out a sigh and removed his glasses. He cleaned the lenses on his shirt before he placed them back on his nose.

"I always had my suspicions," he admitted and matched Snowe's gaze again. "Edgar never told me and Lina wouldn't admit it, but you looked nothing like Edgar. All you even shared with your mother was her eyes and complexion."

"Why?" Snowe asked, though he had already come up with an answer himself. "W-Was I j-just born to have this demon inside of me? Did my mother even love me?"

"In her own way I'm sure she did," Richard replied. "When we left Sabine, she wasn't the same person she had been. Same with Edgar. Both of them had completely changed."

"All this time I assumed my fath--_Edgar_--hated me, but I always convinced myself I was wrong because I was his son. That everything was because he was frustrated," Snowe said. "But I'm not. That's why he hated me, isn't it?"

Richard reached forward and pulled Snowe into a tight hug. "That doesn't matter now," he whispered. "What does is that you're here now. I didn't want to tell you because I didn't know how." He withdrew but kept his hands on Snowe's shoulders to keep him steady. "It doesn't matter who your parents are. You're still the same Snowe."

Snowe nodded and Richard watched him for a moment before he sat back in his chair. The kitchen remained quiet until Snowe shifted in his chair; he knew Richard stared at the scar, but he had no idea what to say about it.

"What happened?" Richard asked. "You didn't have a scar when you left."

"I'm still not better." Snowe sat forward and put his hands together. He could feel a headache coming on and he tried his best to will it away. "A-Astra had another idea we could try though. A-At Barina in the desert."

"But you're all right?"

Snowe shrugged. "Considering everything? I think I am." He sighed and stood up. "I'm just going to take a bath, all right? I just want time to think."

Though Richard looked as though he still wanted Snowe nearby, he nodded and stood. Another tight hug before he let Snowe walk away from him. It was all too evident Richard wanted to talk more, but Snowe had nothing else to say. All he wanted to do was relax so he didn't have to think about any of this.

As Snowe moved through the dark house and neared his room, he paused at River's nursery where light from a lamp inside spilled out into the hallway. Vera hummed to her daughter and Snowe imagined River would fall asleep any moment if she hadn't already. Snowe sighed to himself and he left Vera and River be as he moved into his room and shut the door. He wondered if he should have come back.

After a quick shake of his head, Snowe moved to the small bathroom connected to his room and he prepared for a bath. He didn't want the thoughts to get to him; dwelling on it now wouldn't do any good, he was home after all. As Snowe turned on the faucet and let the bath fill up with hot water, Snowe pulled off his tunic and undershirt before he checked all his scars in the mirror. With how Erio had reacted, Snowe prepared for the worst scars he had ever seen, but as he studied them in the mirror, he felt as though they had been with him for a long time. As though he couldn't imagine himself without the scars. But why? Snowe ran his fingers along the scar across his eye as he thought, hoping something would come to mind, but the harder he tried to recall, the more his eye stung and his headache only grew worse.

"_Are you sad_?"

Snowe snapped his eyes open and found the demon beside him, the grin absent, and he watched Snowe through the mirror.

"One way to end all of this," the demon said as he leaned closer. "You know what to do."

Quickly, Snowe stepped away and flattened against the wall near the mirror, but the demon had disappeared. The water flowed over the edge of the bath and Snowe cursed as he reached over to turn the faucet off. How long had he been lost in thought? Like all his other questions, the answer didn't come and Snowe shook his head as he finished undressing. No other scars had appeared anywhere else on his body and Snowe breathed a sigh of relief as he slipped into the tub. Though the water stung against Snowe's skin as he sunk down, he didn't mind; under normal conditions he knew it would be unpleasant, but right now it kept the chill at bay and Snowe breathed in deep. The headache didn't leave him alone, however, and as Snowe shut his eyes, he concentrated on breathing.

_Breath in--breath out_.

* * *

With a twitch, Snowe awoke and jumped; he was still submerged in the bath and the water had grown cold. Snowe let out a groan as he ran his hand through his wet hair to get it out of his face; he couldn't even recall settling in the bath, but his skin had wrinkled and it was all too obvious he had been in too long. As he slipped out to dry and get into something warm, he paused. He felt more refreshed than he had in the past few months. No nightmarish images came flooding back to him--he must have slept soundly for once.

Relieved, Snowe finished dressing in higher spirits and as he stepped back into his room, he noticed the sun had completely set. Only when he left his room to find Richard and Vera had already gone to sleep did he realize how late it was. Astra and Erio came to mind and Snowe decided to see if the two were still awake.

Night in Aldcoast was as pleasant as could be; it didn't have the same loud bustle Lura did in the night and didn't nearly have as many lights. He could see all the stars overhead as they peeked out of the clouds which must have parted earlier. The smell of rain covered the port town and the ground was soaked with rainwater. _At least it hadn't thundered_, Snowe thought to himself as he started for the apothecary shop.

The lights in the main room were still lit and as Snowe neared the door, he realized someone had opened the windows. He could hear Astra, Erio, and Hiante's voices from within.

"I'm still not happy no matter how much you apologize," Hiante said. "All this time he could--"

"But he didn't," Astra insisted. "I'll be fine."

"I assure you he won't hurt us," Erio added. "If he hasn't already, he has it under control."

A pause and Snowe leaned against the apothecary shop's walls and looked at his hands. The image of him trying to harm Erio came back to the forefront of his mind and he shuddered. He could still feel Erio's throat in his hands. How warm it had been; how he really had tried to harm Erio.

"We still have to help," Astra said. "Canan said that they might have something in the White Cloud Temple."

"It's our only lead, we might as well see," Erio pointed out. "Demons couldn't help so naturally if it's as Canan said, a spell the lords had made, maybe they have a way to get him out of Snowe."

Their conversation continued, but Snow's attention hadn't left his hands. There was no telling when he would break down completely and he knew if it did happen, Erio and Astra would both be in trouble. Restless night or not, it was only a matter of time before the demon gained control. Images of Astra dead and bleeding filled Snowe's mind and he took a deep breath. He needed to get away from everyone to keep them safe. If something happened and if he was far away, at least Astra could prepare.

Another deep breath and Snowe started for the gates at the edge of town. Richard and Vera would be safe in Aldcoast as well with River. He didn't want to drag them into his own problems. Everyone would be better off if he did it alone. He could still remember the way to Lura; the merchant hadn't made many turns and he was confident in himself he could find his way back.

A flash of light and Snowe felt warm hands at his shoulders; Erio had appeared before him and the demon had a frown on his lips.

"I forgot you could do that," Snowe said.

"You're not going anywhere alone," Erio replied.

Snowe sighed and tried to brush Erio off, but his hold remained strong. "Wouldn't it be better if I did?" he asked. "I don't want you to be hurt--nor Astra. Or anyone else. It's not fair that I drag all of you into this. I can do this on my own."

"Snowe!"

Snowe jumped and spun around. Astra and Hiante had followed him as well and Snowe felt his stomach plummet as he saw Astra's expression; her normal pleasant expression gone, her brow furrowed, and her lips in a frown. Snowe had never seen her mad before.

"What has gotten into you?" she asked.

"I don't want to see you hurt."

"If you leave alone, you're the one that'll get hurt. Do you think we want to see that?"

"At least it's not you."

"I am not dense, Snowe!" Astra shouted. "I know what you're thinking--if you can't find out what to do, you'll kill yourself, right? Maybe that'll be rid of the demon, right?"

Snowe froze in place. "I-I don't--"

"Have you even thought about Richard or Vera?" Astra asked, exasperated. "Or anyone else for that matter? You act like you don't have a friend in the world, but we're still here and if you disappeared like that, we'd feel it." She paused and when Snowe looked away she took a deep breath. "We will help you Snowe, not because we feel sorry for you. But because we are your friends. We want to see you get better."

When Snowe couldn't fumble out a reply, Astra turned and headed back for the apothecary shop. Hiante looked as though he wanted to add something, but he gave Snowe a stern look instead, and turned to go after Astra. Alone with Erio, Snowe started shaking again; he had really thought he would do just that and coming face-to-face with the thought scared him. Lost in his thoughts, Snowe flinched as he felt Erio pat his back.

"Don't make us chase after you," Erio said. "It's better if we were together on this."

"If..." Snowe looked at Erio sadly. "If he does get control of me, you'll kill me, right?"

Erio looked at Snowe, alarmed, and withdrew his hand. "Snowe--don't even--"

"You're here because of Astra," Snowe interrupted. "Not me. You have to protect her. Forget we're friends now. If he does take control, you have to kill me. I don't want to harm her or anyone else." When Erio didn't reply right away, Snowe turned and grabbed his shoulders. "Please. Something isn't right with these dreams. I have this feeling I killed her before--killed everyone before. I don't want it to happen."

Erio watched Snowe sadly and when Snowe kept his gaze and wouldn't let go, Erio sighed. "I promise," he said. "But we won't let it come to that. You're stronger than that."

Snowe smiled and nodded. "I-I know," he said.

"Let's make a stop at the shop before I make sure you stay home this time," Erio pointed out as he pulled Snowe down the road. "I have a few tonics made up for you and I made you a new charm that I hope won't lose the piece I put in."

"Piece?" Snowe asked. "W-What do you make them w-with?"

The demon just shrugged and Snowe laughed weakly; he started to name off guesses and eventually Erio stopped and flicked Snowe in the forehead for such absurd suggestions, only making Snowe laugh even more.

Though Snowe knew his mind wasn't entirely at ease, he focused on the distractions at hand and happily followed Erio back to the apothecary shop. They were right; it had been a bad idea. He knew he could focus on being strong if only for a little while longer.