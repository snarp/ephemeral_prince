# Chapter 13 - "Overgrown path with lilies all year round."

![](images/014_fleeting_13.png)


The next morning, Canan woke early with a plan in mind: ask around delicately if someone knew where an entrance to the land of demons was nearby. As she dressed and washed up, she had no idea how to be delicate about it. Every single question that popped in her mind was far too obvious. If she asked the wrong person, she knew she'd be in trouble as they would find the nearest lord and tell them everything.

Rhea still slept as Canan exited their washroom and Canan quietly stepped out of their room. Might as well let her sleep, Canan thought to herself as she shut the door. Delicate wasn't Rhea's forte. Once in the hall, Canan tip toed down to the end of the hall and listened in. Since she heard nothing through Snowe and Iain's door, she assumed they still slept as well. Neither would be much use in asking questions; Snowe hadn't spoken much after they had decided what to do (and Canan didn't want to put him through any more than she had to) and Iain was just as delicate as Rhea was.

As Canan exited the inn, she sighed to herself as the cold hit her face. The chill hadn't let up and the sun hadn't even risen yet. Her breath came out as a white cloud and she watched it as she thought to herself. How to broach the question without arising suspicion?

"Already awake?"

Canan yelped and jumped. The skeleton from before was at her side in mere moments. She was astonished that she hadn't heard the clatter of his bones or the sound his cloak made when he moved.

"Did you sit out here all night?" Canan asked.

"I had nothing else to do."

"I should get you a new cloak," Canan conversed as she glanced around. Aldcoast was empty this early, thankfully, and she sighed in relief to herself.

"You don't have to."

"If we're going to be leaving, I should get you one that you can fully hide underneath," Canan pointed out. When Hiante gave her a slow nod, she fidgeted with her braid. As she tightened it, she glanced back at Hiante. "You know, you-you should let Snowe handle this on his own terms," she stammered. "Threatening him isn't going to help either of you."

Hiante folded his arms. "If it was your brother he had killed, how would you feel?"

There goes diplomacy, Canan thought to herself as she dropped her arms at her sides. She wished she hadn't brought it up. "Do you have any idea where to get information from then?"

Hiante nodded toward the inn. "A few travelers are milling about, aren't they?" he suggested. "I would have asked myself... but I'm certain they'd try to bash my skull in if I came close."

Canan looked back at the inn for a moment and looked toward where the small dining hall was. All the windows were alight and even from here, she could hear the bustle from within. She looked back at Hiante.

"I'm sorry," she whispered.

"If only it was your apology I was looking for."

"Stay out of sight then." Canan passed Hiante quickly and moved toward the dining hall doors. When she looked back to make sure Hiante hadn't followed her, he had already gone back to hide someplace nearby. She was glad he hadn't tried to harm Snowe in the night; they had all been worried about that. Iain had promised to stay up to make sure nothing happened, but Canan knew he had probably fallen asleep before long. He was never one for night guard duty.

As Canan entered the dining hall, she saw a few faces she didn't recognize and a few familiar ones that had been in Aldcoast for a few days already. They ate meals where steam practically wafted off the food and heated up the room; just perfect for a chilly morning. As Canan stayed near the doors and studied the patrons' faces once more, she sighed to herself. Everyone looked friendly enough but she had no idea if they would tell a lord about her if she started asking about the land of demons. In frustration, Canan cursed to herself and studied the faces once more. This time, she stopped on a woman at a small table pushed toward the far corner. Her skin was dark like she had come from the desert and her black hair was done up in a bun with many braids leading toward it. It took Canan a moment before she recognized the woman; Lera, the woman who helped move the transports back in Barina. Without Parin at her side, Canan almost didn't recognize her. For as long as Canan recalled, Lera and Parin had never been far from each other, as though they had been joined at the hip. Canan knew she could trust Lera and made her way over to the small table.

"Lera!" Canan called out cheerfully as she drew close.

Lera flinched and spun in her chair, a hand on the dagger strapped to her belt, ready for a fight. When she met Canan's gaze, however, her own softened and she smiled. "Wow, is this where you fled to?" she said and laughed. She pulled out the chair next to hers and patted the seat. "Well, sit down, honey. I've missed seeing your face."

Canan returned the smile as she settled in the chair. "How have you been?"

"Not bad. Making money elsewhere is a good change of pace even though I miss the desert," she said.

"What do you do?"

"I escort carriages coming out of Lura," Lera explained as she turned back to her breakfast. "It's not bad. I much prefer the transports though. They are much more fun. You priestesses make much better conversation when you want to."

"I miss Barina too," Canan said sadly.

"Did you guys settle here to help the doctors then?" Lera asked.

"We did. Though we have to leave soon," Canan admitted quietly. She leaned closer to Lera. "Please don't look at me funny, but have you heard about any entrances nearby to the land of demons?"

Lera stopped, her hand shaking as she held her fork in the air, and she watched Canan with wide eyes. She dropped her fork, it clattered against her plate, and she glanced around the dining hall wildly as though expecting an attack. When none came, she put her arm around Canan and pulled the priestess closer.

"Are you crazy?" she whispered. "Quite sure some of those people have lords hiding inside them."

"W-what?" Canan tried to glance back at the dining hall but Lera kept her firmly in place. "I'm sorry, I thought I was being careful. How do you know that?"

"A hunch--a very well-studied hunch," Lera replied. She glanced at the dining hall but still, no one had looked their way. Quickly, Lera stood from her seat and with her arm still firmly around Canan's shoulder, she took them both out of the dining hall and back outside. No sooner had Canan felt the cold against her cheeks, Lera dragged her away from the inn. Canan wriggled free from Lera's grasp just enough so she could glance around and she saw Hiante start after them, but one stern glance thrown his way and he was out of sight.

"Lera, what's wrong?" Canan asked as Lera stopped moving and took her arm away. "Was it really that bad? I don't think anyone even looked at us."

"It sure is that bad. Lords don't want anyone going near the land of demons. Anyone who knows how to get there can get into a lot of trouble now," Lera explained. "I mean, why are you going there? What business would a priestess have there?"

"I just need to--I can't explain it right now," Canan said. "I'm not doing it for a lord, I promise you that."

Lera watched Canan a moment before she sighed and rubbed the back of her neck. "Near Lura," she admitted. "Right outside the eastern gate you can find an overgrown path with lilies at the sides all year around. You follow that and go down to a tiny shore. The grave's right there you can enter; it has an amaryllis engraved on it."

Canan stared at Lera; that had been easier than she had thought. "How did you come across it?"

"Neither here nor there, honey," Lera said. "My carriage is going back to Lura tomorrow morning. If you want to hop on, I'd be glad to see you safely there."

"I have others who need to go with me," Canan pointed out. "Are you sure we wouldn't be any trouble?"

"Of course not. I miss familiar faces." Lera grinned and patted the priestess on the shoulder. "I'll meet you at the gates tomorrow, all right? Bright and early."

Canan giggled. "Do you still leave earlier than you're supposed to?"

Lera laughed and shook her head. "Don't worry, I'll wait for you." She gave the priestess a wink before she headed back for the dining hall.

Alone, Canan sighed in relief. As she turned to head back toward the inn entrance that was closest to her room, she jumped. Hiante was right next to her as he watched Lera return to the dining hall.

"How do you do that?" Canan asked, exasperated. "I never hear you as you approach."

"Ah, I just got used to walking quietly," Hiante said, a smile to his voice. "The two I raised were such light sleepers, I just naturally picked up the habit."

Canan frowned. "W-were they related to you?" she asked.

"I'm a skeleton," Hiante reminded, the smile gone from his voice. "I've been dead for over a hundred years I'm sure. They weren't related to me at all." He went quiet as he continued to stare at the inn, though Canan was sure he didn't acknowledge it. His mind was elsewhere.

"I-I'll get you a cloak as soon as I fill everyone in, all right?" Canan said as she fidgeted with her braid again.

A small nod, but barely any acknowledgement still, and Canan went back toward the inn. His voice had been so sad and Canan wished she hadn't said anything.

* * *

Lera made good on her word; the carriage she escorted was owned by a stout woman who had been collecting a few things around Aldcoast to sell in Lura and she didn't mind having others ride in back with her things. By the following morning, everyone had packed things easy to carry and left what remained with the doctors in Aldcoast. Snowe made sure to take the items that reminded him of his previous life; on the off-chance he wouldn't return to Aldcoast, he wanted to make sure he had them close by.

Once they had met Lera and the merchant by the carriage, Snowe helped the merchant charge small charms hung inside the carriage to keep everyone warm. All it needed was a small flame in the glass enclosure and a spell placed on the glass kept the heat circulating through the carriage for hours. As everyone settled into the carriage (Hiante decked in a new cloak courtesy of Canan and it kept every inch of him covered), the interior was as warm as could be. Still a far cry from the first carriage ride the first time Snowe left Aldcoast, but he knew he could handle it. After everything had been secured and everyone was on, the carriage started for Lura.

The trip went by peacefully and although Snowe expected Hiante to bring up an unfriendly conversation, he kept quiet as though his mind was elsewhere and Snowe was glad for that. Already tormented enough, he wanted to keep some peace within himself as much as he could. When they arrived at Lura at dusk, Canan attempted to give Lera a hefty sum for the ride, but Lera wouldn't take it and only wished Canan and everyone else all the luck they would need.

Even after a suggestion from Iain to rest up, Snowe refused it and he led the way out of the eastern exit of Lura to head off. He wanted to get everything over with as soon as possible. Either figure out what he could do or learn he really couldn't do anything and accept his final fate. The overgrown road right outside the eastern gate was easy to find and the lilies proudly poked through the fallen snow, almost gleaming underneath the moonlight overhead.

The path became narrower the closer they moved toward the cape and eventually it led downward in a steep slope toward the tiny inlet right beneath the cape. The shore was hardly big enough for even a small boat and tiny lilies sprouted up from the sand, still leading the way. They followed the lilies around the shore until they found a small alcove that had been dug into the cliffside. A lone lamp stood right outside, clearly weathered down, but the light from the glass orb up top shone brightly still.

"This is just creepy," Iain remarked as they moved into the alcove. "I really hope demons aren't going to jump us."

"Why would they do that?" Hiante asked.

"I doubt they can see we're from the White Cloud Temple," Rhea remarked. "We've been away long enough I'm sure we don't even smell like lords anymore."

Snowe held out his hand in front of him and a small fire lit up in his palm. A few feet before them sat a dark tombstone, weeds growing up all around it. Upon closer inspection, Snowe sighted the engraved amaryllis on the front and he sighed in relief. Warmth spread through his chest, coming from a spot on his left, as he studied the tombstone. While the feeling was curious, nothing happened as he approached the tombstone. Frustrated, Snowe looked back at Iain.

"How do we get in?" he asked.

Iain crossed his arms. "I'm trying to remember," he said quietly. After a moment, he glanced at Hiante. "Would you happen to know? Your one demon didn't mention anything?"

Hiante shook his head. "The demon was too young to know himself," he replied.

Rhea snorted. "Well, we can't get in," she said. "Let's leave for now and come back in the morning.

"We're here though, might as well keep trying," Snowe said. He handed the small ball of flame to Hiante and dropped to his knees. He placed his hands on the soft ground before the tombstone and took a deep breath. Warmth raced up his arms and filled his entire being. It almost felt as though he were home. "Please, let me in," he told the tombstone.

"Would it really be that easy?" Iain wondered aloud. He dropped to his knees next to Snowe and placed his hands on the earth. "Come on, let us in!" he said. "We have to talk to someone. We're not going to cause a ruckus, we promise."

Snowe began to pick up his hands, feeling just a bit silly, but when he realized his hands had submerged themselves in the dirt, he looked at them wide eyed. Iain, with the same expression, grabbed Snowe's arms and started to pull, but in mere seconds his knees had sunk into the ground.

"You're sinking in!" Canan said from behind them. She grabbed hold of Snowe's cloak and gave it a tug. A mere second after, she stopped and yelped. Snowe and Iain looked back at her and found that her ankles were deep in the ground. "Why are we sinking?!"

Before Rhea or Hiante could haul anyone out of the dirt, Snowe felt the ground give way underneath him. Iain grabbed onto his arm and had his other hand grasped tightly around his sister's wrist and all three of them fell into a darkness that swallowed them up, ridding their vision of light. Briefly, Snowe heard Rhea scream after them, but her voice cut off as the darkness overcame his senses completely.