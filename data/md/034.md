# Chapter 6 - "The sun blonde lord."

![](images/034_permanence_06.png)


Despite the thoughts from the night before, Snowe awoke more energetic than he had felt in days. It didn't help to shake away the worry and dread, however; was the demon planning something or was he in fact tired from all what he had tried to do? Was taking control of Snowe's body that exhausting? Even still, as Snowe slipped out of bed and dressed, he knew thinking too hard on it would only disturb him further. What was going on didn't matter, only that Snowe could still fight back against losing control of himself.

He had new charms from Erio and they worked just as well as the last ones as Snowe took them from his nightstand; a moment to warm his hands before he placed two in his pockets and let the last one hang from his neck. Before Snowe left his room, he packed his bag with clean clothes, even a few necessities, and when Snowe stepped out, he took a deep breath. The house had filled with breakfast scents and with a smile on his lips, Snowe followed the aroma to the kitchen.

Vera sat at the table, River nearby accepting whatever breakfast her mother had concocted earlier, and Richard stood at the stove as pancake batter hit the frying pan and sizzled.

"Feeling rested, Snowe?" Vera asked as she looked up from River.

Snowe nodded and set his bag next to his chair. "Surprisingly," he said, relieved. With a tone like that, Snowe knew neither of them must have realized he had slipped out the previous night. As he sat at the table, he sniffed the air and smiled. "That smells nice," he said.

"I can't remember if we made pancakes for you yet," Richard said as he took the finished pancakes and set them on a stack nearby. After he turned off the burner on the stove, he brought the fresh stack of pancakes closer and served them up between himself, Snowe, and Vera.

A memory tugged at the back of Snowe's mind as Richard returned the spatula and serving plate back to the counter, but as Snowe stared at the pancakes, nothing concrete came to mind. "I don't think so." He stifled a laugh as he realized how many pancakes were loaded on his plate. "Am I really going to be able to eat all of this?"

"You have a long road ahead of you!" Vera said as she grinned. She took the syrup and drizzled a generous amount of syrup over his stack. "I promise, Richard made them. He wouldn't let me try my new recipe."

Another laugh exchanged and once Richard sat in front of his own stack of pancakes and after Vera gave him the same helping of syrup, they ate together. They were quiet as they ate; Vera talked about a few people helping her out at the small library, and River squirmed as she listened to her mother, and Richard mentioned a few letters he had received from others from Sabine and relayed how they were doing. Just like they had before. Like nothing at all had changed. Snowe missed it.

When Snowe had polished off all his pancakes and felt ready to roll back to bed to sleep off the food, he knew it was time to leave. He pulled up his bag and just as he slung it over his shoulder, he had to keep from stumbling over as Vera wrapped him up in a tight hug.

"Remember to write back to us," Vera said as she withdrew.

"I know," Snowe said as Vera turned to pick up River. As soon as Vera put River in arm's reach of Snowe, River reached out and tried to pull him into a hug of her own.

Snowe gave them both a smile and a small wave before he followed Richard to the door. As Snowe stepped out, he found Richard had followed him and he closed the door behind him.

"Seeing me off?" Snowe asked as he smiled.

Richard returned the smile. "I had something I wanted to tell you," he said. He took a deep breath and adjusted his glasses out of habit. "Snowe, I always thought of you as my son," he whispered. "I-I know it doesn't make up for what Lina and Edgar put you through, but I can't imagine my life without thinking you're my son."

Without giving Snowe a chance to reply, Richard pulled Snowe into a tight hug and held him still for a moment. "I know I'm pretty bad at this at times, but I just want to see you healthy and happy," he said. "I'm sorry I can't help you as much as I would like to help you."

"Don't say that," Snowe said as Richard withdrew. He grinned. "You do more than you have to really," he admitted. "Thank you though. Hearing that makes me feel better."

Richard chuckled. "Just make sure to stay safe," he reminded as he walked with Snowe toward the apothecary shop. As Snowe snickered, Richard patted him on the back. "Don't give me that."

"I have it firmly planted in my head to stay safe," Snowe said. "I won't run blindly into danger. I've learned my lesson."

"I hope so! I think I'm getting too old to worry about you doing that."

Snowe laughed again and nodded. "When I get to Barina, I'll send you word, all right?"

"That would put me at ease," Richard replied.

When they arrived at the apothecary shop, Richard gave Snowe another tight hug before he left to head toward the schoolhouse nearby. As Snowe turned toward the shop doors, he took a deep breath; this would be easier if he hadn't tried to leave on his own before. Part of him didn't want to face Astra or Hiante, but he knew he'd have to get it over with sooner or later. He opened the door and stepped inside.

Hiante sat at the counter, the cane Astra had found for him leaned against the side, as Erio pointed out everything he still had in the shop. Spread out on the counter was a small booklet Erio had made and Snowe knew inside it held exactly how Erio made everything in the shop (Erio had showed Snowe the same booklet when he finally relented and let Snowe help him out). Astra leaned against the counter as she watched the two, her bag already on her shoulder and her sword safely at her hip.

"You're not coming again?" Snowe asked as he stood beside Astra.

Hiante shrugged. "My legs are still stiff--I'd only slow you down," he said sadly. "I don't mind holding down the fort though."

Astra reached over the counter and gave Hiante a hug. "When your legs cooperate, we should go sightseeing," she said and Hiante gave her a laugh.

"I'm sure Relenia has that planned too," Hiante said. "She told me of all the spots she wanted to show me as soon as I could get up to Lumisia."

"Did you tell her about what's going on here?" Erio asked.

"I sent her word earlier today," Hiante replied. "I told her if the White Cloud Temple's a bust, her in Lumisia would be our next bet. I asked her to start asking around just in case."

Erio nodded and pulled Hiante's attention back to his booklet and as Hiante became engrossed, Astra took Snowe outside. When the doors closed, Astra turned to Snowe and took his hands in hers.

"I shouldn't have yelled at you last night," she said.

"I would have yelled at me too," Snowe said and he gave her a smile. "I think I needed that though."

"I just don't want you to go away." A smile stretched across Astra's lips as she took her hands away. "I like the family I've found here and you're part of that. I don't want any of you to leave anytime soon."

"I promise I won't."

They went quiet as they both smiled and after a few moments, Hiante and Erio came out from the apothecary shop.

"Remember to stay safe," Hiante said as he gave Astra a tight hug. "Keep everyone out of trouble, all right?"

Astra giggled and nodded. "Isn't that what I'm good at?" she teased.

Erio rolled his eyes as Snowe stifled a laugh and Hiante gave them both a stern look. Snowe had trouble stopping his laugh; he had a hard time getting used to Hiante having emotions he could see and he wondered if Hiante had always given him the same look if he had said anything out of line. Astra gave Hiante one last tight hug before she withdrew completely and smiled widely. They headed for the entrance to town just as Hiante stepped back into the apothecary shop and as they walked, Snowe looked at Astra and Erio.

"How are we getting back to Lura this time?" Snowe asked as Astra led the way toward Aldcoast's main gates.

"The same merchant! He kindly offered to take us again if we needed it," Astra said with a grin.

"He's going to get real tired of us by the end of all this," Erio pointed out.

"We can protect his cargo if he's attacked though," Astra said. "Besides, he wanted to head back to Lura to set up a stall for a while."

The merchant had waited for them patiently right outside the gates and greeted them as they came out. Like before, they helped him set up the candles to keep the phantoms at bay and soon they were back on the road and headed for Lura once again.

* * *

In due time, they had returned to Lura and the weather had been picture-perfect along the way. The merchant let them off near the gates before he went inside on his own and while Erio and Snowe made their way to the stagecoach station to wait, Astra trekked into Lura on her own to find Canan; just in case lords were still in the city, Erio and Snowe would be safest outside.

Just inside of Lura, Astra had to pause to get her bearings straight; she still didn't know the city well enough to remember where their inn had been but after asking a passing merchant, she found the right way to the inn and headed on her way. As she dodged merchants clogging the city square as they tried to sell their wares, she felt someone walking instep beside her.

"Hello."

Astra jumped and turned toward the voice too close for comfort while she kept a hand at her sword. Her stomach plummeted as she sighted the lord she had seen before they had left Lura the first time. He hadn't changed in the least, a different shirt and trousers maybe, but the same unearthly feeling came from him. She wondered how he had moved so close without her noticing or any shoppers making a fuss at how quickly he must have moved. No matter how frightened Astra felt that she had been snuck up on, a warmth spread around her within seconds and replaced the unearthly feeling as the lord gave her a kind smile.

"I've never seen you in Lura before," he conversed and held out his hand. "My name is Beliaz, what's yours?"

"A-Astra." She shook his hand and immediately cursed herself that she hadn't thought of a fake name.

"That's a nice name," Beliaz said after he took his hand away. "Do you like the stars?"

"I do."

"Too bad you can't normally see them in Lura," the lord said sadly. "Where are you from? Your accent is peculiar."

"Aldcoast."

Beliaz folded his arms. "I can see that a bit, but before that?"

Astra laughed weakly and shrugged. "D-Does it matter?" she asked. "I'm in Aldcoast now and that's what matters."

Beliaz returned her laugh and nodded. "True," he said. "I'd rather not explain why I'm here either." He moved closer as a merchant pushed past him. "What brings you to Lura?"

"Looking for Canan," Astra replied. "She was going to take me to Barina."

"Sightseeing?" Beliaz guessed and when Astra nodded, he chuckled. "The temple would like you--they love those good with magic and I can tell you are." Another merchant shoved her way between them and once she passed, Beliaz placed a hand at Astra's back and took her forward. "I wanted to see Canan as well, I believe she's at the inn still."

Astra wished he hadn't seen her and wished he had left her be; if he wanted to join her and Canan to Barina, she wouldn't know what to do. No matter how much Canan vouched for him, she would rather not have any lord close to either Erio or Snowe. Her unease must have shown, however, as Beliaz glanced at her sadly.

"Have you been Canan's friend for long?" he asked.

"I met her here actually," Astra admitted quickly as she started to fiddle with her amulet to calm her nerves. "She told me about Barina and I wanted to visit."

"You know, that's a nice amulet," he said. "How long have you had it?"

"Since I was little," Astra answered. "I was told it was from my parents."

"Have you tried opening it?"

"What?"

Beliaz took hold of the amulet and pointed to the very top where the chain attached through a small clasp holding onto the crystal. "It's melted up here, but normally they can be opened," he explained. "People used to use these amulets to send secret messages back and forth."

Astra stared at her amulet and held it up to the sun; she had never thought of that. Beliaz hid his laugh.

"If it was that easy to see through, secret messages wouldn't be secret," he teased. "If you can find a thin knife, I'm sure you can pry it open."

A tad embarrassed, Astra nodded and let the amulet rest against her chest again. They remained quiet the rest of the way to the inn and once inside, Beliaz led the way up the stairs toward Canan's room. As Astra followed him, she noticed he favored his right leg and looked as though walking up the steps put him in pain. Before she could work up the nerve to ask him if something hurt, they had arrived at Canan's room. Beliaz knocked softly on it and within seconds, Canan opened it with a smile; when she caught sight of Astra beside Beliaz however, she paled.

"I-Is something wrong?" she asked.

"I was seeing your friend here," Beliaz said as he patted Astra's shoulder. "You're taking her to Barina soon?"

"Gotta show her that temple!" Canan said.

Beliaz smiled again. "And I wanted you to tell Esmeralda that I should be visiting the temple pretty soon," he said.

Canan frowned. "Alone?" she asked.

A simple shrug of his shoulders and Canan gave out a sigh. "It won't be a long visit I'm sure and we'll be there the middle of next month," he explained. He gave Canan a kind smile again. "As much as I would love to see you both to Suzerre safely, I'd better refrain." He sighed and folded his arms. "If Zuan learned I made a new friend, I'm not sure what he'd do. I do wish you both safety as you go, however."

Before Beliaz had a chance to turn and leave, Canan gave him a hug. "We should make time to meet up again soon," she said. "I really miss seeing you lately."

"And I miss you too." Beliaz squeezed Canan before he withdrew and gave Astra a nod. "It was a pleasure meeting you, Astra. I hope we can talk again soon."

As he walked down the hall, Canan pulled Astra into her room and went to her bag that sat against the bed. She went around the room and collected her things quickly.

"Could he sense Erio on me?" Astra whispered.

"Even if he did, he wouldn't have done anything," Canan said. She finished packing her bag and once she clasped it closed, she slung it over her shoulder. "Zuan would though. I really think Beliaz would like Snowe and Erio, to be honest. He's always friendly."

"When he said he was going to visit the temple, do you think we'll be safe?" Astra asked.

"We should be gone by the time they visit," Canan pointed out. "It won't take Sephi or the temple long to figure out if they can help. By the time Beliaz and Zuan get to the temple, you'll be long gone."

As Astra gave out a relieved sigh, she followed Canan back into the hallway. Canan gave out a quiet sigh as she looked toward the stairwell; Beliaz had already left the hall.

"When... When you were walking with him, did you see if he limped at all?" she asked quietly.

"A little bit." Astra looked at Canan. "Why?"

Canan sighed and took Astra down the hall. "I think Zuan's been abusing him pretty badly lately," she whispered. "He's been Zuan's disciple since he was young and about two years ago he was free to be on his own, but this past year..." She sighed again and ran her fingers along her braid. "I don't know what happened--people say he did something wrong, but he couldn't have done something so wrong to warrant that much abuse."

Astra frowned. "Can you help him? Has anyone tried?"

"I wish I knew how to. Everyone's afraid to help him because of Zuan; he wouldn't hesitate to make anyone who likes Beliaz disappear so Beliaz is really alone."

They went quiet for a moment before Canan took a deep breath and tried to smile. "Are you ready to head to Suzerre?"

"I was born ready," Astra joked as she laced her arm within Canan's and pulled the priestess down the steps with her. She stifled a laugh. "Will it really take us two days?"

"If we were in Virin, we could take the train, but if we went to Virin from here, it'd take about the same time," Canan explained. When Astra pouted, Canan knocked into her playfully. "Don't worry! I have cards and if we get tired, the carriages are very comfortable. The inns we even stop at have really good things to eat too."

In higher spirits, they left the inn and headed for the southern exit. Astra kept a look out for the lord, but she didn't see him again in the crowd. Where he had gone lingered in Astra's mind, but she had no time to entertain the thought before Canan had to pull her through the thick crowd that still swarmed around the town square. Once they had reached the city gates, Astra took the lead herself and took Canan to the small stagecoach entrance right outside the southern exit. Erio and Snowe sat beside the road and Astra smiled at them; they were meditating again.

"Are you two ready?" Canan asked once they were close enough.

"For riding for two days?" Erio said as he stood and helped Snowe up with him. "I can't wait," he added dryly.

Canan laughed as she took her arm away from Astra. "I've taken this route a dozen times now, it doesn't feel as long as it really is," she pointed out.

"Does the temple know we're coming?" Snowe asked as he stretched.

"I sent my Head Priestess a letter early this morning," Canan said. "Though even if I hadn't, we would have been fine. We have drop-ins all the time. Just don't set the place on fire, all right?"

Snowe laughed weakly. "S-Since when do you know I set things on fire?"

The priestess paused as everyone looked at her. She ran her hand over her braid again and returned Snowe's weak laugh. "I-I'm sorry, I just had a thought you did," she said.

The worried look Snowe had given Canan made Astra uneasy, but if something had bothered him about that statement, he didn't bring it up. Canan pulled all of them into the stagecoach station to secure a ride before they missed their chance and very soon after Canan handed over the money, they were all on a carriage headed for Suzerre. Though Astra was sure the long ride would bother her, as soon as Canan settled back, she pulled a small deck of cards from her bag and started to describe a game she had learned from Beliaz a long time ago. Astra hoped it would at least make time go by faster.