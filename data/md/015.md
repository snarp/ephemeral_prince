# Chapter 14 - "You still have a piece of him inside you."

![](images/015_fleeting_14.png)


Solid ground met Snowe head-on and he groaned as he pulled himself up. Canan's legs lay across his back and Snowe realized she had landed on her brother's back. As Canan regained composure and ran a hand through her hair, she muttered an apology to her brother before she slipped off his back. Once she was off, Iain groaned in pain.

"Where are we?" Iain asked as he stood up. He stretched his back and as it popped, he gave a quick glare to his sister. She smiled at him sheepishly and he rolled his eyes and helped her and Snowe to their feet.

"Hopefully where we need to be?" Snowe replied as he looked around.

They stood atop a solid, but soft piece of land that resembled the cap of a mushroom. It glowed beneath them and as Snowe looked up, he saw a long dark tunnel alit with bright red amaryllises that lit the way up. Down the tunnel before them, Snowe could see glow stones embedded into the wall that worked to light the way forward.

"Rhea?" Canan looked around them and paled once she didn't sight her fellow priestess. "Why didn't she come in with us?"

"Did she grab Snowe?" Iain asked. He grabbed Snowe's arm again. "I think we both had a hold of him, right?"

Snowe nodded. "I think she'll be safe," he said and shook Iain's hand off him. He glanced down the tunnel and blanched. "W-Wait, how do we get back up?"

The siblings shared a worried glance.

"I'm sure we can ask," Canan said after a moment. She looked above them and sighed. "I hope Rhea doesn't panic..."

"I wonder if she's digging up the grave as we speak," Iain joked as he crossed his arms.

"Let's just get going," Snowe said. When the siblings nodded, he turned to the path and felt the warmth fill his body again. More and more, it felt like home and after the initial warmth, he felt a dread. This shouldn't be home. Nevertheless, he took a deep breath and steeled himself for what lay ahead and led the way.

The path twisted and turned and once they went further in, small amaryllises started to line the path, peeking out from the dirt and sticking out of the walls. Each petal glowed vibrantly and kept the way alight along with the glow stones in the wall. When Snowe started to worry about how long the path was, it opened up to a slope where before them, they could see a large area that resembled an underground city. The ceiling was too high to be seen, but stalagmites hung down from the ceiling and once low enough, glowed brightly to help keep the area lit. Homey houses lined the snaking path that went throughout the area, some houses stacked on top of each other where only ladders and small stairways allowed access. The doors were mere colorful curtains and the windows lit up brightly, keeping everything warm and inviting. Beyond the houses lay larger buildings with windows so colorful and even a set of immaculate thrones sat in front of the largest buildings in the back.

"This is amazing," Canan breathed as she stepped forward. A few balls of light raced in front of her before they smashed together and formed a larger ball of light.

"I didn't think there'd be a city," Iain said as he followed his sister. He paused as the larger ball of light flowed through his stomach and he shuddered. The orb of light gave pause, as though now sentient, and flew away at a fast pace. Iain snorted and crossed his arms. "What was that?"

Snowe started to follow Iain and Canan, but suddenly he felt the wind knocked out of him and his back was against the ground. Someone had pinned him down, his hands at Snowe's wrists.

"Wait!" Canan shouted. "We come in peace!"

"Canan?" the one on top of Snowe paused and looked at Canan.

"P-Parin?" Canan asked, wide eyed.

Parin stood and laughed weakly as he pulled Snowe to his feet. In better light, Snowe recognized his features; though his skin had paled considerably and the whites of his eyes were a deep black, his voice and expressions were the same.

"You're a demon?" Canan asked.

Parin nodded and rubbed the back of his neck. "Lera summoned me a long time ago to help her out with the transports," he admitted. "She figured it was time to part ways when the lords kept looking at me funny. Didn't expect to see you guys down here at all."

Snowe started to move forward, but Parin pushed him back into the tunnel. "I can't let you in," he said quickly.

"We just need help," Snowe said as Iain and Canan moved to his sides. "Why can't I come in?"

"You just can't," Parin said. He started to say more but paused when someone appeared at his side. With a yelp, he fell back and the new demon stood in front of him.

"I got this, Parin," the female demon said. "Don't worry."

"Soria, you didn't have to come out. I told you to take a break," Parin said.

The new demon glared at Parin and he took a step back after a sigh. When she turned back to Snowe, he felt his breath get caught in his throat. All of her features resembled the demon he had known on Sabine. She folded her arms and gave Snowe an expression he recalled Erio giving him many times before. She searched him quietly before her yellow eyes stopped at Snowe's scar. She reached forward and traced it with her finger.

"Erio should have cut you deeper," she said quietly. "Then maybe he'd be back home by now."

"You're his sister then?" Snowe asked. "Please--"

"I won't stand here and listen to you beg for forgiveness," she snapped as she folded her arms again. "What do you want? I can't let you in any further than you are now."

"Why not?" Iain asked. "We just need to talk to a few demons. Not gonna harm anything."

"You can," Soria said as she nodded toward Iain and Canan. She looked back at Snowe and gave him a stern glare. "Just not him. If he walks past where Parin stands, this whole place will come down and the lords in the Crown will kill us all."

"Why?" Canan asked.

Soria reached forward and tapped Snowe's chest. "You still have a piece of him inside you," she said. "Lords forced us into a pact that said if we allow him to return home, even a small bit of him, they can come here and kill us all."

Snowe felt his stomach plummet and he felt sick; not at the thought of all the demons dying (though he was sure it attributed to the sinking feeling), but the thought that _he_ had left a piece of himself behind. Before Snowe could work up a reply, Canan rubbed his back.

"Can you help us then?" she asked.

Soria glanced back at Parin and he nodded.

"Canan and her brother are all right," he said. "Canan and her one red haired friend actually made me sweets once, remember? I brought them back home when I visited."

A laugh and Soria sounded younger than she appeared. She smiled at Canan, friendly. "What can I do for you then?" she asked.

"Can demons bring back anyone from the dead?" Canan asked.

"I'm really not sure, if I had to take a guess I would say my mother could... she was nicknamed the Bringer of Life after all," Soria replied without much thought. "She's in a deep slumber though."

"Can you wake her?" Iain asked.

"Afraid not. Only one demon can do that now and if he comes down here, she and we all will die."

"There has to be something," Canan whispered. "Please?"

Soria considered the priestess a moment as though she had come to realize something. She reached forward and grabbed Canan's braid, pulling her close. The priestess blushed and yelped as Soria sniffed her braid. In a moment, the demon dropped it and her face scrunched up into a scowl.

"You _do_ smell like a lord," she said.

"She's from the White Cloud Temple--I already told you, the priestesses aren't bad," Parin interrupted as Canan moved away from Soria. He moved his hand to his side where a short sword was sheathed and he watched the female demon as though expecting her to attack.

"Why don't you ask your lords then?" Soria snapped, her friendly demeanor gone. "Why, they think they can manipulate the world to their whim. You fight for them even. Go ask them." She took a deep breath and shot Snowe a glare. "Why should I help you regardless? You killed my brother."

"I'm trying to bring him back as well," Snowe stammered. "I didn't mean to kill him."

"When demons die, our energy disperses and our bodies disappear," Soria said. "There's no bringing us back from the dead." She turned and started to leave, only pausing at Parin's side. "Parin, make them leave. I don't want to see any of them back here ever again."

With that, she walked briskly down the path and Snowe trembled in his spot. Iain reached around Snowe and patted his back.

"You made a lot of enemies, didn't you?" he said lightheartedly.

Parin drew closer and frowned. "I'm sorry," he said. "She's right though; we can't wake her mother without that demon walking among us. And it's not even a guarantee she can bring anyone back from the dead. Best course of action are the lords if you can figure it out."

"Thank you Parin," Canan said.

"It was nice to see you again though," the demon said after he paused. "I hope whenever the lords finish whatever it is they're doing, I can come back. I miss Lera." He laughed and rubbed the back of his neck. "The desert was much better for my looks, right?"

Once Canan and Iain gave him a smile, Parin ushered them back through the tunnel. They returned to the mushroom cap and Parin had them stand next to each other, their hands clasped together.

"Just keep a tight hold on each other and I can send you back out," he said. He gave them a sad wave and Snowe felt the mushroom cap shudder underneath them.

Darkness wafted up in a thick cloud around them and all his senses were soon lost to the dark. His senses came back all at once and he found himself flat against the soft dirt; the cold rushed in at once and he heard Rhea's shriek. Rhea pulled him to his knees and quickly pulled him into a hug.

"I can stop digging!" Rhea said as she gave Snowe a squeeze. After a swift kiss to his cheeks, Rhea left Snowe dazed and moved onto Canan, doing the same to her.

"I did the digging," Hiante said from a few feet away, just a tad annoyed. The fire had disappeared from his hand and in its place was a small spark that feebly lit the alcove as it danced in his palm. "What did you learn?"

Snowe frowned and everyone went quiet. The look alone said everything but Snowe looked at Canan and Rhea, already a new plan forming. "Can Beliaz help me?"

Canan froze and her hand grasped the crystal pendant around her neck. She watched Snowe with wide eyes as though it had already occurred to her and she hoped he wouldn't say anything. After a moment, she nodded slowly and Rhea and Iain frowned from behind her. "If I had to guess, he would be the same level as that demon's mother. But he's in the Crown, Snowe, and that's in the land of lords."

"How do we get there?"

Another question that Canan looked worried to answer. She held up the crystal pendant. "B-Beliaz told me once where it was. I can open up the entrance with this; we don't even need a lord with us," she said. "The entrance is near the capital, though. At-At least the one he had told me about."

"We'll go there next then," Hiante decided as he stood. He pulled Snowe up from the ground and started to drag him along. Rhea hopped up quickly and grabbed Snowe's other arm.

"We will rest in Lura for the night," she said through gritted teeth. "We're tired."

"I do not need rest," Hiante said. He pulled Snowe out of Rhea's grip and brought him closer.

Rhea took a deep breath and moved around Snowe and grabbed Hiante by his cloak and pushed him back. His grip slipped from Snowe and Rhea stood between them. "I will not let you march Snowe to his death!" she snapped. "We will rest in Lura or so help me, I will tie you to a tree and we will leave you there."

Hiante considered Rhea a moment and once Canan and Iain had stood beside her, blocking him from reaching out to grab Snowe again, he sighed. "Whenever you are prepared then."