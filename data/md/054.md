# Chapter 24 - "Stop lying to me."

![](images/054_permanence_24.png)


The front desk looked as though a small twister had hit it by the time Erio got back down the steps. The innkeeper sat with her eyes closed and she had her head tipped back. Erio stood at the desk and she didn't immediately notice him so he cleared his throat. The innkeeper snapped back to attention and sat straight in her chair.

"Ah! You came back so soon!" the innkeeper said and laughed. "Sorry about that. Did you need something?"

"Can I have a map?" Erio asked.

"You should know the area!" the innkeeper said as she laughed.

"Y-yeah... I just never left town before," Erio lied. "N-Never had a reason to before."

With a smile, the innkeeper took a small map out from beneath her desk and she held it out for Erio. "You can keep it; I have lots more where that came from. Our inn is marked with a red dot, just so you know."

Erio thanked her quickly and stepped away; she must have seen through his and Snowe's story they had told the driver of the wagon. Though if it only garnered him a teasing smile, Erio was happy to take it. As he moved into a deserted hallway, Erio opened the map and glanced over it. It had an overview of Lumisia Kingdom and sure enough, the red dot for the inn was on the west end, far enough away from the border. Though as he glanced over the surrounding area on the map, Erio knew he should have guessed they were in Lumisia; it must have been on Snowe's mind.

After a quick glance down the hall to make sure no one was watching him, Erio closed his eyes and thought of Astra. Image clear in his mind, Erio pulled himself into a teleport.

Erio felt a wave of dizziness come over him--he was using far too much of his magic--and in a flash, he appeared on top of Astra and Hiante. Both gave out a groan at his sudden weight and with a quick apology, Erio slipped off. Despite the groan, they both had a look of relief on their faces as they watched Erio.

"I'm so glad you both are safe," Astra said quietly.

"Don't worry dad!" Canan spoke up from the other side of the cart. Her feet hung over the edge as she sat beside her father who had placed a hand on his sword, ready to attack. She patted his hand. "That's Erio--Astra's friend."

"Sorry about that," Erio said as he settled in front of Astra and Hiante. They were in the midst of boxes of produce and the like while they sat in the back of a cart. "Where are you headed?"

"Just back to Alsan and then we'll head back to Aldcoast," Astra explained. "We caught a ride out of Morzin. Where did you two end up?"

"Lumisia," Erio replied. He showed Astra and Hiante the map and pointed out the red dot on the map. "We're about here," he explained. "A small inn we found."

Hiante took the map in his hands and looked it over. He pointed to a large city north of the inn. "I think Relenia's up here somewhere," he said. "I can send her word when we get to Alsan and I can tell her you two are around."

"I think that'll be helpful," Erio said. "I'm just glad I didn't teleport us into the ocean or something."

"Did you teleport you two like you used to do with me?" Astra asked.

Erio nodded and he took back the map. As he rolled it up he sighed. "That was the only way I could think of to get out of there quickly. I'm glad we found the inn."

"Are you planning to stay there a while?" Canan asked as she turned in her spot.

"Better than Norin," Erio replied. "Especially after what Xiri did."

"The Crown came down to Morzin as we left," Ciran explained as he kept his eyes on the road. "Chances are they'll scour Norin for that kid. You should be safe in Lumisia for the time being; just stay away from the borders and even the capital."

"That's the plan."

"You two should see if my friend is near you," Canan suggested. "Her name is Rhea; she's from the temple like I am and I bet she can help you if you need to rest up."

"Does she have red hair?" Erio asked and once Canan nodded, he smirked. "Met her already then. She helped out Snowe's hands a bit."

Canan grinned. "Then you'll be in good hands!" she said.

Erio nodded and he looked back to Astra and Hiante. "So what is the plan from here on out?"

"Well, like Astra said, Aldcoast," Hiante replied. "We have to tell Richard and Vera what happened and we can pick up some supplies when we're back there." He crossed his arms and sat back against a create of produce. "Then we can meet up with you and then meet up with Relenia. She might have an idea."

"Sounds good," Erio said.

Astra leaned forward. "How's Snowe doing?"

"I'm not sure," Erio replied. "He's not really talking about it." He turned his attention to Canan and Ciran again. "How did Morzin go when I left?"

Ciran sighed and ran a hand through his hair. "We got the fires out," he said. "But we left before they could count the bodies." He glanced back. "If it is as you all say and it's not Snowe's fault, just keep his head down."

"Wouldn't have it any other way," Erio replied. He looked at Canan. "Were you coming to Lumisia too?"

Canan quickly shook her head. "I should stay in Norin for now," she said. She reached behind her neck and unhooked her necklace. A simple clear gem attached to a chain and she held it out to Erio. As he held it closer, he felt it warm his hands. "Beliaz gave me this for luck when I was younger," she said. "I think Snowe needs it more than I do."

"Thank you for all your help," Erio said and he slipped the necklace into his pocket. "I know Snowe thinks that too."

"Just make sure you all stay safe," Canan said. "I know you guys can figure this out. I'll keep my eyes open and I'll check Alsan again when we get back for anything you might have missed."

As Erio turned to look at Astra again, she caught him off guard as she reached forward and gave him a tight hug.

"I'm glad you're safe," she said as she withdrew. "Just get some rest while you're in Lumisia and we'll meet up again soon, all right?"

"Rest is all I want to do right now," Erio said and chuckled. "You all stay safe too while I'm gone too."

"When are we not?" Hiante asked. He turned in his spot and pulled up two bags he had behind him. "Take these back with you. I'm sure you both would be happy to have your things back."

"I'm surprised they didn't burn," Erio said as he took them both. "Where did you find Snowe's bag?"

"I found it actually," Ciran said as he glanced back. "I went to check out the cell Zuan had him in and that was nearby."

"Thanks," Erio said as he pulled both straps over his shoulders. He looked back at Astra and Hiante. "I guess I'll see you two again soon?"

"That's the plan," Astra replied.

They shared a smile between themselves and after a small wave, Erio closed his eyes and concentrated on Snowe to get back. The cart disappeared as Erio moved back through a teleport and as he moved through it, the dizziness came back and he knew he was running himself ragged again.

When he opened his eyes, he heard Snowe yelp from beside him as he jumped away and Erio groaned as he realized he stood in the bathroom.

"Don't do that!" Snowe said quickly as he pulled his belt tight to keep his pants up. His face beet red, he made a reach for his shirt he had left nearby.

"Sorry about that!" Erio replied.

"You really should send out a warning or something," Snowe added with a short laugh.

"I'll work on that."

Erio quickly exited the bathroom to give Snowe more privacy and once he entered their room, he stood still to will away the dizziness that had surfaced. At least I don't have to teleport again, Erio told himself as he went to the nightstand next to the bed and emptied his pockets. The map went first, the necklace from Canan, the two daggers he had left on his person, and he placed his and Snowe's bags down beside the nightstand. He knew there had to be food somewhere in the bags, but he decided he'd dig through them later. Erio sat on the bed once he finished and he took a deep breath.

When Snowe came from the bathroom, his cheeks were still red and Erio stifled down a laugh.

"I really thought you'd take a longer bath," Erio remarked.

"You teleported back thinking I'd still be in the tub?" Snowe asked and he laughed.

Erio felt his face heat up and he looked away from Snowe. "Nevermind," he said as he fell back against the bed.

"I just wanted to sleep," Snowe admitted as he came around the bed and sat beside Erio. "I figured if I fell asleep in the tub, I'd end up drowning." He held out his hands without looking at them. "Could y-you bandage these back up?"

Erio pulled himself from the bed and went to grab the bandages from where Rhea had left them before he sat beside Snowe again and carefully took his hands. They still looked worse for wear and as Erio moved them around to bandage them up, Snowe winced with each movement.

"W-What did Astra and Hiante say?" Snowe asked as Erio finished his left hand.

"They'll meet us up here. I even got our bags from them too so we aren't without our things," Erio replied as he slowly stared to move the bandage around Snowe's right hand. "Relenia should be a bit north of us, so we can meet up with her if we feel up to moving."

"D-Did they say what happened in Morzin?"

Erio wished he hadn't asked that and as he glanced at Snowe, he realized Snowe wouldn't meet his gaze. Snowe kept his eyes firmly on his hands and Erio sighed as he finished wrapping Snowe's right hand. "Let's just rest for right now," he said.

A quick nod and Snowe took away his hands away. "I refilled the tub for you if you wanted a bath too," he said.

"Was there even any hot water left?" Erio asked.

"N-Not really!" Snowe laughed weakly. "B-But I heated it up myself for you. Should be cooled off enough now so you can actually touch it."

Erio stood up and stretched. "Then you just rest out here and I'll go and wash up."

Snowe stayed quiet as he nodded and he kept his eyes on his freshly bandaged hands as Erio collected his clean clothes. Even when Erio closed the door to the bathroom, Snowe hadn't looked away from his hands. Though freshly clean with fresh bandages, his hands still stung when he made even the smallest of movements. Much better than before, but it stayed to remind him of what he'd done. Snowe took his eyes away from his hands and fell back against the bed. The bruises against his chest hurt, his head throbbed, and he just wanted it all to go away.

Nothing had gone right. Not like it could have, Snowe told himself as he watched the ceiling. More thoughts crept up on him and he closed his eyes tight to concentrate on breathing. With each breath however, the bruises only hurt worse and the thoughts only grew louder and more desperate. Snowe rolled over on his side and curled up. He wanted the lingering thoughts to leave him alone. If only Xiri would taunt him, mock him, or even laugh at him, Snowe could focus on that. But since Morzin, Xiri had grown silent.

It wasn't worth it.

The thoughts didn't go away. How many had he killed? Snowe sat up as his headache grew worse. He gripped his hair and breathed in deep. How many would he continue to kill? Snowe looked around the room when he couldn't calm down and his eyes stopped at Erio's daggers on the nightstand. How many have died from the phantoms that leaked into the world because of what Snowe had done to gain his second chance? Snowe stood up and he took the dagger in hand and stared at it. How many had Snowe killed? Those people who would never breathe again, never smile again? All those people who had healthy lives ahead of them, not at all like what Snowe had ahead of him? Those lives all cut short because they had been in the wrong place when all Snowe had were hands that stung, bruises that ached.

It wasn't fair. He was too dangerous.

Snowe pulled the sheath off the dagger and stared at the blade. If Erio couldn't find him to stop the bleeding, he knew Xiri couldn't hold on forever.

He left the sheath on the bed and he started for the door. The bathroom door opened before he reached out to take hold of the handle, however, and Snowe froze.

"What are you doing?" Erio asked as he came out. He had changed and had come out with his old clothes in his arms, and he had his boots in hand. He stopped short and stared at Snowe.

Snowe had no answer prepared and he hid the dagger behind his back before he turned to Erio and shook his head. "J-Just for a w-walk."

"I can come with you." Erio took a step forward; his tone guarded, he had his eyes on Snowe's arms. He must have seen the dagger.

"It's better if you didn't," Snowe said. "You're tired. I-I'll be fine on my own."

"No, I'll come with you." Erio dropped his clothes at his feet and put down his boots. "I really don't mind."

"Stop lying to me."

Erio straightened up and left his boots where he had placed them as he kept his eyes on Snowe.

"You don't care," Snowe said as he brought the dagger out from behind him and kept it near his chest. His hands trembled as he looked down at it. Anything to look at but Erio. "I don't deserve this. I killed all those people in Morzin and all I have are hands that sting." He raised up the dagger and he noticed Erio tensed up. Snowe shut his eyes tight; he knew it. He deserved pain.

He didn't plunge the dagger into his stomach; Erio had moved too fast for that and Snowe fell backwards as Erio gripped his wrist. Snowe's feet fumbled behind him and he fell down but Erio didn't let go; he had his other hand at Snowe's other wrist and he kept him pinned to the floor. Dagger out of reach, Snowe squeezed his eyes shut and tried to get back up, but Erio's grip tightened.

"Don't you dare," Erio said as his voice shook. "We already had this conversation--we--"

"Stop it!" Snowe interrupted as he struggled underneath Erio's weight. "Astra is not here, you do not have to pretend you care. You didn't even kill me like you said you would!"

"And I told you--"

"Stop it!" Snowe cried. "I am not dense. All I have ever done is cause everyone problems--you all only help me because you feel sorry for me!" He tried to shove Erio off himself, but his arms fell back against the floor. "When I'm better, when all of this is done, you all will leave me and I know that!"

"Snowe, we would not do that," Erio replied.

"Stop lying, stop pretending," Snowe whispered. "Everyone did that to me before; they all were made to like me and they left me and looked at me like I was the worst thing they had ever seen. Now all you feel so sorry for me you only pretend to like me." He felt a few tears fall down his cheeks; he hadn't even realized his eyes had welled up with tears. He tried to move his hand to wipe them, but Erio didn't let his wrist go. "Get off of me!"

"Why do you think we'd do that to you?" Erio asked. "We are not on Sabine; we do not have spells on us."

"I'm not needed," Snowe snapped. "What am I really? Just someone with a demon stuck inside of him that may kill every last one of you. I'm useless. The real me? It wouldn't please anyone and once you all figure that out, you'll be gone." He squeezed his eyes shut again as more tears threatened to escape. "I-I mean, R-Richard and Vera have R-River. All I am is something broken. All I have ever done is caused the two of them pain." He opened his eyes again and wished he hadn't; while Erio's grip had loosened, he looked sad. "Stop looking at me like that!" Snowe ordered. "You don't like me. Stop acting like you do."

Though Erio didn't reply, he removed his hands. As Snowe sat up to wipe his cheeks, he avoided Erio's gaze; he wouldn't stop staring at him. He doubted he could reach the dagger without Erio pinning him down again, so Snowe remained still instead as his shoulders trembled. He wished Erio would stop staring at him.

"You're not useless," Erio said. "You've helped me in my shop."

"Only because Astra asked you to let me," Snowe said quietly.

"No, Snowe--"

"She is not here; I said stop it." Snowe looked away. "You only help keep me warm, you only put up with all this crap I put you through because I'm pathetic and you feel sorry for me." He ran his hands through his hair and winced as his fingers stung from the movement. "I don't deserve it. I killed so many people. I don't deserve to be happy here while they rot."

"You didn't kill them," Erio stressed. "Xiri did."

"I did," Snowe snapped as he gripped his hair. "All those flames? That was me trying to set myself on fire to stop that damn demon." He relaxed his fingers and took a deep breath. "I let him have control because I wanted so badly to live. I killed so many people."

"Don't say that," Erio said. He reached out his hand but Snowe quickly moved out of reach.

"I said stop it!" Snowe ordered. "Stop acting like you care so bad. I can't even be fine on my own and I just rely on you and you just keep lying to me," he whispered to himself as he curled up in his spot. "It doesn't even matter; all of this will mean absolutely nothing in the end. You're just going to leave me like everyone else."

Erio sat back and kept his hands on his legs as he watched Snowe. The room filled with silence and Snowe closed his eyes again. He couldn't stand it. When he heard Erio move, Snowe flinched again.

"I wouldn't leave you all alone," Erio whispered. "I didn't like you on Sabine and I told you why. I do like you now."

"Stop lying."

"I am not lying," Erio stressed as he glared at Snowe. "I don't do this because I feel sorry for you. I do it because I do actually care for you. Not because Astra told me to. Because I chose to."

Snowe shook his head. "And when I'm all better poof, you'll be gone." He picked at the bandages covering his hands before he stopped and laughed. "The real me isn't so interesting. If Xiri wasn't even in me, no one would have cared. You all would have left by now."

Erio moved closer but stopped as Snowe curled up tighter. "I just told you, I won't," Erio said.

"Save it." Snowe shook his head. "I-If we magically get Xiri out of me, I'll just fall apart anyway." He rested his forehead against his arms and drew his knees closer to his chest. "I'm dead, you know. I died when that bird phantom attacked me on Sabine. The only thing keeping me here is Xiri. That's why I'm so cold--I have nothing left. The only reason I feel warm near you is because Xiri's actively keeping me alive. I'll just go away in the end."

The room fell silent again and Snowe trembled in his spot as he tried to feel warm on his own. It didn't work; it only reminded him of what he didn't have. "I will never be able to repay anyone back," Snowe whispered as more tears fell from his eyes. "You help me so much even though I don't deserve it. But I wanted your help no matter what. Just so I could feel warm. I'm so damn needy and selfish and I didn't even care how you felt." Snowe paused again and took a deep breath. "I-I know what I did to you in Alsan and it-it wasn't fair but you put up with me anyway. I just wanted someone to hold me so I felt warm again. I don't know if that's just it or what, but it's not fair I did that to you just so I could feel warm and happy."

"Snowe--"

"I don't deserve this," Snowe interrupted as he shook his head. "For my whole life I lived in a haze. All my life I was merely a host for Xiri, and look at me now. Broken, pathetic, clinging to things that aren't even there. I don't deserve any kindness anyone gives me. I don't deserve to be helped." His shoulders shook as he tried to keep down sobs and his head throbbed with pain. "Richard and Vera deserve to be happy; if I'm still around, they won't be. All I am is a reminder of what they had to go through on Sabine." He went quiet for a moment and ran a hand under his eyes. "I don't want to think. I don't want to go through this anymore; I just want it all to stop."

Snowe tensed as he suddenly felt warm arms wrap around him. Erio pulled him closer and Snowe couldn't stop the trembling anymore and he shook from sobs that escaped his throat. Thoughts he had tried to keep down for so long all bubbled to the surface and he cried.

"I won't suddenly disappear on you when we get this all sorted," Erio said quietly. "None of us will. I help you because since we've been in Aldcoast, I've grown to see you as a friend. I don't do it because anyone told me to. I don't do it expecting anything back." He paused as he rubbed Snowe's back. "I won't let you fall apart when we get Xiri out of you, I promise."

"But it's not fair for any of you," Snowe said. "I-I j-just take advantage of all of y-you. And a-all you a-and everyone else gets in return? Me trying to kill you."

"You aren't taking advantage of us." Erio pulled back to catch Snowe's gaze. "We want you to get better and when you do, it'll be reward enough." When Snowe nodded and a few more tears went down, Erio gave him a small smile. "We won't let you just die. We like you--the real you--too much for that."

The tears kept going down his cheeks and Erio pulled him into another tight hug, holding him still. When Snowe finally stopped his shoulders from shaking and the tears from falling, he pulled himself up and wiped his cheeks. "I'm sorry," he whispered.

"I know I'm difficult to be around--you can ask my sister--but if I really did dislike you, I wouldn't be here right now. I wouldn't share my space with you like I've done," Erio said as he put his hands down. "I just want to see you healthy like you should be. So we can go back to Aldcoast and live that nice life we fought hard for. That's why you wanted to leave Sabine and that's why you made sure everyone got out alive."

Snowe nodded and wiped his cheeks again. "T-Thank you," he said.

"Now, I think you need a nice nap," Erio said. "I do too, so let's just sleep for today." He stood up and held out his hands for Snowe.

"Y-You don't h-have to," Snowe said.

"And what did we just talk about?" Erio joked as he took Snowe's wrists and helped him up. "I'm exhausted, you're exhausted--and cold--nothing's waiting for us anyway."

"B-but--I can't m-make you do that, you don't see me--"

"Snowe," Erio interrupted as he met Snowe's gaze. He didn't his wrists go. "Look, whatever you feel for me right now is all dependent on what you've been going through," he said. "You feel warm near me and I'm not going to keep that from you. We can talk about it when this is all over and you're back on your feet again, I promise."

"I'm sorry."

"You don't have to say that. Now what about that nap?"

Though Snowe didn't answer, Erio turned to the bed and he pulled back the covers. He looked back at Snowe and gave him a smirk. "Don't make me say something awkward," he said as he slipped underneath the covers. "You're capable of that all your own."

Snowe laughed a little harder than he had intended to and nodded as he moved around the bed. As he crawled into bed beside Erio, he found the thoughts in the back of his mind had quieted. The bed felt softer than Snowe had thought it would be and as he settled into the pillow, he felt Erio worm his arm underneath his head. Once Erio had his hand against the back of Snowe's neck, the warmth spread once more and Snowe closed his eyes.

"Sweet dreams, Snowe," Erio said as he pulled up the blankets around them both. "I'll be right here when you wake up, I promise."

The sentiment kept Snowe relaxed and he felt himself fall into the warmth that surrounded him. The thoughts before were silent as his head ceased throbbing and Snowe drifted off to sleep. Part of him wanted to dream, wanted to speak with Xiri. The thought pulled him through the warmth and soon Snowe settled against new blankets, new pillows, each with a familiar scent. Just as warm as Erio, Snowe knew where he was. He opened his eyes and found Xiri kneeling in front of him. The demon looked about as ragged and tired as Snowe felt and his eyes were wide as he watched Snowe.

"Y-You really would have," he breathed.

"I'm tired," Snowe whispered as he remained where he lay. The blankets were so warm he felt the exhaustion attempt to take him over again, but he fought against it and remained awake. "You didn't have to try to kill Astra. I--"

"You want me dead," Xiri replied, his voice quiet. "How long do I have left before you're tired of me and just get rid of me? I don't want to die, Snowe."

The warmth faded from the blankets and Snowe pushed himself up; even in his dream he could feel each muscle ache and each bruise pain him, but he sat up and he put his hands against Xiri's face and made the demon look back at him. "Just trust me," Snowe said. "I don't want to hurt you, please stop making me try to kill Astra."

"And you think I haven't trusted you before?" Xiri snapped and as he stood up, he gripped Snowe's shirt and pulled him up as well. "Look where it got me! Clinging to old memories you hardly remember, that no one at all remembers but me. All I am is a thought in your head. I want to go home, Snowe! You damned me here and I just want to go home."

"I'm sorry!" Snowe shouted. "I know I ate you. I know I used you to get back to the island and I trapped you here again, but I had no other options before." He flinched as his right eye stung against the thoughts. "I-I know I thought the same then as I do now; I wanted to die because of what I'd done."

Xiri shook his head. "I'm sorry," he whispered. "I just want to get out, Snowe--I just--"

"I know you do and I know I promised you I'd get you free. Please, just trust me." He tensed and shut his eyes tight as Xiri pulled him closer, but when he felt Xiri's forehead rest against his own, he opened his eyes and looked at Xiri again. "I don't want to hurt you. But we have to work together on this."

For a moment, Xiri remained quiet. Snowe felt the demon tremble in front of him and as Xiri wrapped his arms around Snowe, he held him tight.

The dread and fear faded from Snowe's mind and the warmth returned to him as he found the dream disappearing around him. Xiri disappeared with it and soon the inn room came back into view, just the same as ever. The warmth still kept Snowe wrapped up and Snowe sighed in relief when he found Erio still beside him. The demon's breathing even, his eyes closed, he must have been as exhausted as Snowe had been and fallen right asleep. He didn't look annoyed, he didn't look uncomfortable even though Snowe was against him and Snowe smiled. He moved further into the blankets and let out a deep breath. If the warmth never left him, he felt as though he could sleep forever.

Sleep crept up on Snowe once again and as he found himself drifting off once more, he found new thoughts coming forth. He knew he could fix what he had done and though he knew he would never forgive himself for what he had allowed in Morzin, he knew he could focus on the task at hand. He had to free Xiri. Everything else would come to him in time and he knew that. Determination was the one thing he knew he was good at and he wanted to hold onto that.

« | ★ | »