# Chapter 18 - "Liquor burns the throat as it loosens the tongue."

![](images/047_permanence_18.png)


By the time Snowe had redressed in his own clothes and had returned Iain's, it had turned early evening; since Canan had wanted to spend time with her father since she hadn't seen him in a few months, Iain volunteered to take Snowe back to where Snowe had planned to meet up with Astra and the others. Thankfully, Iain knew the city like the back of his hand. With all that had happened, Snowe had already forgotten the way back.

Despite what had transpired and how uneasy it made Snowe, Soan had extended an invitation for him to take up lodging in the castle for the night if he had nowhere else to stay. It would allow him the earliest access to the library. Not to mention he doubted any lord could touch him within the castle if Soan had a weapon like that with him. While Snowe knew he felt fine with staying at the castle, he was unsure of everyone else.

As Iain led Snowe down the darkening main street, Snowe was amazed to find the streets as busy as ever. The lights flanking the street kept everything lit as though it was day.

"The city never sleeps, I swear," Iain conversed as he looked back at Snowe. "I hated when I worked the night shift; I always had so much to do because everyone's always still busy this late."

Snowe chuckled. "I really can't see you as a guard," he said. "You look too fidgety."

"I wasn't always like this!" Iain smacked Snowe on the shoulder as he laughed. "This city has just felt off lately; like I shouldn't even be here," he admitted. "I mean, I feel like something like this happened before and my dad sent me packing for safety." He rubbed the back of his neck as he slowed his pace. "Though, at least Canan came back to Alsan. I don't feel as nervous if we're all together as a family again."

"I'm sorry I took you away from catching up with her and your father," Snowe said.

Iain snorted. "Don't say that," he said. "I see my dad every day. I'll be all caught up with Canan by tomorrow morning." As he pulled Snowe around the corner of a pub they had passed, he looked at Snowe. "Why do you want in the library anyway? Canan wouldn't tell me. What are you hoping to find?"

"I don't really know." Snowe shrugged. "Something's wrong and Canan was confident we could find an answer in those libraries."

Iain frowned and came to a stop. "Something's bothering you still?" he asked.

Snowe paused and he felt a weird sensation run through him. He looked at Iain, wide-eyed. Did he remember?

With a quick clear of his throat, Iain shifted in place. He started walking forward again, pulling Snowe along. "I-I meant that I just feel like something's always been wrong," he said quickly. "I don't know. There's this dread I feel when I look at you, like something really went wrong before. A really unshakable feeling."

"I'm sorry," Snowe whispered. He laughed at himself. "I-I tend to m-make people feel that lately."

"Don't say that--it's not you," Iain said. "I'm sure this time you'll figure it all out."

The topic made Snowe uneasy and he took a deep breath. "D-Do you like what's happening here lately?" he asked.

"Getting away from the lords would be great," Iain replied. "My dad's pretty confident we can do it. He usually has a good gut feeling."

They went quiet for the rest of their walk and soon Iain came to a stop. The place they had agreed upon earlier hadn't changed much from morning; still the same amount of people. As Snowe looked around, he found Astra and Hiante nearby and Snowe smiled as he waved to them. Both looked relieved and they made their way closer.

"You didn't bring Canan?" Astra asked as she looked at Iain. She gave him a wave. "A new friend?"

"This is Canan's brother, Iain," Snowe introduced. "He was seeing me here since Canan wanted to catch up with her father."

"Nice to meet you." Iain held out his hand.

Astra smiled as she shook it. "My name is Astra," she said. "And this is Hiante."

As Iain took his hand away, he paused. As though the name was familiar. He looked at Hiante with his eyebrows raised before he crossed his arms. "Never thought that name would ever feel familiar," he said. He patted Snowe on the back before he took a step away. "I'll give you three some privacy. Just let me know when you want to head back, Snowe."

Iain moved away and as he turned, he found a guard on patrol and once he became engrossed with his own conversation, Snowe looked at Astra and Hiante. He had hoped Erio would have returned as well, but he didn't catch sight of the demon and Snowe frowned.

"Erio's not back yet?"

Astra shook her head. "Not yet," she said. She leaned forward. "Snowe, you look pale. Did something happen?"

Snowe hadn't realized his discomfort had shown on his face and after he sighed to himself, he explained to Astra and Hiante what had happened in the castle. When he finished, though Astra and Hiante looked uncomfortable at the prospect, neither looked shocked.

"The head of the guard made mention of something like that," Hiante said.

"When did you talk to Ciran?" Snowe asked.

"You know him?" Hiante folded his arms.

"He's Canan's father. I met him while I was in the castle," Snowe said quickly.

Hiante watched Snowe with a hard look but if the former-skeleton had seen through Snowe's hastily spoken excuse, he didn't mention it.

"Astra and I were checking these ruins deep in the woods nearby," Hiante explained. "When there, a group of phantoms attacked us and Ciran and his guards helped us out. He mentioned Alsan had a plan to be rid of all the lords."

"You went to the woods?" Snowe asked. "W-Weren't you going to one of the libraries in town?"

"Just a detour," Astra said. "We did find something though."

Hiante pulled out from his bag a crystal amulet and he held it out to Snowe. A dark blue crystal hung from a melted clasp at the top and Snowe realized it looked just like the one Astra wore around her neck. As Snowe touched it, an image of burning phantoms in a desert flashed in his mind, making him feel sick, and he recoiled. I'm sorry, a voice in his head said sadly. I should have warned you about that.

"Did you see something as well then?" Astra asked.

Snowe looked at her, his eyes wide. "You-You saw something?"

Astra put her hand on her stomach. "I saw that you killed me," she whispered. "Then you were doing a spell with a lord. And then in the desert with phantoms." She paused as if waiting for a reply, but when Snowe remained silent, she stepped closer. "You know what this is, don't you? What happened?"

Snowe couldn't match her gaze no matter how hard she tried to hold it. He felt sick.

"I know you've been hiding something, Snowe," Astra said quietly. "It's plain to see. I can handle whatever it is, Snowe, just please be upfront with me. This is getting us nowhere. I told you I will help you but I need to know what is going on."

No words came forth and Snowe found himself shaking as he tried to think of anything to say. He flinched when he felt Astra's hands around his and she gave them a squeeze. He still couldn't look her in the eye. When Hiante put the amulet away and crossed his arms, Snowe took a deep breath.

"I killed you," he whispered. "I killed everyone from Sabine."

"Yet we're still alive now," Astra pointed out.

"I don't know how, but I changed everything." Snowe looked at Astra. "T-The lord in Barina told me I released Xiri before but I-I devoured him so I could go back to myself on Sabine and so-so I could save everyone in a second chance," he explained. "When I did that--I broke s-something and it-it's why the lords are in the position they're in right now. It's why they want me dead." He went quiet as he felt a few tears in his eyes. He dropped his gaze again and looked down. "I'm sorry. I didn't mean for this to happen. I-I just keep wanting to believe it didn't happen."

Astra's grip loosened on Snowe's hands for a moment before she squeezed again. "But it did," she whispered.

"I'm sorry," Snowe said, feeling the dread well up again. "I'm sorry--I-I'd understand if you'd h-hate me--I'm..." He trailed off, unable to find any more words. He tried to pull his hands away from Astra's, but she held tight.

"I don't hate you, Snowe." Astra's hands squeezed Snowe's once more. "I'm alive now and I know you won't let that happen again. I believe in you Snowe. Don't be afraid." She shifted in her spot and matched Snowe's gaze. She looked determined, just as she always did. "Remember what I told you? We're friends and I will help you. That will never change." A smile spread across her face. "You forget, but I can protect myself. My big sword isn't for show, you know."

She chuckled and Snowe couldn't help but laugh with her. He straightened up and Astra brought her hands up and patted Snowe's shoulders.

"I can protect myself better if there's nothing kept hidden Snowe, remember that. And I can help you better this way," she said. "I know you're used to shouldering everything alone and you think that's the best, but I know it's not. We know it's not. You don't have to be alone. Please trust me on that."

Snowe nodded quickly, unsure of what to say. Some of the dread washed away and he felt a great deal warmer as Astra smiled at him. Before he could think of what to say, a flash of light appeared beside them both and they jumped. Erio appeared right beside them and sighed to himself as he adjusted the bag on his shoulder.

"Sorry I'm late," he said quickly. "I sat down to look through the books I grabbed and lost track of time."

"You're just in time," Snowe said as he laughed weakly. "Did you find anything?"

Erio shrugged and opened his bag. "What I was looking for at least," he said and pulled out a leather-bound book from his bag. He quickly handed it to Astra. "And this thing. It burns my fingers when I touch it for long. Thought it might be useful to look through."

"What is it?" Astra opened it and as she looked at the pages, Erio took a step closer to Snowe, as though to keep a distance away from the book.

"Not really sure," Erio said. "Was told it was written by a lord for her spells; worth looking into at the very least."

As Erio pulled out another book to show everyone, Snowe caught sight of his eyes; through the past days, Erio's eyes had been passable for a human's eyes, but now their whites were a pitch black again.

"What happened to your eyes?" Snowe asked.

"What?"

Astra looked up from the book and stepped closer. "They're back to looking demony again," she said and frowned. "What happened?"

Erio sighed as he readjusted his bag again. "I had to do a few spells I hadn't used in a long time to trap someone," he said. "Probably too much for my disguise."

"Why?"

"A lord disguised as a demon was waiting for me," Erio said. "Had to get him off my tail." He paused as everyone looked at him, their eyes wide, and he sighed. "Don't look at me like that. We're safe. I promise." When Astra looked back at the book, Erio looked at Snowe. "How about that library?"

"We can get in tomorrow morning," Snowe explained. "The castle even offered to let us stay the night if we wish." Snowe paused as Astra and Hiante shared an uneasy look. "W-We'll be safe in the castle, I promise. No way a lord is getting inside."

"I'd rather not," Astra admitted as she closed the book. "You two can though! It'll get you into the library sooner I bet." She smiled at Snowe and Erio. "We can meet up with you two later."

"Are you sure?" Snowe asked.

"I am. The castle unnerves me to be honest," Astra said. She slipped the book into her bag and she gave both Snowe and Erio a quick hug. "We'll be staying at that bed-and-breakfast just down the road. See you tomorrow, all right? I'll try looking through this book later tonight."

As Astra and Hiante turned to head back down the street to find an inn, Snowe led Erio to where Iain had moved to. The patrolling guard had already moved on his way and Iain stood still as he watched the street. When he noticed Snowe had come closer, he frowned.

"The other two don't want to stay in the castle?" he asked.

"They don't really like the castle," Snowe said. "They'll stay at the bed-and-breakfast down the road."

"I can understand that," Iain said as he nodded. "It bugs me too. Though anywhere in our city walls are safe, so don't worry. I take it you still want to stay in the castle?"

Snowe nodded. "I would like to get to the library first thing in the morning," he said.

"Then that's the best way to do it," Iain replied. He looked at Erio and smiled. "I don't think we've met yet--my name's Iain."

"Erio," the demon replied.

"Your eyes look funny," Iain said.

Erio narrowed his eyes. "And your clothes look funny."

Iain laughed and he pulled at his tunic. "All guards have to wear it!" he said and led the way back down the street toward the castle.

"Then all guards look funny," Erio replied.

Though Iain gave Erio a glare, his smile did not disappear. As he went quiet and picked up the pace, Snowe breathed a sigh of relief; Iain was still as laid back as he remembered him to be. While Iain led them down the streets, his pace never letting up, Snowe found it tiring to keep up. All he wanted to do was crawl into bed and sleep. He could make sense of everything in the morning.

* * *

The bath had been warm, the night clothes and robe soft against his skin, but as Snowe left the bathroom connected to his room in the castle, he kept down a shudder. Though the bath had helped momentarily, he hadn't been able to stay warm since the castle attendants had shown him to his room. He doubted the sheets on his bed would even bring him any warmth at all and Snowe sighed as he stared at the bed. Tired for sure, but he doubted he would even fall asleep no matter how much he wanted to. Instead, Snowe went to the door and peeked into the hallway.

Alsan guards stood by dutifully in the halls, just like the Sabine guards had done (though the Alsan guards were all different people thankfully). As Snowe stepped out of his room and went down the hall, the guards only gave him a glance but none stopped him.

Snowe went to Erio's room right next door, but as he stared at the door, he decided to leave the demon be. On their way to the castle, Snowe had explained to him what had happened earlier and Erio was on edge. Even though Snowe assured Erio he would be fine, the demon didn't drop his guard while the attendants showed them both to their rooms.

He left Erio alone and continued down the hall. The walls, the drapery, even the carpet were too familiar for comfort and Snowe did his best to keep from trembling. He let his feet lead the way and he found his way down the main staircase before he went off into a hallway devoid of guards. Familiar again and as Snowe sighted a door left ajar at the end, light slipping into the dim hallway, Snowe realized he knew this hall. Last time he had been down here, Ciran had been at his side.

Snowe went to the door at the end of the hall and peeked inside, almost terrified of what he might find; Soan sat in a plush armchair facing away from the door. A book lay open on the armrest with a few stacked nearby and Snowe continued to look around the room. Another armchair sat beside Soan's and an end table was between the two. A wineglass, half-full, and an opened wine bottle were on top of the end table. A dark coffee table sat in front of the armchairs, books upon books resting upon it with an empty wine bottle beside the piles. Along the sides of the room were bookcases filled to the brim with books of all shapes and sizes and against the back wall was an impressive display cabinet. Behind the ornate glass doors were different kinds of drinking glasses next to a wide-range of liquor. As Snowe pushed the door open, it creaked and Soan glanced over the back of the armchair. A smile and Soan stood up.

"Good evening," he said. "Come in if you want. I don't mind the company."

As Snowe slipped inside, he shut the door behind him and slowly made his way over. Soan sat back in his armchair and picked up the book from the armrest and flipped through a few pages. When Snowe sat down and sunk into the armchair, Soan glanced at him.

"What did you think of the lunch, by the way?" Soan asked.

"Why didn't you want me to drink the wine you served?" Snowe replied as he looked at the old king.

"I didn't know what it would have done to Xiri," Soan said. He placed the book down on the stack of books on the coffee table before he sat back in his chair and grabbed his wineglass. "The directions only mentioned lords and you know, if it had really pulled Xiri out of you, I'm sure he would have broken through the magic binds and I don't even know what he would have done." He sighed and swirled the wine in his glass. "Besides, Xiri had ten years to get acclimated to you. If I forced him out like that, there could have been ramifications on your own body; it's not a nice spell in the least." He took a sip of wine.

"Do you think he'd be that connected to me?"

"Honestly, I don't know if you could live without Xiri."

The thought made him shudder. Sure it was something he must have known himself, but hearing it from someone else made it worse. "Why do you think that?" he asked.

"Just a hunch." Soan tapped his neck before he stood up and moved to the liquor cabinet across the room.

As the old king opened the cabinet doors, Snowe sat back in the chair and felt his neck. Without Xiri, he would be dead for sure by now. Thoughts drifted back to him and made him wonder if Xiri did leave him, would his body simply fall apart with nothing to hold it together? Even counting all the magic Erio had done on him, his body would have surely decayed to dust by now had Xiri not been inside of him. The thought made Snowe's head ache and he watched as Soan returned to his seat with another wineglass in hand. He poured Snowe half a glass and held it out.

"And I was sitting beside you Snowe," Soan said as Snowe took the glass. "If I accidentally released Xiri, who do you think would be the first to feel his wrath? I'm sure he would have killed me."

A chuckle escaped Snowe's throat; he wondered to himself if Soan was right. Would Xiri simply kill Soan? He hadn't done anything to Soan while his apparition had moved around the room. He had just looked at Soan sadly. No answer came forth from the back of Snowe's mind, not even a mocking one, and Snowe sipped his wine; it tasted distinctly of plums.

"Do you have any idea how to get Xiri out of me?" Snowe asked. "I don't want to cause him harm."

Soan sighed as he leaned his elbow on the armrest. "I do not," he said. "What Edgar and Lina did to you was monstrous. The spell they used was never intended for a demon inside anyone not prepared and certainly not a child." He took a sip of his wine. "Lords stopped using it, as I recall, years and years before Edgar and Lina happened upon it because demons became persuasive. I honestly have no idea where they found it."

Snowe watched his wine for a moment. It shuddered in his glass as he struggled to keep his hand steady.

"Though." Soan leaned toward Snowe. "If you're here, I think you're doing fine. Just don't let Xiri persuade you to kill your whole kingdom."

"What do you mean by that?" Snowe asked.

Soan paused before he downed the rest of his wine. He placed his glass on the low table in front of him before he stood. "That calls for something stronger." He returned to the liquor cabinet across the room and after shifting bottles and glasses inside for a moment, he pulled out two short glasses with a bottle of whiskey. He placed them on the low table and after he sat down again, he filled both glasses halfway. Before Snowe could place his wineglass down and grab the new glass, Soan had already downed his entire glass before he grabbed the bottle and refilled his glass again. Quiet, Snowe brought his glass to his lips and sipped. He cringed at the taste.

"It's not that bad," Soan said and laughed. He took a short drink this time from his own glass before he took a deep breath. "Xiri once convinced me killing everyone in my kingdom would make them all immortal and I naïvely believed him."

Snowe frowned. "Is that why you summoned him?"

Another drink and another glass poured. "A lord--can you believe it?--told me Xiri could make dreams come true. I didn't want my kingdom to die and it was the only course of action I could think of at the moment." He swirled the contents of his glass as he stared at it. "My father's death hit me very hard and I realized just how real mortality was. I wanted to stop it. Xiri made me that paradise in the sky for everyone, but no matter what we did, my people would still die. I couldn't even get them up to the paradise originally."

"But I could go into it."

"After I fixed it." Soan chuckled. "Long after I had killed everyone unfortunately." He took another drink before he leaned back in his seat. "A lord told me Xiri had lied about not being able to make them live forever or bring them onto my paradise--he knew how to and was just playing with me. The lord convinced me to devour Xiri so I could use his powers as my own." He tipped his glass back and drank the rest in one gulp while Snowe took another sip of his.

"I had been tricked, of course," Soan continued. "Years went by and I still had no way to bring my people to my paradise or even let them live forever. Xiri convinced me that all I had to do was to free my people from their bodies; that's why we don't live forever--our bodies decay. I just had to bring their souls and minds together and we could live forever in the sky."

Snowe took a longer drink from his glass. "I-Is that what my mother managed?" he asked. "W-With her and E-Edgar?"

"Brilliantly," Soan said and smirked.

"Did you really like her?"

Soan shrugged. "I was lonely," he said and laughed. "In any case, I have no idea what she and Edgar found, but they managed what I could not. I'm sure if my paradise had not fallen into the ocean, they really would have lived forever."

"But their minds warped and decayed," Snowe said. "They didn't even remember me."

"Consequences of their actions. Humans really aren't meant to live forever I suppose." Soan leaned forward and poured himself another glass. "Regardless, everyone did indeed live as long as I did while they slept in my mind. A few guards were missing, but that's beside the point." He paused for a moment before he sighed. "But I couldn't get them out." He drank again. "No bodies to return to, nothing for them to move out of my head. When I awoke in the Western Tower, Snowe, all of them were gone. Xiri's voice was gone, all the souls I felt in my head happy and asleep had gone away. It felt cold." He downed his glass again. "When Edgar pulled Xiri out of me, they all died in the process. All my plans gone in the span of one spell."

Snowe drank the rest in his glass and kept from cringing as the whiskey burned his throat. It didn't warm him in the least and Snowe sighed. "How old are you?" he asked before he held out his glass.

Soan snorted as he refilled Snowe's glass. "I have no idea," he said. "If I had to guess, I am well over a hundred by now. Though the world has not changed very much." He poured more whiskey into his glass and sat back again. "Lords don't want us to change, they don't want us to grow and change the world. The only new thing really are the trains." He took another drink and exhaled.

After a quick drink, Snowe looked at Soan again. "Is that why you want the lords gone?"

"Might as well use what power I have left to change the world so we can take charge of our own life," Soan said. "I've been tricked for far too long."

Snowe nodded as he drank a bit more. His head began to buzz and the trembling of his hands only increased.

"What do you want to do with Xiri anyway?" Soan asked as he placed his glass on the end table. He sat back and watched Snowe.

"I don't know. I just don't want to hurt him."

Soan smirked again. "Has he grown on you?" he asked. He chuckled when Snowe sighed and looked away. "Xiri was fun to be around. When I summoned him, he didn't greet me; he simply complained how cold it was and that the throne he had appeared with had turned to ice. Stole my cloak even and made it his own though it was much too big on him." Soan paused as Snowe chuckled. The old king let out a sigh and his smile disappeared. "I still regret every day that I ever ate him."

They went quiet. Snowe felt himself grow colder as his own smile faded. Regret. The word hung in Snowe's mind as he watched his whiskey in his glass and he felt his head pound. He leaned forward and sighed. "I hate this," he whispered. "I have to worry Xiri will make me kill everyone. I have to worry a lord will find me and kill me." He drank from his glass as he felt a few tears in his eyes. "All because he's trapped inside me. He won't trust me."

"I think he's been burned too much to trust anyone anymore," Soan said as he watched Snowe sadly.

"When I left that island, I was supposed to be happy," Snowe said as he shook his head. "But now? I have no idea what I'm doing." He downed his glass before he exhaled, the whiskey stung his throat again. "I did who knows what to piss off the lords--I'm dragging everyone down with me, I just--"

"And what of it?" Soan interrupted as he placed a hand on Snowe's shoulder, causing Snowe to flinch. When Snowe calmed down, he smiled. "Life is what it is in the end and you just have to keep going forward." He took his hand away and turned to Snowe. "You can make life great if you're determined enough and I sure as hell know you are. You survived against me. No one has ever done that."

Snowe smiled and as Soan sat back, he held out his glass.

"Just focus on living and pay Xiri no mind; he only has as much control as you give him," Soan said "Life is meant to be on your own terms. It's far too short so you owe it to yourself to make it what you want."

"You're right." Snowe clinked his glass against Soan's and he downed it in one gulp. It stung for sure but after exhaling, Snowe held out his glass again.

Soan laughed. "That's the spirit!" he said.

The glass filled once more, the buzz only louder in the back of Snowe's head, and Snowe put the glass to his lips and tipped it back.

* * *

Erio left the bathroom connected to his room and he hadn't felt any happier than when he had gone in for a bath. He had tried to study the book Beliaz had given him with the dictionary he had lifted, but he couldn't concentrate for very long. He had thought a nice bath would help and though the bath had been nice and his nightclothes fit perfectly and softly, Erio still felt uneasy. He wished Snowe hadn't told him what had happened to the lords earlier; the thought alone made him sick. Lords and demon were far apart indeed, but it still left him unnerved.

He moved to his bed and as he pulled back the sheets, he heard the door open. Instinctively, Erio reached for where his knife would have been if he had been in his normal clothes, but when he only saw Snowe at the door, his things with him, Erio relaxed.

"I thought I had that locked," Erio said as he stepped toward Snowe. The bag looked to have all Snowe's things in it and Snowe's cheeks were pinker than usual. "What's wrong?"

"Did-Did I say s-something was wrong?" Snowe said, his words slurred together. He stepped toward Erio but stopped and wavered in his spot.

"Are you drunk?" Erio asked as he stood in front of Snowe. He reached around Snowe and locked the door before Snowe fell into him. After he had Snowe standing on his own again, Erio glared at him. "You really are. What the hell did you drink?"

"J-Just a little wine," Snowe said as he kept down a few laughs.

"Sure it was." Erio sighed. "So what did you want?"

Snowe almost fell forward but he caught himself and straightened up. He looked at Erio, his smile gone and he looked uneasy. "C-Can I s-sleep here?" he asked.

"Why?"

Snowe shrugged but that only made him waver in his spot again. "It's cold in my room," he said. He looked at his feet and shifted them before he took a deep breath. "I-I d-don't know. I j-just don't f-feel safe in my room."

"Then drop your things," Erio said. "You're already here anyway. Might as well let you stay."

The relief on Snowe's face put Erio at ease; Snowe quickly knelt to place his things beside Erio's and as he stood back up, he started to fall to his side. Erio quickly got him upright again and helped him up.

"Sorry--did the room move?"

"Just you. Come on, time for bed so you can hopefully sleep off that hangover."

"I won't have a hangover!" Snowe laughed. "I didn't d-drink that much."

Erio rolled his eyes he helped Snowe over to the bed and once there, Snowe happily fell into it. As he curled up, robe and all, Erio pulled off his robe and left it on the nightstand beside his bed before he went around the room to put out the lights. By the time he glanced back at the bed, partly to make sure Snowe hadn't rolled off in a drunken stupor, he noticed Snowe had already hidden himself deep within the covers.

The lights out and the room darkened with only the moon keeping anything inside visible, Erio went to the other side of his bed and crawled underneath the covers beside Snowe. He rolled over to his side and rested his back against Snowe's before he closed his eyes. The room fell silent for only a moment before Snowe shifted beside Erio and rolled over.

"H-How h-have you and A-Astra been?" Snowe asked.

"What?" Erio rolled over and gave Snowe a hard look. "Where did that come from?"

Snowe shrugged as he peeked out from underneath the covers. "I just-just wanted to know when I should find s-some rice to toss."

Erio frowned as he fell back into his pillow. "Why do you have to say it like that," he said and sighed. "We tried a few things--and no I will not tell you what--but it just didn't click the way we thought it would. But that happens."

"Does it make you sad?"

Erio shrugged. "No it doesn't," he said. "It was nice to try, but we decided we like what we have. I don't want to change it for anything at this point." He stopped as he felt his cheeks heat up. "Why am I even replying to this?" He shut his eyes, annoyed. "Snowe you're--"

He stopped short as he felt lips against his own; he snapped his eyes open just in time to see Snowe's hand slip on the sheets underneath; Snowe fell down with a yelp and hit Erio's chest with his head.

"Ah, s-sorry," Snowe stammered. Though he had started to pull himself up, he stopped and settled against Erio's chest as his body trembled. "C-Can I s-sleep here?" He laughed awkwardly. "I-It's warm. I-I can hear your heartbeat." He paused before he exhaled. His trembling slowed to a stop. "It-It's nice. Much nicer than mine, you know..." His voice trailed off and soon he breathed evenly as though he had fallen asleep and he settled against Erio.

The demon hadn't moved. His mind had gone blank but when Snowe had quieted, Erio finally put his fingers to his lips. Had that really happened? Erio put his arm back down and stared at Snowe, wide-eyed. Of course Snowe had already fallen asleep and missed any expression Erio had thrown at him. He had half a mind to slip his arm out from underneath Snowe and ask him what just happened, but he didn't move and nor did he raise his voice.

He felt his lips again with his free hand. Snowe was really drunk; that must have been it.