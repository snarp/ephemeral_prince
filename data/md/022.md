# Epilogue - "Fallen from grace."

![](images/022_fleeting_epilogue.png)


A far cry from the Crown, the dark cell had no warmth, had very little light to speak of, but even still, Beliaz almost called it home if only to annoy his wardens. Even if his wrists bled from the cuffs almost too tight and his body twitched from the pain, it was home. Every day was the same and they blurred together now and just like always, Zuan stood in front of him and Beliaz just waited for the questions to begin. Of course he wouldn't have an answer. Zuan would become angry like he always did and the sun blonde lord would be shocked, strangled, or whatever the Crown ordered Zuan to do to get answers. In the past, the Crown had always given Zuan the deeds they themselves never wished to dirty their hands with and Zuan always did what was asked. Though, most would break quickly underneath Zuan's torture, Beliaz wouldn't allow himself to. Every day Zuan would leave frustrated and Beliaz would have solace for a brief few hours to ruminate on the new bruises fresh on his skin.

"We know you did something," Zuan snapped as he forced Beliaz to look up at him. "A month ago we all felt it. So did you. Something changed and the Crown broke. Phantoms are appearing at an alarming rate and we know at our very core that you had something to do with it."

"I don't remember."

"Beliaz, I know you do," Zuan whispered. "Just tell me."

"I don't."

Zuan let out a breath and he put his hands at Beliaz's throat. He lifted him up and the chains at Beliaz's wrists kept him from moving far.

"Do you think this is funny?" Zuan asked.

"If it was really my fault and you had proof, you'd have killed me already," Beliaz said quietly.

Beliaz crumpled to the ground as Zuan let his throat go. He steeled himself for another shock, but none came. Instead, Zuan knelt in front of Beliaz and he sighed.

"It's funny," Zuan said. "The very thing we make to protect our cores from destruction are the very things that give us the most pain. It's almost not worth it." He pulled a long dagger from his belt and the blade gleamed in the dim light. "It would be easy to open you up and get rid of you. After what you did, however, the Crown doesn't think you deserve a quick death."

"They can't even prove what I did if anything," Beliaz replied.

"But as lords, we know at the very core of our beings, that you did something that broke the Crown; we can sense you all over it when you shouldn't be," Zuan whispered as he gripped Beliaz's hair and pulled his head back. With his other hand, he rested the dagger against the side of Beliaz's neck. "We may not know the details, but you can't hide it from us. What did you do, Beliaz? I don't like hurting you. If we can fix it, perhaps they'll let me go easy on you."

_What a bold-faced lie_, Beliaz thought to himself as he kept down another laugh. Of course he delighted in hurting Beliaz; why else would he come day after day? Though Zuan looked sad, Beliaz knew it was only a ruse; anything to get Beliaz to talk. When the Crown realized what Beliaz had done, Beliaz had done his best to hide but Zuan had found him immediately. Beliaz had pleaded with him to listen, to not tell the Crown where he was, but Zuan turned him in anyway. This is what Zuan had always wanted: for Beliaz to be weaker than him again, and Beliaz knew it to be true.

"I don't know."

The wrong answer and the dagger slid along the side of Beliaz's neck. He shuddered as he felt his blood run along the side of his neck, staining his already ripped robes. If only it were a normal dagger, it wouldn't have hurt so much; long before Beliaz had been born, the blade had been infused with a spell. In seconds, pain blazed at the wound and slowly moved through his body. It felt as though he was on fire and he forced himself to keep from trembling.

"Just tell me, Beliaz," Zuan said, his voice quiet. When Beliaz didn't open his mouth, Zuan sighed again and placed the tip of the dagger against Beliaz's chest.

Beliaz didn't open his mouth or else he would have betrayed how much pain he was in. The dagger pressed against his chest and Beliaz tensed in place. If that wasn't enough, sparks formed at his wrists and pricked at his fingers. Another desperate attempt to frighten him. Beliaz would have laughed if wasn't in so much pain; the Crown really had no idea what he had done and it drove them all mad they sent Zuan to him day after day just to torture the answer--_any_ answer--out of him. The spell had worked better than Beliaz had thought it would; he had expected them to realize what exactly he did, but all they felt was his presence on the Crown. Though it hadn't been his intention, the spell had even broken the Crown itself; the stress had been too much for it. With it broken, the lords' powers waned and it was only a matter of time their power over the demons and the world faded away and as a result, the demons of old would wake to get their vengeance.

"Zuan."

The dagger stopped pressing into Beliaz's skin and the sparks faded away as Zuan stood. Free from Zuan's grip, Beliaz slumped against the wall, relieved at the break from the pain. Zuan turned his back to his former disciple and Beliaz noticed Azria, a lord from the broken Crown, had appeared. Her gold hair cropped just below her ears, it was almost a match for her eyes. Both seemed to glow in the dim light. She had never liked Beliaz.

"I told you not to kill him," Azria said.

"Did you now?" Zuan asked. "As I recall, just this morning you--"

"Zuan." Azria's eyes narrowed and the dark haired lord fell silent. "We've had a change of plan," she said and she took the dagger from Zuan. She slid it into her sash before she looked up at Zuan again. "He will be in your hands as your disciple once more and will accompany you wherever we send you. You must keep an eye on him, Zuan."

Even from behind, Zuan looked annoyed. Beliaz couldn't help it and let out a short laugh. He regretted it almost instantly, however, and sparks had returned and raced along his legs. He held back his scream as he struggled against the shock. The lightning stopped when Azria touched Zuan's arm.

"Don't look at me like that," she whispered. "Since he was your disciple, you must carry his burden as well. This is as much your fault as it is his."

"Is that wise? What if he gets away from me?"

"Then don't let that happen," she replied. "Keep an eye on him Zuan. You can harm him however you wish if he steps out of line, but if he dies, we will not hesitate to kill you too. If you cannot fix what he broke, then be sure to remember Rizec will deal with you then." She folded her arms and watched Zuan a moment as he tensed up. Beliaz hid a chuckle; he knew his mentor tensed because of the mention of a lord one could say was crueler than Zuan. "Don't forget what we're looking for."

With that, she was gone. Zuan stood frozen in his spot for a moment. When he turned to Beliaz finally, the sun blonde lord had to stop from laughing again; his mentor looked more annoyed than he had ever seen him before. The chains disappeared from Beliaz's wrists and Zuan stepped closer.

"Stand up."

"I like it down here," Beliaz said. He tensed up as Zuan took him by the hair and forced him to his feet.

"We have things to do, my disciple," Zuan said.

"Ah, you won't let me clean up first?"

Zuan grabbed Beliaz's throat and squeezed it. He put his forehead against Beliaz's and Beliaz kept from shuddering; he would not show how terrified he was.

"My patience wears thin. Everyone should see you as you are: a pathetic lord fallen from grace," Zuan whispered. "They made it so you can't use even half your powers while I live so don't even try."

A shock went through Beliaz again and flowed down from his neck. He gritted his teeth to prevent from screaming and Zuan smashed him against the wall before he finally let go. Just as Beliaz started to fall to his knees, Zuan punched Beliaz hard across the face, sending him to the floor. Struggling to pull himself up, Beliaz took a few deep breaths. He tensed for another assault, but Zuan had turned his back on Beliaz and started forward. As Beliaz steadied himself, blood fell from his nose and stained the floor. A smile he couldn't hide, however, formed on his own lips as he watched his blood on the floor. He covered his mouth to prevent himself from laughing else the pain would return twofold.

His spell had worked. No matter what Zuan or the Crown did to him, it wouldn't bother Beliaz in the least. His spell had worked. At the back of his mind, a new plan formed, one that had been whispered to him when he became one of the Crown, but he couldn't enact it yet. He knew he had to pay Snowe a visit, but he only had a vague idea where the fair-haired young man and his demon would be. Even still, he knew a visit would have to wait until he could get away from Zuan and the other lords. They wanted his head; they would ruin everything.

Beliaz forced himself back to his feet and despite the pain he had to endure each step, he followed Zuan like he had done when he was younger. Even when Zuan handed him his cloak, stain free, Beliaz didn't put it on. Everyone had to see what a pathetic lord he was. Pain all over, bruises dark against his skin--just like old times.

As he followed Zuan and left the small prison the lords had made just for him, he could feel all the lords' eyes on him, boring into his soul. Young and old alike. No doubt they saw each and every bruise underneath the bright sky of their lands. They didn't even know half the story; only that he had been dragged from the Crown unwilling. In fact, they knew he still held onto the throne given to him as the markings of the Crown were still visible on his face. He wouldn't give it up even if they tore his skin from his face; he would hold onto it as long as possible. If he wanted to help Snowe, that was the least he could do.

The lords nearby hushed as Beliaz followed Zuan, and he smiled at each of them just to prove he was the same Beliaz they had always known. He looked like hell to be sure; his blood stuck to his skin, his robes stained with the very same blood, but he didn't even care to wipe the blood away. His spell had worked. He had nothing to fear and he knew it showed in his smile.